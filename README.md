# Painel de Controle Super Ideal

Este projeto foi gerado com o [Angular CLI](https://github.com/angular/angular-cli) versão 1.6.7, utilizando o comando `ng new painel --style=scss --routing`.

## Atenção

Verifique se, em seu ambiente de desenvolvimento ou produção, o [NodeJS](https://nodejs.org/en/download/) está instalado e configurado.

## Servidor de desenvolvimento

Para o desenvolvimento, você deve instalar o Angular Cli: `npm install -g @angular/cli` e o Typescript: `npm i -g typescript`

Execute `npm start` para rodar a aplicação em modo dev. Navegue para `http://localhost:4200/` e o aplicativo irá recarregar automaticamente se você alterar qualquer um dos arquivos de origem.

## Cli

Execute `ng generate component component-name` para gerar um novo componente. Você também pode usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Execute `ng build --prod --build-optimizer` para fazer o [build](https://angular.io/guide/deployment) do projeto em modo de produção.

## Rodando o Build

Uma das alternativas é instalar um servidor do NodeJs chamado HttpServer: `npm install http-server -g`. Após a instalação basta entrar na pasta `dist` e rodar o comando: `http-server`. Para desabilitar o cors passe o `--cors` depois do comando. Caso prefira pode usar outro servidor como por exemplo o Apache ou Ngix.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Comandos GIT
### Remote
- Visualizar os remote adicionado ao projeto:
```
git remote -v
```

- Adicionar um remote:
```
git remote add nome_para_o_remote link_do_remote
```

- Remover um remote:
```
git remote rm nome_do_remote
```

### Commits
- Adicionar todos ou apenas um determinado arquivo para poder fazer o commit localmente
```
git add .
git add nome_do_arquivo
```

- Fazer commit localmente
```
git commit -m "mensagem_da_modificação"
```

- Adicionar e fazer o commit localmente ao mesmo tempo
```
git commit -a -m "mensagem_da_modificação"
```

### Push e Pull
- Pegar as modificações que tem no servidor: 
```
git pull nome_do_remote nome_do_branch_que_ira_receber_a_modificação
```

- Enviar as modificações para o servidor: 
```
git push nome_do_remote nome_do_branch_que_ira_receber_a_modificação
```

### Branch
- Listar todos os branchs com o último commit de cada um
```
git branch -v
```

- Criar um novo branch
```
git checkout -b nome_do_branch
```

- Ir para um determinado branch
```
git checkout nome_do_branch
```

- Integração de um Branch no Master
```
1- git checkout nova_funcionalidade
2- git rebase master // Atualiza o Branch com commits do Master
3- git checkout master
4- git merge nova_funcionalidade // Junta o Branch no Master
```

- Apagar um branch localmente:
```
git branch -D <nome do branch>
```

- Apagar um branch remotamente:
```
git push <nome do origin> <nome do branch> --delete
```

### Tags
- Listar as tags
```
git tag –l
```

- Adicionar tag
```
git tag texto_da_tag
```

- Enviar tags para o servidor
```
git push <nome do origin> <nome do branch> --tags
```

- Remover tag local
```
git tag --delete texto_da_tag 
```

- Remover tag do servidor
```
git push --delete texto_da_tag 
```

## Links Úteis
- [Forms Reactive Angular](https://toddmotto.com/angular-2-forms-reactive)
- [Wow.Js + Animation](https://wowjs.uk/docs.html): Revela animações de acordo com o programado
- [Css Animator](https://github.com/fabiandev/css-animator): Adiciona animações a elementos da página via TS
- [Toasty](https://github.com/akserg/ng2-toasty): Mostra Toast de mensagens
- [Buttons Ladda](https://github.com/hakimel/Ladda): Pre loader em botões
- [Context Menu](https://github.com/isaacplmann/ngx-contextmenu): Menu que é apresentado ao clicar com botão direito na página

- [Accesskey HTML5](https://www.w3schools.com/tags/att_global_accesskey.asp)

- Emitindo eventos entre páginas: [link](https://www.youtube.com/watch?v=R9afVKty3Dg)
