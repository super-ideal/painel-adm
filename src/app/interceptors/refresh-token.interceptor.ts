
import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, throwError as observableThrowError } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';

import { environment } from './../../environments/environment';
import { AuthService } from '../services/panel/auth.service';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

  constructor(private injector: Injector, private router: Router, private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(catchError((errorResponse: HttpErrorResponse) => {
      const error = (typeof errorResponse.error !== 'object') ? JSON.parse(errorResponse.error) : errorResponse;

      if (errorResponse.status === 401 && error.error.code === 'E_JWT_TOKEN_EXPIRED') {
        const http = this.injector.get(HttpClient);
        const refresh = localStorage.getItem('refresh') ? JSON.parse(atob(localStorage.getItem('refresh'))) : null;

        return http.post<any>(`${environment.api_url}auth/refresh`, {refresh_token: refresh}).pipe(
          mergeMap(res => {
            localStorage.setItem('token', btoa(JSON.stringify(res.data.token)));
            localStorage.setItem('refresh', btoa(JSON.stringify(res.data.refreshToken)));
            const cloneRequest = request.clone({setHeaders: {'Authorization': `Bearer ${res.data.token}`}});

            return next.handle(cloneRequest);
          }));

      } else if (errorResponse.status === 401 && (error.error.code === 'E_INVALID_JWT_REFRESH_TOKEN' || error.error.code === 'E_INVALID_JWT_TOKEN')) {
        // Irá fazer o logout no servidor e redirecionar para o login
        this.authService.logout().then((resp) => {
          localStorage.clear();
          this.router.navigate(['/auth'], { queryParams: { returnUrl: this.router.url } });
        }, (err) => {
          localStorage.clear();
          this.router.navigate(['/auth'], { queryParams: { returnUrl: this.router.url } });
        });
      }

      return observableThrowError(errorResponse);
    }));
  }
}
