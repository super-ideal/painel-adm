import { FormControl } from '@angular/forms';

/**
 *
 * @param otherControlName Propriedade do formulário a ser comparado.
 * @param nameError Opcional: nome para capturar o erro. Padrão: matchOther
 *
 * Arquivo Original: https://gist.github.com/slavafomin/17ded0e723a7d3216fb3d8bf845c2f30#gistcomment-2303196
 * Modicado por: Carlos Eduardo
 */
export function MatchOtherValidator (otherControlName: string, nameError?: string) {

  let thisControl: FormControl;
  let otherControl: FormControl;

  return function matchOtherValidate (control: FormControl) {

    if (!control.parent) {
      return null;
    }

    // Initializing the validator.
    if (!thisControl) {
      thisControl = control;
      otherControl = control.parent.get(otherControlName) as FormControl;
      if (!otherControl) {
        throw new Error('matchOtherValidator(): other control is not found in parent group');
      }
      otherControl.valueChanges.subscribe(() => {
        thisControl.updateValueAndValidity();
      });
    }

    if (!otherControl) {
      return null;
    }

    if (otherControl.value !== thisControl.value) {
      if (nameError) {
        const obj = {};
        obj[nameError] = true;
        return obj;
      }

      return { matchOther: true };
    }

    return null;

  };

}
