import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToggleFullscreenDirective } from './fullscreen/toggle-fullscreen.directive';

import { SpinnerComponent } from './spinner/spinner.component';

import { NgwWowModule } from 'ngx-wow';
import { AnimatorModule } from 'css-animator';

import { SplitButtonModule } from 'primeng/splitbutton';
import { PanelModule } from 'primeng/panel';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { GrowlModule } from 'primeng/growl';
import { TooltipModule } from 'primeng/tooltip';
import { ToolbarModule } from 'primeng/toolbar';

import { PipesModule } from '../pipes/pipes.module';
import { ValidadorCpfCNPJ } from './validator-cpf-cnpj/validador';
import { VerifyPermissions } from './verify-permissions/verify-permissions';

import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  imports: [
    CommonModule,
    NgwWowModule.forRoot(), // Wow + Animation
  ],
  declarations: [
    ToggleFullscreenDirective,
    SpinnerComponent,
  ],
  exports: [
    ToggleFullscreenDirective,
    SpinnerComponent,
    SplitButtonModule,
    InputTextModule,
    GrowlModule,
    TooltipModule,
    CardModule,
    PipesModule,
    PanelModule,
    ToolbarModule,
    TextMaskModule,
    AnimatorModule, // Animação de CSS no HTML
    NgwWowModule, // Wow + Animation
  ],
  providers: [ValidadorCpfCNPJ, VerifyPermissions]
})
export class SharedModule { }
