import { Injectable } from '@angular/core';

@Injectable()
export class ValidadorCpfCNPJ {
  constructor() { }

  /**
   * @description Válida um determinado CPF ou CNPJ
   *
   * ```ts
   * import { ValidadorCpfCNPJ } from './validador';
   *
   * export class MyClass{
   *  constructor(public ValidadorCpfCNPJ: ValidadorCpfCNPJ){
   *    const result = this.ValidadorCpfCNPJ.valida('number', 'type');
   *  }
   * }
   * ```
   *
	 * @param {string} number Informe o número do CPF ou CNPJ
	 * @param {string} type Informe o tipo da validação (CPF ou CNPJ)
   * @return {Boolean} Um booleano é retornado, sendo True caso o valor seja válido
   */
  valida(number: string, type: string) {
    if (type !== undefined && type.trim().toLowerCase() === 'cpf') {
      return this.validaCpf(number);
    } else if (type !== undefined && type.trim().toLowerCase() === 'cnpj') {
      return this.validaCnpj(number);
    } else {
    return false;
    }
  }

  /**
   * Válida um determinado CNPJ
   *
   * ```ts
   * import { ValidadorCpfCNPJ } from './validador';
   *
   * export class MyClass{
   *  constructor(public ValidadorCpfCNPJ: ValidadorCpfCNPJ){
   *    const result = this.ValidadorCpfCNPJ.validaCnpj('number');
   *  }
   * }
   * ```
   *
	 * @param {string} number Informe o número do CNPJ
   * @return {Boolean} Um booleano é retornado, sendo True caso o valor seja válido
   */
  validaCnpj(number: string) {
    const strCnpj = number.toString().replace(/[^\d]+/g, '');
    let numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais = 1;

    if (strCnpj.length < 14 && strCnpj.length < 15) {
      return false;
    }

    for (i = 0; i < strCnpj.length - 1; i++) {
      if (strCnpj.charAt(i) !== strCnpj.charAt(i + 1)) {
        digitos_iguais = 0;
        break;
      }
    }

    // tslint:disable-next-line:triple-equals
    if (digitos_iguais == 0) {
      tamanho = strCnpj.length - 2;
      numeros = strCnpj.substring(0, tamanho);
      digitos = strCnpj.substring(tamanho);
      soma = 0;
      pos = tamanho - 7;

      for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) {
          pos = 9;
        }
      }

      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

      // tslint:disable-next-line:triple-equals
      if (resultado != digitos.charAt(0)) {
        return false;
      }

      tamanho = tamanho + 1;
      numeros = strCnpj.substring(0, tamanho);
      soma = 0;
      pos = tamanho - 7;

      for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) {
          pos = 9;
        }
      }

      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

      // tslint:disable-next-line:triple-equals
      if (resultado != digitos.charAt(1)) {
        return false;
      }

      return true;
    } else {
      return false;
    }
  }

  /**
   * Válida um determinado CPF
   *
   * ```ts
   * import { ValidadorCpfCNPJ } from './validador';
   *
   * export class MyClass{
   *  constructor(public ValidadorCpfCNPJ: ValidadorCpfCNPJ){
   *    const result = this.ValidadorCpfCNPJ.validaCpf('number');
   *  }
   * }
   * ```
   *
   *
	 * @param {string} number Informe o número do CPF
   * @return {Boolean} Um booleano é retornado, sendo True caso o valor seja válido
   */
  validaCpf(number: string) {
    const strCpf = number.toString().replace(/[^\d]+/g, '');
    let soma = 0;
    let resto = 0;

    if (
      strCpf === '00000000000' ||
      strCpf === '11111111111' ||
      strCpf === '22222222222' ||
      strCpf === '33333333333' ||
      strCpf === '44444444444' ||
      strCpf === '55555555555' ||
      strCpf === '66666666666' ||
      strCpf === '77777777777' ||
      strCpf === '88888888888' ||
      strCpf === '99999999999'
    ) {
      return false;
    }

    // Cálculo do 1º dígito verificador
    soma = 0;
    for (let i = 10; i >= 2; i--) {
      // tslint:disable-next-line:radix
      soma = soma + parseInt(strCpf.substring(10 - i, 11 - i)) * i;
    }

    resto = (soma * 10) % 11;

    if (resto === 10) {
      resto = 0;
    }

    // tslint:disable-next-line:radix
    if (resto !== parseInt(strCpf.substring(9, 10))) {
      return false;
    }

    // Cálculo do 2º dígito verificador
    soma = 0;
    for (let i = 11; i >= 2; i--) {
      // tslint:disable-next-line:radix
      soma = soma + parseInt(strCpf.substring(11 - i, 12 - i)) * i;
    }

    resto = (soma * 10) % 11;

    if (resto === 10) {
      resto = 0;
    }

    // tslint:disable-next-line:radix
    if (resto !== parseInt(strCpf.substring(10, 11))) {
      return false;
    }

    return true;
    }
}
