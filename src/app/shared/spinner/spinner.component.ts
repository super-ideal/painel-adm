import {Component, Input, OnDestroy, Inject, ViewEncapsulation} from '@angular/core';
import {Spinkit} from './spinkits';
import {Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError} from '@angular/router';
import {DOCUMENT} from '@angular/common';

@Component({
    selector: 'app-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: [
        './spinner.component.css',
        './spinkit-css/sk-wave.css'
    ],
    encapsulation: ViewEncapsulation.None
})
export class SpinnerComponent implements OnDestroy {
    public isSpinnerVisible = true;
    public Spinkit = Spinkit;
    @Input() public backgroundColor = 'rgba(160, 0, 229, 0.69)';
    @Input() public spinner = Spinkit.skWave;

    constructor(private router: Router, @Inject(DOCUMENT) private document: Document) {
      this.router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
          this.isSpinnerVisible = true;
        } else if ( event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
          this.isSpinnerVisible = false;
        }
      }, () => {
        this.isSpinnerVisible = false;
      });
    }

    ngOnDestroy(): void {
      this.isSpinnerVisible = false;
    }
}
