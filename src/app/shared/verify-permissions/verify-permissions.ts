import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import { AuthService } from '../../services/panel/auth.service';

export interface Actions {
  id: number;
  name: string;
  description: string;
  page_action_id: number;
}

@Injectable()
export class VerifyPermissions {
  constructor(private _authService: AuthService) {}

  /**
   * Pega todas as permissões que o usuário tem em uma página especifica
   * @param page Nome da página
   */
  getActionsUser(page: string, typeReturn: string = 'object'): any {
    const permissionsUser = this._authService.getUserLocal()['permissions'];

    if (typeReturn.toLowerCase() === 'object') {
      const actionsUser = {};
      _.filter(permissionsUser, { page_name: page.toUpperCase() }).forEach(e => {
          actionsUser[e.action_name] = {
            id: e.action_id,
            name: e.action_name,
            description: e.action_description,
            page_action_id: e.page_action_id
          };
      });
      return actionsUser;
    } else {
      const actionsUser = [];
      _.filter(permissionsUser, { page_name: page.toUpperCase() }).forEach(e => {
        actionsUser.push({
          id: e.action_id,
          name: e.action_name,
          description: e.action_description,
          page_action_id: e.page_action_id
        });
      });
      return actionsUser;
    }
  }

  /**
   * Verifica se o usuário tem uma determinada permissão
   * @param page Nome da página
   * @param action Nome da ação
   */
  verifyPermission(page: string, action: string): boolean {
    const permissionsUser = this._authService.getUserLocal()['permissions'];
    if (page && action) {
      const actionsUser = _.filter(permissionsUser, { page_name: page.toUpperCase() || null });
      const a = _.filter(actionsUser, { action_name: action.toUpperCase() || null });

      if (a.length > 0) {
        return true;
      }
    }

    return false;
  }
}
