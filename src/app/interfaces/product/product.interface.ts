export interface Product {
  id: Number;
  active: Boolean;
  barcode: string;
  description: string;
  description_long: string;
  price: Number;
  unity_id: Number;
  created_by: Number;
  updated_by: Number;
  created_at: string;
  updated_at: string;
}
