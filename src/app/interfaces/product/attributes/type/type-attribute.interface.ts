export interface TypeAttribute {
  id: Number;
  active: Boolean;
  style: string;
  required: Boolean;
  public: Boolean;
  name: string;
  description: string;
  created_at: string;
  updated_at: string;
}
