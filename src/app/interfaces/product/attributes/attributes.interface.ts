export interface Attribute {
  id: Number;
  active: Boolean;
  name: string;
  description: string;
  type_id: Number;
  type_name: string;
  type_description: string;
  type_active: Boolean;
  type_multiple: Boolean;
  type_required: Boolean;
  created_at: string;
  updated_at: string;
}
