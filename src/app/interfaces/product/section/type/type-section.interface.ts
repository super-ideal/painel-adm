export interface TypeSection {
  id: Number;
  name: string;
  required: Boolean;
  created_at: string;
  updated_at: string;
}
