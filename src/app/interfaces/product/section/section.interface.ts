export interface Section {
  id: Number;
  active: Boolean;
  name: string;
  description: string;
  type_id: Number;
  type_name: Number;
  type_required: Boolean;
  created_at: string;
  updated_at: string;
}
