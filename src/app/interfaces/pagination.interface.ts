export interface Pagination {
  perPage: number; // O número de elementos na página
  total: number; // O número total de elementos
  lastPage: number; // O número total de páginas
  page: number; // O número da página atual
}
