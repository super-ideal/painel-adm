import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: '<router-outlet><app-spinner></app-spinner></router-outlet><ng2-toasty position="bottom-right"></ng2-toasty>',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
}
