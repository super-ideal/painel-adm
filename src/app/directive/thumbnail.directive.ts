
import { Directive, ElementRef, Input, Renderer, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: 'img[thumbnail]'
})
export class ThumbnailDirective {

  @Input() public image: any;
  @Input() public maxSize: any;

  constructor(private el: ElementRef, private renderer: Renderer) { }

  // tslint:disable-next-line:use-life-cycle-interface
  public ngOnChanges(changes: SimpleChanges) {

    const reader = new FileReader();
    const el = this.el;

    reader.onloadend = (readerEvent) => {
      const image = new Image();
      image.onload = (imageEvent) => {
        // Resize the image
        const canvas = document.createElement('canvas');
        const maxSize = this.maxSize || 100;
        let width = image.width;
        let height = image.height;

        if (width > height) {
          if (width > maxSize) {
            height *= maxSize / width;
            width = maxSize;
          }
        } else {
          if (height > maxSize) {
            width *= maxSize / height;
            height = maxSize;
          }
        }

        canvas.width = width;
        canvas.height = height;
        canvas.getContext('2d').drawImage(image, 0, 0, width, height);
        el.nativeElement.src = canvas.toDataURL('image/jpeg');
      };
      image.src = reader.result;
    };

    if (this.image) {
      return reader.readAsDataURL(this.image);
    }

  }

}
