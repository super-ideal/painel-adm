import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';

import { VerifyPermissions } from '../shared/verify-permissions/verify-permissions';
import { ToastData, ToastOptions, ToastyService } from 'ng2-toasty';
import { AuthService } from '../services/panel/auth.service';

@Injectable()
export class AclGuard implements CanActivate, CanActivateChild {

  constructor(
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private location: Location,
    private _authService: AuthService,
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.verifyPermission(route);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.verifyPermission(route);
  }

  private verifyPermission(route): boolean {
    const data = route['data'];
    const permissionPage = this.verifyPermissions.verifyPermission(data['permission']['page'], data['permission']['action']);

    if (!permissionPage) {
      // faz o logout do usuário caso ele não tenha permissão de acesso ao painel de controle
      if (data['logout']) {
        this.onLogout();
      }

      this.addToast('warning', 'Acesso negado', 'Você não tem permissão para acessar a página solicitada.');
      this.location.back();
      return false;
    }

    return true;
  }

  /**
   * Faz o logout do usuário
   */
  onLogout() {
    this._authService.logout().then((resp) => {
      localStorage.clear();
    }, (err) => {
      localStorage.clear();
    });
  }

  /**
    * Toast de alertas
    * @param {string} type Tipo do toast (success, error, warning, wait)
    * @param {string} title Título do toast
    * @param {string} message Mensagem do toast
    * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
    * @param {string} [idToast] Id único para o toast
    */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
