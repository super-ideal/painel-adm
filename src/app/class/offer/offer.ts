export class Offer {
  id: number;
  name: string;
  description: string;
  offer_types_id: number;
  offerType: {
    id: number;
    description: string;
  };
  datetime_start: Date;
  datetime_end: Date;
  created_by: number;
  updated_by: number;
  created_at: string;
  updated_at: string;
  created: {
    id: number;
    person_id: number;
    username: string;
    people: {
      id: number;
      name_reason: string;
    }
  };
  updated: {
    id: number;
    person_id: number;
    username: string;
    people: {
      id: number;
      name_reason: string;
    }
  };
}
