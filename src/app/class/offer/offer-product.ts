export class OfferProducts {
  id: number;
  active: boolean;
  product_id: number;
  product: {
    id: number;
    active: boolean;
    barcode: string;
    description: string;
    description_long: string;
    price: number;
    unity_id: number;
    created_by: number;
    updated_by: number;
    reated_at: Date;
    updated_at: Date;
  };
  offer_id: number;
  offer_motive_id: number;
  offerMotive: {
    id: number;
    name: string;
    description: string;
    created_at: Date;
    updated_at: Date;
  };
  type_discount_percentage: boolean;
  type_discount_money: boolean;
  discount: number;
  cumulative: number;
  price_group: boolean;
  created_at: string;
  updated_at: string;
  added_by: number;
  added: {
    id: number;
    person_id: number;
    username: string;
    people: {
      id: number;
      name_reason: string;
    }
  };
  updated_by: number;
  updated: {
    id: number;
    person_id: number;
    username: string;
    people: {
      id: number;
      name_reason: string;
    }
  };
}
