import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pickUpCertainWords',
})
export class PickUpCertainWordsPipe implements PipeTransform {
  /**
   * Pega determinada quantidade de palavras de uma string e converte a primeira letra de cada palavra para maiúsculo
   *
   * @param wordQuantity Quantidade de palavras a retornar
   */
  transform(value: string, wordQuantity: number = 1, ...args) {
    let string = '';

    if (value !== null) {
      for (let _i = 0; _i < wordQuantity; _i++) {
        if (value.trim().split(' ')[_i] === undefined) {
          break;
        }

        const word = value.trim().split(' ')[_i];
        const convertFirstLetterUppercase = word.substr(0, 1).toUpperCase() + word.substr(1);
        string = string + ' ' + convertFirstLetterUppercase;
      }
    }

    return string;
  }
}

