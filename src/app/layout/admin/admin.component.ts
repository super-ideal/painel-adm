import { Component, OnInit } from '@angular/core';
import { animate, AUTO_STYLE, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';

import { ToastyService, ToastOptions } from 'ng2-toasty';

import { AuthService } from '../../services/panel/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  animations: [
    trigger('mobileMenuTop', [
      state('no-block, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('yes-block',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('no-block <=> yes-block', [
        animate('400ms ease-in-out')
      ])
    ]),
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('slideOnOff', [
      state('on', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('off', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('on => off', animate('400ms ease-in-out')),
      transition('off => on', animate('400ms ease-in-out'))
    ]),
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({opacity: 0}),
        animate('400ms ease-in-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translate(0)'}),
        animate('400ms ease-in-out', style({opacity: 0}))
      ])
    ])
  ]
})

export class AdminComponent implements OnInit {
  toggleOn: boolean;
  isCollapsedMobile: string;
  deviceType: string; /* desktop(default), tablet, mobile */
  verticalNavType: string; /* expanded(default), offcanvas */
  verticalEffect: string; /* shrink(default), push, overlay */
  innerHeight: string;
  windowWidth: number;

  userData: any;
  itemsMenu: MenuItem[];

  constructor(
    private router: Router,
    private _authService: AuthService,
    private toastyService: ToastyService
  ) {
    this.toggleOn = true;
    this.isCollapsedMobile = 'no-block';
    this.deviceType = 'desktop';
    this.verticalNavType = 'expanded';
    this.verticalEffect = 'shrink';

    const scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributes(this.windowWidth);
  }

  async ngOnInit() {
    this.getUserLocal();

    this.itemsMenu = [
      {
        label: 'Dashboard',
        icon: 'fas fa-tachometer-alt',
        routerLink: '/dashboard'
      },
      {
        label: 'Parceiros',
        icon: 'fas fa-handshake',
        routerLink: '/parceiros'
      },
      {
        label: 'Usuários',
        icon: 'fas fa-users',
        routerLink: '/usuarios'
      },
      {
        label: 'Permissões',
        icon: 'fas fa-lock',
        routerLink: '/permissoes'
      },
      {
        label: 'Produtos',
        icon: 'fas fa-shopping-cart',
        items: [
          {
            label: 'Catálago',
            routerLink: '/produtos'
          },
          {
            label: 'Atributos',
            items: [
              {
                label: 'Catálago',
                routerLink: '/produtos/atributos'
              },
              {
                label: 'Tipos',
                routerLink: '/produtos/atributos/tipos'
              }
            ]
          },
          {
            label: 'Seções',
            items: [
              {
                label: 'Catálago',
                routerLink: '/produtos/secoes'
              },
              {
                label: 'Tipos',
                routerLink: '/produtos/secoes/tipos'
              }
            ]
          }
        ]
      },
      {
        label: 'Agenda de Ofertas',
        icon: 'fas fa-cart-arrow-down',
        items: [
          {
            label: 'Catálago',
            routerLink: '/ofertas'
          },
          {
            label: 'Tipos',
            routerLink: '/ofertas/tipos'
          },
          {
            label: 'Motivos',
            routerLink: '/ofertas/motivos'
          }
        ]
      },
      {
        label: 'Listas de Compras',
        icon: 'fas fa-clipboard-list',
        routerLink: '/listas-compras'
      },
    ];
  }

  async getUserLocal() {
    this.userData = await this._authService.getUserLocal();
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';

    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    let reSizeFlag = true;
    if (this.deviceType === 'tablet' && this.windowWidth >= 768 && this.windowWidth <= 1024) {
      reSizeFlag = false;
    } else if (this.deviceType === 'mobile' && this.windowWidth < 768) {
      reSizeFlag = false;
    }

    /* for check device */
    if (reSizeFlag) {
      this.setMenuAttributes(this.windowWidth);
    }
  }

  setMenuAttributes(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  toggleOpened() {
    if (this.windowWidth < 768) {
      this.toggleOn = this.verticalNavType === 'offcanvas' ? true : this.toggleOn;
    }
    this.verticalNavType = this.verticalNavType === 'expanded' ? 'offcanvas' : 'expanded';
  }

  onClickedOutside(e: Event) {
    if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
      this.toggleOn = true;
      this.verticalNavType = 'offcanvas';
    }
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onLogout() {
    this._authService.logout().then((resp) => {
      this.router.navigate(['auth/login']);
      localStorage.clear();
    }, (err) => {
      this.router.navigate(['auth/login']);
      localStorage.clear();
    });
  }

  /**
   * Toast de carregamento
   */
  addToast(title: string, message: string, type: string) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: false,
      timeout: 5000,
      theme: 'bootstrap',
    };

    switch (type) {
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'success': this.toastyService.success(toastOptions); break;
      case 'info': this.toastyService.info(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
    }
  }
}
