import { Component } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  public version: string = environment.version;
  public year: number;

  constructor() {
    const now = new Date();
    this.year = now.getFullYear();
  }

}
