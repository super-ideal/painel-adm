
import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './../api.service';

@Injectable()
export class UsersService {

  constructor(private api: ApiService) { }

  public getAll(
    options?: {
      pageNumber?: Number,
      perPage?: Number,
      whereBy?: string,
      operator?: string,
      whereText?: string | boolean,
      orderby?: string,
      ordersort?: string
    }
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('users', {
        page: options && options.pageNumber || 1,
        perPage: options && options.perPage || 25,
        column: options && options.whereBy || 'email',
        operator: options && options.operator || 'anywhere',
        text: options && options.whereText || '',
        oBy: options && options.orderby || 'email',
        oSort: options && options.ordersort || 'asc'
      })
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public get(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('users/' + id)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public update(data, id: Number) {
    return new Promise((resolve, reject) => {
      this.api.patch('users/' + id, data)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(data) {
    return new Promise((resolve, reject) => {
      this.api.post('users', data)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public delete(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.delete('users/' + id)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
