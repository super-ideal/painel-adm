
import {timeout} from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './../api.service';

@Injectable()
export class OfferService {

  constructor(private api: ApiService) { }

  public getAll(
    options?: {
      pageNumber?: Number,
      perPage?: Number,
      whereBy?: string,
      operator: string,
      whereText?: string
    }
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('offers', {
        page: options && options.pageNumber || 1,
        perPage: options && options.perPage || 25,
        column: options && options.whereBy || '',
        operator: options && options.operator || 'like',
        text: options && options.whereText || ''
      }).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public get(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('offers/' + id).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public update(data, id: Number) {
    return new Promise((resolve, reject) => {
      this.api.patch('offers/' + id, data).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(data) {
    return new Promise((resolve, reject) => {
      this.api.post('offers', data).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public delete(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.delete('offers/' + id).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
