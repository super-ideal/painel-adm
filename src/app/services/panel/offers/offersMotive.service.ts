
import {timeout} from 'rxjs/operators';
import { Injectable } from '@angular/core';



import { ApiService } from './../api.service';

@Injectable()
export class OffersMotiveService {

  constructor(private api: ApiService) { }

  public getAll(
    options?: {
      pageNumber?: Number,
      perPage?: Number,
      whereBy?: string,
      whereText?: string,
      pagination?: boolean,
      active?: boolean
    }
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('offers/motives/product', {
        page: options && options.pageNumber || 1,
        perPage: options && options.perPage || 25,
        wBy: options && options.whereBy || '',
        w: options && options.whereText || '',
        pagination: options && options.pagination || false,
        active: options && options.active || null
      })
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public get(idMotive: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('offers/motives/' + idMotive + '/product').pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public patch(data, idMotive: Number) {
    return new Promise((resolve, reject) => {
      this.api.patch('offers/motives/' + idMotive + '/product', data).pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(data) {
    return new Promise((resolve, reject) => {
      this.api.post('offers/motives/product', data).pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public delete(idMotive: Number) {
    return new Promise((resolve, reject) => {
      this.api.delete('offers/motives/' + idMotive + '/product').pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
