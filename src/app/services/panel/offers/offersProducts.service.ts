
import {timeout} from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './../api.service';

@Injectable()
export class OfferProductService {

  constructor(private api: ApiService) { }

  /**
   * Verifica se um determinado produto está em alguma agenda de oferta que está a iniciar ou em andamento
   * @param column Coluna no BD que será utilizado para filtrar os resultados
   * @param text Texto usado para filtrar o conteúdo
   */
  public verifyProductsInOffersProgress(column: string = 'offer_name', text?: string | Number, operator: string = 'equal', pagination: boolean = false) {
    return new Promise((resolve, reject) => {
      this.api.get('offers/products/progress', {column: column, text: text, operator: operator, pagination: pagination}).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public verifyProductsInOffers(column: string = 'offer_name', text?: string | number, operator: string = 'equal', pagination: boolean = false) {
    return new Promise((resolve, reject) => {
      this.api.get('offers/products/verify', {column: column, text: text, operator: operator, pagination: pagination}).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  /**
   * Busca os produtos do grupo de preço que estão em oferta
   * @param idOffer ID da agenda de oferta
   * @param idOfferProduct ID da tabela de relacionamento do produto com a agenda de oferta
   */
  public getProductsGroupPrice(idOffer: number, idOfferProduct: number) {
    return new Promise((resolve, reject) => {
      this.api.get('offers/' + idOffer + '/products/pricegroup/' + idOfferProduct)
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  /**
   * Busca todos os produtos de grupo de preço que estão em oferta em uma determinada agenda
   * @param idOffer ID da agenda de oferta
   */
  public getAllProductsGroupPrice(idOffer: number) {
    return new Promise((resolve, reject) => {
      this.api.get('offers/' + idOffer + '/products/pricegroup')
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  /**
   * Busca todos os produtos cadastrados nas agendas de oferta
   * @param idOffer ID da agenda de oferta
   * @param pageNumber
   * @param perPage
   * @param whereBy
   * @param operator
   * @param whereText
   * @param groupBy
   */
  public getAll(
    idOffer: Number,
    pageNumber: Number = 1,
    perPage: Number = 50,
    whereBy: string = '',
    operator: string,
    whereText: string = ''
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('offers/' + idOffer + '/products', {
        page: pageNumber,
        perPage: perPage,
        column: whereBy,
        operator: operator,
        text: whereText
      }).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public update(data, idProductOffer: Number) {
    return new Promise((resolve, reject) => {
      this.api.patch('offers/products/' + idProductOffer, data).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(data, idOffer: Number) {
    return new Promise((resolve, reject) => {
      this.api.post('offers/' + idOffer + '/products', data).pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
