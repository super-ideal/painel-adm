
import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from '../api.service';

@Injectable()
export class ShoppingListService {

  constructor(private api: ApiService) { }

  public get(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('shopping/lists/' + id)
        .pipe(timeout(15000))
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public getAll(
    options?: {
      pageNumber?: Number,
      perPage?: Number,
      column?: string,
      operator?: string,
      text?: string | boolean,
      orderby?: string,
      ordersort?: string
    }
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('shopping/lists', {
        page: options && options.pageNumber || 1,
        perPage: options && options.perPage || 25,
        column: options && options.column || 'name',
        operator: options && options.operator || 'anywhere',
        text: options && options.text || '',
        oBy: options && options.orderby || 'id',
        oSort: options && options.ordersort || 'asc'
      })
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
