
import {timeout} from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from '../api.service';

@Injectable()
export class AcessoService {

  constructor(private api: ApiService) { }

  public get(
    pageNumber: Number = 1,
    perPage: Number = 25,
    whereBy: string = '',
    whereText: string = ''
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('ihx/acessos', {page: pageNumber, perPage: perPage, wBy: whereBy, w: whereText})
      .pipe(timeout(10000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

}
