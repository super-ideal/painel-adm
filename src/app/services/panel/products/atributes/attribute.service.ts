
import {timeout} from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './../../api.service';

@Injectable()
export class AttributeService {

  constructor(private api: ApiService) { }

  public getAll(
    options?: {
      pageNumber?: Number,
      perPage?: Number,
      whereBy?: string,
      whereText?: string
    }
) {
    return new Promise((resolve, reject) => {
      this.api.get('products/attributes', {
        page: options && options.pageNumber || 1,
        perPage: options && options.perPage || 25,
        wBy: options && options.whereBy || '',
        w: options && options.whereText || ''
      })
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public get(idAttribute: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('products/attributes/' + idAttribute).pipe(
      timeout(15000)) // tempo de espera para resposta do servidor
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public update(data, id: Number) {
    return new Promise((resolve, reject) => {
      this.api.patch('products/attributes/' + id, data).pipe(
      timeout(15000)) // tempo de espera para resposta do servidor
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(data) {
    return new Promise((resolve, reject) => {
      this.api.post('products/attributes', data).pipe(
      timeout(15000)) // tempo de espera para resposta do servidor
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
