
import {timeout} from 'rxjs/operators';
import { Injectable } from '@angular/core';



import { ApiService } from './../api.service';

@Injectable()
export class ProductService {

  constructor(private api: ApiService) { }

  public getAll(
    options?: {
      pageNumber?: Number,
      perPage?: Number,
      whereBy?: string,
      whereText?: string
    }
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('products', {
        page: options && options.pageNumber || 1,
        perPage: options && options.perPage || 25,
        wBy: options && options.whereBy || '',
        w: options && options.whereText || '',
      })
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public getAllTypesAttributes() {
    return new Promise((resolve, reject) => {
      this.api.get('products/attributes/types', {
        page: 1,
        perPage: 100,
        active: 'true',
        detailsAttributes: 'true'
      })
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public getAllTypesSection() {
    return new Promise((resolve, reject) => {
      this.api.get('products/section/types/?detailsSections=true', {
        page: 1,
        perPage: 100,
        detailsAttributes: 'true'
      })
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public get(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('products/' + id)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public update(data, id: Number) {
    return new Promise((resolve, reject) => {
      this.api.patch('products/' + id, data)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(data) {
    return new Promise((resolve, reject) => {
      this.api.post('products', data)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public delete(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.delete('products/' + id)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public verifyBarcodeDuplicated(barcode: string) {
    return new Promise((resolve, reject) => {
      this.api.get('products/barcode/' + barcode)
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public getDetailsProductsGroupPrice(
    idAttributeProduct: number,
    column: string = 'p.id',
    text: string = '',
    operator: string = 'anywhere'
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('products/groupprice/' + idAttributeProduct, {column: column, text: text, operator: operator})
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
