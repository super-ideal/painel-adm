
import {timeout} from 'rxjs/operators';
import { Injectable } from '@angular/core';



import { ApiService } from '../../../api.service';

@Injectable()
export class SectionTypeService {

  constructor(private api: ApiService) { }

  public getAllTypes(
    options?: {
      pageNumber?: Number,
      perPage?: Number,
      whereBy?: string,
      whereText?: string,
      pagination?: boolean,
      active?: boolean
    }
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('products/section/types', {
        page: options && options.pageNumber || 1,
        perPage: options && options.perPage || 25,
        wBy: options && options.whereBy || '',
        w: options && options.whereText || '',
        pagination: options && options.pagination || false
      })
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public getType(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('products/section/types/' + id).pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public updateType(data, id: Number) {
    return new Promise((resolve, reject) => {
      this.api.patch('products/section/types/' + id, data).pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public addType(data) {
    return new Promise((resolve, reject) => {
      this.api.post('products/section/types', data).pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
