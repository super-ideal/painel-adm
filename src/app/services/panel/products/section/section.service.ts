
import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';



import { ApiService } from '../../api.service';

@Injectable()
export class SectionService {

  constructor(private api: ApiService) { }

  public getAll(
    pageNumber: Number = 1,
    perPage: Number = 25,
    whereBy: string = '',
    whereText: string = ''
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('products/section', {
        page: pageNumber,
        perPage: perPage,
        wBy: whereBy,
        w: whereText
      })
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public get(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('products/section/' + id).pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public update(data, id: Number) {
    return new Promise((resolve, reject) => {
      this.api.patch('products/section/' + id, data).pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(data) {
    return new Promise((resolve, reject) => {
      this.api.post('products/section', data).pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public delete(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.delete('products/section/' + id).pipe(
      timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

}
