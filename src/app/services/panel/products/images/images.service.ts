
import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './../../api.service';

@Injectable()
export class ProductImageService {

  constructor(private api: ApiService) { }

  public delete(idImage: Number) {
    return new Promise((resolve, reject) => {
      this.api.delete('products/images/' + idImage)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
