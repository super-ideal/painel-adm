
import {timeout} from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from '../api.service';

@Injectable()
export class UnitysService {

  constructor(private api: ApiService) { }

  public getAll() {
    return new Promise((resolve, reject) => {
      this.api.get('products/unitys')
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

}
