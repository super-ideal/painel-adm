import { Injectable } from '@angular/core';
import { ApiService } from './../../api.service';

@Injectable()
export class EmailsService {

  constructor(private api: ApiService) { }

  public update(id, credentials) {
    return new Promise((resolve, reject) => {
      this.api.patch('partners/emails/' + id, JSON.stringify(credentials))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public delete(id) {
    return new Promise((resolve, reject) => {
      this.api.delete('partners/emails/' + id)
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(data, idPartner) {
    return new Promise((resolve, reject) => {
      this.api.post('partners/emails/' + idPartner, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
