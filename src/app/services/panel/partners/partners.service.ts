
import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './../api.service';

@Injectable()
export class PartnersService {

  constructor(private api: ApiService) { }

  public getAll(
    options?: {
      pageNumber?: Number,
      perPage?: Number,
      whereBy?: string,
      operator?: string,
      whereText?: string,
      orderby?: string,
      ordersort?: string,
      groupBy?: string,
      user?: boolean
    }
  ) {
    return new Promise((resolve, reject) => {
      this.api.get('partners', {
        page: options && options.pageNumber || 1,
        perPage: options && options.perPage || 25,
        column: options && options.whereBy || '',
        operator: options && options.operator || 'anywhere',
        text: options && options.whereText || '',
        oBy: options && options.orderby || 'name_reason',
        oSort: options && options.ordersort || 'asc',
        gBy: options && options.groupBy,
        user: options && options.user || false
      })
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public get(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('partners/' + id)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public update(data, id: Number) {
    return new Promise((resolve, reject) => {
      this.api.patch('partners/' + id, data)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(data) {
    return new Promise((resolve, reject) => {
      this.api.post('partners', data)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public delete(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.delete('partners/' + id)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public typesPartners() {
    return new Promise((resolve, reject) => {
      this.api.get('partners/types')
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
