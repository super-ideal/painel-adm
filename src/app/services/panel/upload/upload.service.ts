import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class UploadService {

  constructor(public http: HttpClient) { }

  public urlApi = environment.upload_url;

  public deleteImage(path: string, filename: string) {
    return new Promise((resolve, reject) => {
      this.http.delete(environment.upload_url + 'image/' + path + '/' + filename)
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
