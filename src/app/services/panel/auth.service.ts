import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './api.service';

@Injectable()
export class AuthService {

  constructor(private api: ApiService) { }

  public check(): boolean {
    return (localStorage.getItem('token') && localStorage.getItem('user')) ? true : false;
  }

  public login(credentials: { email: string, password: string }) {
    return new Promise((resolve, reject) => {
      this.api.post('auth/login', credentials)
      .pipe(timeout(10000))
      .subscribe(res => {
        localStorage.setItem('token', btoa(JSON.stringify(res['data']['auth']['token'])));
        localStorage.setItem('refresh', btoa(JSON.stringify(res['data']['auth']['refreshToken'])));
        localStorage.setItem('user', btoa(JSON.stringify(res['data']['user'])));
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public logout() {
    return new Promise((resolve, reject) => {
      this.api.get('auth/logout').pipe(
      timeout(10000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public getUserLocal() {
    return localStorage.getItem('user') ? JSON.parse(atob(localStorage.getItem('user'))) : null;
  }

}
