
import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './../api.service';

@Injectable()
export class UserPermissionsService {

  constructor(private api: ApiService) { }

  /**
   * Busca todas as permissões de um usuário
   * @param idUser Id único do usuário
   */
  public getIdUser(idUser: number) {
    return new Promise((resolve, reject) => {
      this.api.get(`acl/user/${idUser}`)
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public update(data, idUser: number) {
    return new Promise((resolve, reject) => {
      this.api.patch('acl/user/' + idUser, data)
      .pipe(timeout(15000))
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }
}
