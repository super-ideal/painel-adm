
import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './../api.service';

@Injectable()
export class PageActionPermissionService {

  constructor(private api: ApiService) { }

  /**
   * Busca todas as permissões cadastradas
   */
  public getAll() {
    return new Promise((resolve, reject) => {
      this.api.get('acl/action/page')
        .pipe(timeout(15000))
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }
}
