
import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { ApiService } from './../api.service';

@Injectable()
export class PagePermissionService {

  constructor(private api: ApiService) { }

  /**
   * Busca todas as páginas
   */
  public getAll() {
    return new Promise((resolve, reject) => {
      this.api.get('acl/page')
        .pipe(timeout(15000))
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }
}
