import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class ApiService {
  private apiUrl = environment.api_url;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(public http: HttpClient) {
  }

  public get(endpoint: string, params?: any) {
    let httpParams = new HttpParams();

    if (params) {
      Object.keys(params).forEach(function (key) {
        httpParams = httpParams.append(key, params[key]);
      });
    }

    return this.http.get(this.apiUrl + endpoint, {headers: this.headers, params: httpParams});
  }

  public post(endpoint: string, body: any, params?: any) {
    let httpParams = new HttpParams();

    if (params) {
      Object.keys(params).forEach(function (key) {
        httpParams = httpParams.append(key, params[key]);
      });
    }

    return this.http.post(this.apiUrl + endpoint, body, {headers: this.headers, params: httpParams});
  }

  public put(endpoint: string, body: any) {
    return this.http.put(this.apiUrl + endpoint, body, {headers: this.headers});
  }

  public delete(endpoint: string) {
    return this.http.delete(this.apiUrl + endpoint, {headers: this.headers});
  }

  public patch(endpoint: string, body: any) {
    return this.http.patch(this.apiUrl + endpoint, body, {headers: this.headers});
  }

}
