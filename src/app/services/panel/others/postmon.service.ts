// http://api.postmon.com.br/v1/cep/38840000

import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable()
export class PostmonService {

  constructor(public http: HttpClient) { }

  public getCep(cep) {
    return new Promise((resolve, reject) => {
      this.http.get('http://api.postmon.com.br/v1/cep/' + cep)
      .pipe(timeout(15000))
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
