import { NgModule } from '@angular/core';

import { ApiService } from './panel/api.service';
import { AcessoService } from './panel/ihx/acesso.service';
import { AuthService } from './panel/auth.service';
import { AttributeTypeService } from './panel/products/atributes/types/type.service';
import { AttributeService } from './panel/products/atributes/attribute.service';
import { SectionService } from './panel/products/section/section.service';
import { SectionTypeService } from './panel/products/section/types/type.service';
import { UnitysService } from './panel/products/unitys.service';
import { ProductService } from './panel/products/product.service';
import { OfferService } from './panel/offers/offers.service';
import { OfferProductService } from './panel/offers/offersProducts.service';
import { OffersTypesService } from './panel/offers/offersType.service';
import { OffersMotiveService } from './panel/offers/offersMotive.service';
import { PartnersService } from './panel/partners/partners.service';
import { AdressesService } from './panel/partners/adresses/adresses.service';
import { EmailsService } from './panel/partners/emails/emails.service';
import { PhonesService } from './panel/partners/phones/phones.service';
import { PostmonService } from './panel/others/postmon.service';
import { UsersService } from './panel/users/users.service';
import { UploadService } from './panel/upload/upload.service';
import { ProductImageService } from './panel/products/images/images.service';
import { UserPermissionsService } from './panel/acl/user-permissions.service';
import { PageActionPermissionService } from './panel/acl/page-action.service';
import { PagePermissionService } from './panel/acl/pages.service';
import { AttributeTypeStyleService } from './panel/products/atributes/types/type-style.service';
import { ShoppingListService } from './panel/shopping-list/shopping-list.service';
import { PublicApiService } from './public/api.service';
import { PublicShoppingListService } from './public/shopping-list/shopping-list.service';

@NgModule({
  providers: [
    ApiService,
    PublicApiService,
    PublicShoppingListService,
    AuthService,
    PageActionPermissionService,
    PagePermissionService,
    UserPermissionsService,
    AcessoService,
    AttributeTypeStyleService,
    AttributeTypeService,
    AttributeService,
    SectionService,
    SectionTypeService,
    UnitysService,
    ProductService,
    ProductImageService,
    OfferService,
    OffersTypesService,
    OfferProductService,
    OffersMotiveService,
    PartnersService,
    AdressesService,
    EmailsService,
    PhonesService,
    UsersService,
    ShoppingListService,
    UploadService,
    PostmonService
  ]
})
export class ServiceModule {}
