
import { timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { PublicApiService } from '../api.service';

@Injectable()
export class PublicShoppingListService {

  constructor(private api: PublicApiService) { }

  public get(id: Number) {
    return new Promise((resolve, reject) => {
      this.api.get('shopping/list/' + id)
        .pipe(timeout(15000))
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }
}
