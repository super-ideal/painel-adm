import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import * as moment from 'moment';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ClickOutsideModule } from 'ng-click-outside';
import { ToastyModule } from 'ng2-toasty';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { PanelMenuModule } from 'primeng/panelmenu';

import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

import { SharedModule } from './shared/shared.module';

import { AuthComponent } from './layout/auth/auth.component';
import { AdminComponent } from './layout/admin/admin.component';
import { BreadcrumbsComponent } from './layout/utils/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layout/utils/title/title.component';
import { FooterComponent } from './layout/utils/footer/footer.component';
import { PublicComponent } from './layout/public/public.component';

import { ServiceModule } from './services/service.module';

import { AplicationErrorHandle } from './app.error-handle';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { RefreshTokenInterceptor } from './interceptors/refresh-token.interceptor';

import { AuthGuard } from './guards/auth.guard';
import { NoAuthGuard } from './guards/noauth.guard';
import { AclGuard } from './guards/acl.guard';

registerLocaleData(localePt, 'pt-BR');
moment.locale('pt-BR');

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    PublicComponent,
    AdminComponent,
    BreadcrumbsComponent,
    TitleComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    ToastyModule.forRoot(), // Mensagem Toast
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutes),
    SweetAlert2Module.forRoot(),
    SweetAlert2Module,
    ClickOutsideModule,
    SharedModule,
    ServiceModule,
    PanelMenuModule
  ],
  exports: [
    ToastyModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    AclGuard,
    NoAuthGuard,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RefreshTokenInterceptor, multi: true },
    { provide: ErrorHandler, useClass: AplicationErrorHandle },
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
