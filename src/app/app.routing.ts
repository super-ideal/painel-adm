import { Routes } from '@angular/router';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import { PublicComponent } from './layout/public/public.component';

import { AuthGuard } from './guards/auth.guard';
import { AclGuard } from './guards/acl.guard';

export const AppRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard, AclGuard],
    canActivateChild: [AuthGuard],
    canLoad: [AuthGuard],
    data: { logout: true, permission: { page: 'CONTROL_PANEL', action: 'ACCESS' } },
    children: [
      {
        path: '',
        redirectTo: 'dashboard/default',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'parceiros',
        loadChildren: './pages/partners/partners.module#PartnersModule'
      },
      {
        path: 'usuarios',
        loadChildren: './pages/users/users.module#UsersModule'
      },
      {
        path: 'permissoes',
        loadChildren: './pages/permissions/permissions-form.module#PermissionsFormModule'
      },
      {
        path: 'produtos',
        loadChildren: './pages/products/products.module#ProductsModule'
      },
      {
        path: 'ofertas',
        loadChildren: './pages/offers/offers.module#OffersModule'
      },
      {
        path: 'listas-compras',
        loadChildren: './pages/shopping-lists/shopping-lists.module#ShoppingListsModule'
      }
    ],
  }, {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'auth',
        loadChildren: './pages/authentication/authentication.module#AuthenticationModule'
      },
    ]
  }, {
    path: 'web',
    component: PublicComponent,
    children: [
      {
        path: 'lista-compra',
        loadChildren: './pages/public/public-details-shopping-list/public-details-shopping-list.module#PublicDetailsShoppingListModule'
      },
    ]
  },
  { path: '404', loadChildren: './pages/utils/not-found/not-found.module#NotFoundModule' },
  { path: '**', redirectTo: '/404' }
  // { path: '**', component: PaginaNaoEncontradaComponent } //, canActivate: [AuthGuard]}
];
