import { Routes } from '@angular/router';
import { ProductFormComponent } from './product-form.component';
import { AclGuard } from '../../../guards/acl.guard';

import { ProductsFormResolver } from './resolvers/products-form.resolver';
import { UnitysResolver } from './resolvers/unitys.resolver';
import { AttributesTypesResolver } from './resolvers/attributes-types.resolver';
import { SectionsTypesResolver } from './resolvers/sections-types.resolver';

const message = '';

export const ProductFormRoutes: Routes = [
  {
    path: '',
    component: ProductFormComponent,
    canActivate: [AclGuard],
    resolve: { unitysData: UnitysResolver, attributesTypesData: AttributesTypesResolver, sectionsTypesData: SectionsTypesResolver },
    data: { permission: { page: 'PRODUCTS', action: 'NEW' }, breadcrumb: 'Cadastro', message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: ProductFormComponent,
        canActivateChild: [AclGuard],
        resolve: { productData: ProductsFormResolver, unitysData: UnitysResolver, attributesTypesData: AttributesTypesResolver, sectionsTypesData: SectionsTypesResolver },
        data: { permission: { page: 'PRODUCTS', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' }
      },
      {
        path: ':id/editar',
        component: ProductFormComponent,
        canActivateChild: [AclGuard],
        resolve: { productData: ProductsFormResolver, unitysData: UnitysResolver, attributesTypesData: AttributesTypesResolver, sectionsTypesData: SectionsTypesResolver },
        data: { permission: { page: 'PRODUCTS', action: 'EDIT' }, breadcrumb: 'Edição' }
      },
    ]
  }
];
