
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'ng2-file-upload';

import { TableModule } from 'primeng/table';
import { ProgressBarModule } from 'primeng/progressbar';
import { DataViewModule } from 'primeng/dataview';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { SharedModule } from '../../../../../shared/shared.module';

import { UploadComponent } from './upload.component';
import { ThumbnailDirective } from '../../../../../directive/thumbnail.directive';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TableModule,
    ProgressBarModule,
    DataViewModule,
    OverlayPanelModule,
    FileUploadModule,
    SweetAlert2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [UploadComponent, ThumbnailDirective],
  exports: [UploadComponent]
})
export class UploadModule { }
