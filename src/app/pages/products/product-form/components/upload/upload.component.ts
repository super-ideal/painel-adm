import { Component, Output, EventEmitter, Input } from '@angular/core';
import { ToastOptions, ToastData, ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { FileUploader } from 'ng2-file-upload';

import { UploadService } from '../../../../../services/panel/upload/upload.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent {
  @Output()
  newImages: EventEmitter<any> = new EventEmitter<any>();

  imagesForSaveApi: any = [];
  imagesResultUploaded: any = [];
  urlApi: string;
  enabled = false;

  uploader: FileUploader = new FileUploader({});
  maxFileSize: number;
  allowedMimeType: Array<string> = [];
  hasAnotherDropZoneOver = false;
  itemOverlayImage: any;

  constructor(
    private toastyService: ToastyService,
    private _uploadService: UploadService
  ) {
    this.urlApi = this._uploadService.urlApi + 'image/';
    this.uploadImages();
  }

  @Input()
  set formEnabled(enabled) {
    this.enabled = enabled;
  }

  // Ajustes para upload
  private uploadImages(): void {
    this.allowedMimeType = ['capture=camera', 'image/png', 'image/gif', 'image/jpg', 'image/jpeg'];
    this.maxFileSize =  1 * 1024 * 1024; // 1 MB

    // Seta configuração
    this.uploader.setOptions({
      url: this.urlApi + 'product/',
      method: 'POST',
      itemAlias: 'product',
      isHTML5: true,
      removeAfterUpload: false,
      autoUpload: true,
      allowedMimeType: this.allowedMimeType,
      maxFileSize: this.maxFileSize
    });

    // Antes do upload
    this.uploader.onAfterAddingFile = (item => {
      item.withCredentials = false;
    });

    // Upload com sucesso
    this.uploader.onSuccessItem = (item: any, response: any, status: any, headers: any) => {
      const data = {
        filename: JSON.parse(response)[0]['filename'],
        fieldname: JSON.parse(response)[0]['fieldname'],
      };
      this.imagesForSaveApi.push(data);
      this.newImages.emit(this.imagesForSaveApi);

      this.imagesResultUploaded.push(JSON.parse(response)[0]);
    };

    this.uploader.onErrorItem = (item: any, response: any, status: any, headers: any) => {
      // Api de upload não está respondendo
      if (status === 0) {
        this.addToast('error', 'Erro ao Enviar', `Ocorreu um erro interno ao tentar enviar a imagem "${item.file.name}"`, 5000);
      }
    };

    // Upload com erro
    this.uploader.onWhenAddingFileFailed = (item: any, filter: any, options: any) => {
      switch (filter.name) {
        case 'fileSize':
          this.addToast('error', 'Erro', `Tamanho máximo de upload excedido (${item.size} de ${this.maxFileSize} permitido)`, 7000);
          break;
        case 'mimeType':
          const allowedTypes = this.allowedMimeType.join();
          this.addToast('error', 'Erro', `O tipo "${item.type}" não é permitido. Tipos permitidos: ${allowedTypes}"`, 7000);
          break;
        default:
          this.addToast('error', 'Erro', `Erro desconhecido (filtro: ${filter.name})`, 5000);
      }
    };
  }

  async deleteImage(image: any) {
    const fileIndex = await _.findIndex(this.imagesResultUploaded, {originalname: image['file']['name']});
    const file = this.imagesResultUploaded[fileIndex];

    if (file) {
      swal({
        title: 'Exclusão',
        text: `Você deseja realmente excluir a imagem "${file.originalname}"?`,
        type: 'warning',
        timer: 5000,
        showCancelButton: true,
        cancelButtonText: 'Não',
        confirmButtonText: 'Sim, excluir!'
      }).then(async (result) => {

        if (result.value) {
          await this._uploadService.deleteImage(file.fieldname, file.filename.toString())
          .then((res) => {
            this.uploader.removeFromQueue(image);
            this.imagesResultUploaded.splice(fileIndex, 1);
            this.imagesForSaveApi.splice(fileIndex, 1);

            this.addToast('success', 'Sucesso', 'Imagem excluída com sucesso!');
          }, err => {
            console.log(err);
          });
        }

      }).catch(swal.noop);
    }
  }

  fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  // Mostra preview da imagem na tabela de upload
  selectOverlayImageTableUpload(item) {
    this.itemOverlayImage = item;
  }

  /**
   * Função de tratamento de erros
   * @param {Object} err Objeto com os dados do erro
   */
  handleError(err: any): void {
    switch (err.status) {
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.message);
  }

   /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number): void {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
    }
  }
}
