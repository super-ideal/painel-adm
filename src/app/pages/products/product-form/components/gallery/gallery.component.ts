import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Lightbox } from 'ngx-lightbox';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import * as _ from 'lodash';
import swal from 'sweetalert2';

import { UploadService } from '../../../../../services/panel/upload/upload.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent {
  images: any = [];
  urlApi: string;
  enabled = false;
  messageNotFoundImages: [{severity: string, summary: string, detail: string}];

  deleteImagesGallery: any = [];

  @Output()
  deleteImages: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private lightbox: Lightbox,
    private toastyService: ToastyService,
    private _uploadService: UploadService,
  ) {
    this.urlApi = this._uploadService.urlApi + 'image/';
  }

  @Input()
  set formEnabled(enabled: boolean) {
    this.enabled = enabled;
  }

  @Input()
  set dataImages(data: any) {
    if (!data) {
      this.messageNotFoundImages = [{ severity: 'info', summary: 'Informação', detail: 'Este produto ainda não tem nenhuma imagem' }];
    } else {
      for (const image of data) {
        this.images.push({
          thumb: this.urlApi + image['fieldname'] + '/' + image['filename'] + '/?format=jpeg&quality=80&width=100',
          src: this.urlApi + image['fieldname'] + '/' + image['filename'] + '/?format=jpeg&quality=100',
          original: this.urlApi + image['fieldname'] + '/' + image['filename'],
          caption: image['filename'],
          id: image['id'],
          fieldname: image['fieldname'],
          filename: image['filename'],
          master: image['master'],
        });
      }
    }
  }

  /**
   * Mostra a imagem em tela grande, quando clica sobre ela na galeria
   * @param index Index dos dados da imagem no array de imagens
   */
  openLightBoxGallery(index: number): void {
    this.lightbox.open(this.images, index, { centerVertically: true });
  }

  /**
   * Método de exclusão da imagem
   */
  async deleteImage(image: any) {
    const fileIndex = await _.findIndex(this.images, {filename: image['filename']});
    const filename = this.images[fileIndex] ? this.images[fileIndex]['filename'] : null;

    if (filename) {
      swal({
        title: 'Exclusão',
        text: `Você deseja realmente excluir a imagem "${filename}"?`,
        type: 'warning',
        timer: 5000,
        showCancelButton: true,
        cancelButtonText: 'Não',
        confirmButtonText: 'Sim, excluir!'
      }).then(async (result) => {

        if (result.value) {
          this.deleteImagesGallery.push(this.images[fileIndex]);
          this.deleteImages.emit(this.deleteImagesGallery);
          this.images.splice(fileIndex, 1);
        }

      }).catch(swal.noop);
    }
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number): void {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
    }
  }
}
