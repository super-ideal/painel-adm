
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { LightboxModule } from 'ngx-lightbox';
import { PanelModule } from 'primeng/panel';

import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';

import { SharedModule } from '../../../../../shared/shared.module';

import { GalleryComponent } from './gallery.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PanelModule,
    SweetAlert2Module,
    LightboxModule,
    MessageModule,
    MessagesModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [GalleryComponent],
  exports: [GalleryComponent]
})
export class GalleryModule { }
