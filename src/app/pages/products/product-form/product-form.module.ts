
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { MessagesModule } from 'primeng/primeng';

import { SharedModule } from '../../../shared/shared.module';

import { ProductFormComponent } from './product-form.component';
import { ProductFormRoutes } from './products-form.routing';

import { GalleryModule } from './components/gallery/gallery.module';
import { UploadModule } from './components/upload/upload.module';
import { ProductsFormResolver } from './resolvers/products-form.resolver';
import { UnitysResolver } from './resolvers/unitys.resolver';
import { AttributesTypesResolver } from './resolvers/attributes-types.resolver';
import { SectionsTypesResolver } from './resolvers/sections-types.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductFormRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    InputSwitchModule,
    InputTextareaModule,
    AutoCompleteModule,
    DropdownModule,
    MultiSelectModule,
    CurrencyMaskModule,
    TabViewModule,
    MessagesModule,
    SweetAlert2Module,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    GalleryModule,
    UploadModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ProductFormComponent],
  providers: [ProductsFormResolver, UnitysResolver, AttributesTypesResolver, SectionsTypesResolver]
})
export class ProductFormModule { }
