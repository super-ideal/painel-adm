import { Component, OnInit, Renderer2 } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { Message } from 'primeng/api';
import * as _ from 'lodash';

import { Product } from '../../../interfaces/product/product.interface';

import { ProductService } from '../../../services/panel/products/product.service';
import { OfferProductService } from '../../../services/panel/offers/offersProducts.service';
import { UploadService } from '../../../services/panel/upload/upload.service';
import { ProductImageService } from '../../../services/panel/products/images/images.service';
import { VerifyPermissions } from '../../../shared/verify-permissions/verify-permissions';

declare var $: any;

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  permissionsUserLogged: any;

  data: Product;
  data_old: Product;
  unitys: any[];
  attributesTypes: any[];
  sectionTypes: any[];
  dataImages: any = [];

  images = {
    new: [],
    delete: []
  };

  form: FormGroup;
  form_product: FormGroup;
  form_attributes: FormGroup;
  form_section: FormGroup;

  buttonDelete: Button = {visible: false, disabled: false};
  buttonEdit: Button = {visible: false, disabled: false};
  buttonSave: Button = {visible: false, disabled: false};
  buttonCancel: Button = {visible: false, disabled: false};

  msgs: Message[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private renderer: Renderer2,
    private location: Location,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _productService: ProductService,
    private _offerProductService: OfferProductService,
    private _uploadService: UploadService,
    private _productImageService: ProductImageService
  ) {
    this.form = new FormGroup({
      product: this.form_product = new FormGroup({
        active: new FormControl(true),
        description: new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(50)]),
        barcode: new FormControl('', Validators.required),
        description_long: new FormControl(null),
        price: new FormControl(null, Validators.required),
        unity_id: new FormControl(null, Validators.required),
      }),
      attributes: this.form_attributes = new FormGroup({}),
      section: this.form_section = new FormGroup({})
    });

    this.images = {
      new: [],
      delete: []
    };
  }

  /** Executa apenas na primeira vez que a página foi carregada */
  ngOnInit() {
    this.disableForm();

    this.route.data.subscribe(async data => {
      this.attributesTypes = data['attributesTypesData'];
      if (this.attributesTypes) {
        await this.verifyAttributesTypes();
      }

      this.sectionTypes = data['sectionsTypesData'];
      if (this.sectionTypes) {
        await this.verifySectionTypes();
      }

      this.unitys = data['unitysData'];
      this.data = data['productData'];
      this.data_old = data['productData'];

      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões

      switch (data.permission.action) {
        case 'VIEW_ONE': {
          this.setDataForm();
          this.verifyProductsInOffersProgress(this.data['id']);
          $('textarea').froalaEditor('edit.off');
          break;
        }
        case 'EDIT': {
          this.setDataForm();
          this.verifyProductsInOffersProgress(this.data['id']);
          this.enableForm();
          this.focusElement('description');
          break;
        }
        case 'NEW': {
          this.enableForm();
          this.focusElement('barcode');
          break;
        }
        default: {
          this.goBack();
          break;
        }
      }
    });
  }

  private setDataForm(): void {
    this.form_product.patchValue({
      active: this.data['active'],
      barcode: this.data['barcode'],
      description: this.data['description'],
      description_long: this.data['description_long'],
      price: this.data['price'],
      unity_id: this.data['unity']
    });

    this.dataImages = this.data['images'];

    // Organiza as Seções
    for (const a of this.data['sectionProduct']) {
      this.form_section.patchValue({
        [a.type_id]: a
      });
    }

    // Organiza os Atributos
    for (const a of this.data['attributesProduct']) {
      const filtro = _.filter(this.data['attributesProduct'], { type_id: a['type_id'] });
      this.form_attributes.patchValue({
        [a.type_id]: a['type']['multiple'] === true ? filtro : filtro[0]
      });
    }
  }

  /**
   * Organiza os atributos
   */
  private verifyAttributesTypes(): void {
    for (const type of this.attributesTypes) {
      if (type.required) {
        this.form_attributes.addControl(type.id, new FormControl({ value: null, disabled: true }, [Validators.required]));
      } else {
        this.form_attributes.addControl(type.id, new FormControl({ value: null, disabled: true }));
      }
    }
  }

  /**
   * Organiza os secoes
   */
  private verifySectionTypes(): void {
    for (const type of this.sectionTypes) {
      if (type.required) {
        this.form_section.addControl(type.id, new FormControl({ value: null, disabled: true }, [Validators.required]));
      } else {
        this.form_section.addControl(type.id, new FormControl({ value: null, disabled: true }));
      }
    }
  }

  /**
   * Método que verifica se está atualizando ou adicionando um produto
   */
  async save() {
    if (this.form.invalid) {
      this.addToast('error', 'Erro', 'Formulário inválido. Verifique os dados e tente novamente!');
      return;
    }

    this.addToast('wait', 'Aguarde...', 'Estamos processando sua solicitação!', 15000, 100);
    const form = this.form.value;
    form['product']['unity_id'] = this.form_product.value.unity_id['id'];
    form['images'] = this.images.new;

    if (this.data) {
      await this.updateProduct(form);
    } else {
      await this.addProduct(form);
    }

    this.toastyService.clear(100);
  }

  /**
   * Verifica se o produto está em alguma agenda de oferta em andamento
   * @param {number} idProduct ID do produto
   */
  async verifyProductsInOffersProgress(idProduct: Number) {
    await this._offerProductService.verifyProductsInOffersProgress('product_id', idProduct).then((result) => {
      const unionIdOffers = [];
      for (const offer of result['data']) {
        unionIdOffers.push(_.at(offer, 'offer_id'));
      }

      // Informação que o produto está em oferta
      let msg = 'Este produto está em oferta na agenda: ';
      if (unionIdOffers.length !== 1) {
        msg = 'Este produto está em oferta nas agendas: ';
      }

      // Desabilita os campos que não podem ser editados
      const indexPriceGroup = _.findIndex(this.attributesTypes, { 'id_unique': 'price_group' });
      this.form_attributes.get((indexPriceGroup + 1).toString()).disable();
      this.form_product.get('price').disable();

      const idOffers = _.flattenDeep(unionIdOffers);
      return this.msgs = [{ severity: 'info', summary: 'Oferta', detail: `${msg} ${idOffers}` }];
    }, (err) => {
      if (err.status !== 404) {
        this.addToast('error', 'Ops...', 'Ocorreu um erro ao verificar se o produto está em oferta.');
      }
    });
  }

  /**
   * Verifica se o código de barras informado já existe na base da dados
   * @param {any} value Código de barras
   */
  verifyBarcodeDuplicated(value) {
    if (!!value.length) {
      if (this.data_old && this.data_old.barcode !== value.trim()) {
        this._productService.verifyBarcodeDuplicated(value).then(() => {
          this.form_product.get('barcode').setErrors({'duplicity': false});
          this.form_product.patchValue({barcode: value.trim()});
        }, (err) => {
          if (err.status === 422) {
            return this.form_product.get('barcode').setErrors({'duplicity': true});
          }
        });
      }
    }
  }

  // Adiciona
  async addProduct(data: any) {
    await this._productService.post(data)
    .then(async (result) => {
      await this.deleteImagesGalleryApi();
      return this.goProduct(result['data']['id'], 'success', 'Sucesso', 'O produto foi salvo com sucesso!');
    }, (err) => this.handleError(err));
  }

  // Atualiza
  async updateProduct(data: object) {
    await this._productService.update(data, this.data.id)
    .then(async () => {
      await this.deleteImagesGalleryApi();
      return this.goProduct(Number(this.data.id), 'success', 'Sucesso', 'O produto foi atualizado com sucesso!');
    }, (err) => this.handleError(err));
  }

  /**
   * Seta na variavel as imagens que serão adicionadas ao produto
   * A adição somente será efetuada se o usuário gravar as alterações
   * @param data Array com os dados das novas imagens
   */
  setNewImagesUploaded(data: any): void {
    this.images.new = data;
  }

  /**
   * Seta na variavel as imagens que é para serem excluídas
   * A exclusão somente será efetuada se o usuário gravar as alterações
   * @param data Array de imagens que serão excluídas
   */
  setDeleteImagesGallery(data: any): void {
    this.images.delete = data;
  }

  /**
   * Método que exclui as imagens do servidor
   */
  // Modificar para que isso seja feito direto pela API ao atualizar, excluir ou adicionar um novo produto
  async deleteImagesGalleryApi() {
    if (this.images.delete) {
      for (const image of this.images.delete) {
        // Delete a imagem da API de upload
        await this._uploadService.deleteImage(image.fieldname.toString(), image.filename.toString())
        .then(async () => {

          // Delete a imagem do Banco de dados
          await this._productImageService.delete(image.id)
          .then(() => {
            this.addToast('success', 'Sucesso', 'Imagem excluída com sucesso!');
          }, (err) => { console.log(err); });

        }, err => { console.log(err); });
      }
    }
  }

  /**
   * Configurações do editor da descrição longa do produto
   * https://www.froala.com/wysiwyg-editor/docs/concepts/image/upload
   * https://www.froala.com/wysiwyg-editor/docs/options
   */
  // tslint:disable-next-line:member-ordering
  public optionsEditor: Object = {
    charCounterCount: true,
    language: 'pt_br',
    placeholderText: 'Digite uma descrição bem detalhada sobre o produto...',
    heightMin: 200,
    htmlRemoveTags: ['script', 'base'],
    iconsTemplate: 'font_awesome_5',
    toolbarButtons: [
      'fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', '|',
      'fontFamily', 'fontSize', 'color', 'paragraphStyle', '|',
      'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|',
      'insertVideo', 'insertImage', 'insertLink', '|',
      'emoticons', 'insertHR', 'selectAll', 'clearFormatting', 'html', 'undo', 'redo'
    ],
    fontSize: ['8', '10', '12', '14', '18'],
    fontSizeDefaultSelection: '14',
    imageUpload: false,
    imageUploadRemoteUrls: false,
    imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif'],
    imageUploadMethod: 'POST',
    imageUploadURL: '/upload_image',
    imageUploadParam: 'file_name',
    imageUploadParams: { id: 'my_editor' },
    imageManagerLoadMethod: 'GET',
    imageManagerLoadURL: 'http://example.com/load_images',
    imageManagerLoadParams: { user_id: 4219762 },
    imageManagerDeleteMethod: 'DELETE',
    imageManagerDeleteURL: 'http://example.com/delete_image',
    imageManagerDeleteParams: { user_id: 4219762 },
    imageManagerPreloader: '/images/loader.gif',
    videoUpload: false,
    fileUpload: false,
    pluginsEnabled: ['align', 'charCounter', 'codeBeautifier', 'codeView', 'colors', 'draggable', 'embedly', 'emoticons', 'entities', 'file', 'fontFamily', 'fontSize', 'fullscreen', 'image', 'imageManager', 'inlineStyle', 'lineBreaker', 'link', 'lists', 'paragraphFormat', 'paragraphStyle', 'quote', 'save', 'table', 'url', 'video', 'wordPaste'],
    events : {
      'froalaEditor.initialized': function(e, editor) {
        // editor.edit.off();
      }
    }
  };

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableForm(): void {
    this.buttonEdit.visible = false;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = true;
    this.buttonCancel.visible = true;
    this.form.enable();
    $('textarea').froalaEditor('edit.on');
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableForm(): void {
    this.buttonEdit.visible = true;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = false;
    this.buttonCancel.visible = false;
    this.form.disable();
  }

  /**
   * Volta para uma determinada página
   */
  goBack(goList: boolean = false, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    if (goList === true) {
      this.router.navigate(['/produtos']);
    } else {
      this.location.back();
    }
  }

  goProduct(idProduct: number, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    this.router.navigate(['/produtos/cadastro/' + idProduct]);
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.error.code) {
      case 'ER_DUP_ENTRY': err.error.message = 'O Código de Barras informado já esta cadastrado.'; break;
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.error.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
