import { Routes } from '@angular/router';
import { ProductsComponent } from './products.component';
import { AclGuard } from '../../guards/acl.guard';
import { ProductsResolver } from './resolvers/products.resolver';

const breadcrumb = 'Produtos';
const message = '';

export const ProductsRoutes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    pathMatch: 'full',
    canActivate: [AclGuard],
    resolve: { productsData: ProductsResolver },
    data: { permission: { page: 'PRODUCTS', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: 'atributos',
        loadChildren: './attributes/attributes.module#AttributesModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ATTRIBUTES', action: 'VIEW_ALL' } }
      },
      {
        path: 'secoes',
        loadChildren: './section/section.module#SectionModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ADDRESS', action: 'VIEW_ALL' } }
      },
      {
        path: 'cadastro',
        loadChildren: './product-form/product-form.module#ProductFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCTS', action: 'VIEW_ONE' } }
      },
    ]
  }
];
