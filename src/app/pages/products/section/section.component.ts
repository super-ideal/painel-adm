import { Component, OnInit } from '@angular/core';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { SelectItem } from 'primeng/api';

import { SectionService } from '../../../services/panel/products/section/section.service';

import { Pagination } from '../../../interfaces/pagination.interface';
import { Section } from '../../../interfaces/product/section/section.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { VerifyPermissions } from '../../../shared/verify-permissions/verify-permissions';

interface HeaderTable {
  field: string;
  header: string;
}

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {
  permissionsUserLogged: any;

  whereBy;
  whereTextSearch = '';
  selectSearch: SelectItem[];

  table: {
    cols: HeaderTable[],
    dataSection: Section[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number
  };
  selectedRowTable: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _sectionService: SectionService
  ) {
    this.table = {
      cols: [
        { field: 'id', header: 'Id' },
        { field: 'active', header: 'Ativo' },
        { field: 'name', header: 'Nome' },
        { field: 'description', header: 'Descrição' },
        { field: 'type_name', header: 'Tipo' },
        { field: 'updated_at', header: 'Atualizado em' }
      ],
      dataSection: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10
    };

    this.selectSearch = [
      { label: 'Nome', value: 'name' },
      { label: 'Descrição', value: 'description' } ,
      { label: 'Tipo', value: 'type_name' },
      { label: 'Ativo', value: 'active' },
    ];
    this.whereBy = 'name';
  }

  ngOnInit() {
    this.route.data.subscribe(async data => {
      if (data['sectionData']) {
        this.table.elementsPagination = await data['sectionData'];
        this.table.dataSection = await data['sectionData']['data'];
      }
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões
    });
  }

   /**
   * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
   */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllSection();
  }

  /**
   * Realiza uma busca manual
   */
  searchManual() {
    let textSearch = this.whereTextSearch.trim();
    if (this.whereBy === 'active') {
      // tslint:disable-next-line:max-line-length
      if (textSearch === 'sim' || textSearch === 's') { textSearch = '1'; } else if (textSearch === 'nao' || textSearch === 'não' || textSearch === 'n') { textSearch = '0'; }
    }

    this.getAllSection(textSearch, this.whereBy);
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  getAllSection(wText: string = '', wBy: string = 'name') {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando alguns dados para você!', 15000, 100);

    this._sectionService.getAll(this.table.pageNumber, this.table.rowsOnPage, wBy, wText).then(data => {
      this.table.elementsPagination = data['data'];
      this.table.dataSection = data['data']['data'];

      this.toastyService.clear(100);
    }, (err) => {
      if (err.status === 404) {
        return this.handleError(err, 'warning', 'Atenção');
      }
      this.handleError(err);
    });
  }

  goToViewDetails(id) {
    this.router.navigate(['cadastro/' + id], { relativeTo: this.route });
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any, type: string = 'error', title: string = 'Erro') {
    this.toastyService.clearAll();

    switch (err.error.code && err.name && err.status) {
      case 404: err.error.message = 'Não encontramos nenhuma seção cadastrada'; break;
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    this.addToast(type, title, err.error.message);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
