import { Routes } from '@angular/router';
import { SectionComponent } from './section.component';
import { SectionsResolver } from './sections.resolver';
import { AclGuard } from '../../../guards/acl.guard';

const breadcrumb = 'Seções';
const message = '';

export const SectionRoutes: Routes = [
  {
    path: '',
    component: SectionComponent,
    canActivate: [AclGuard],
    data: { permission: { page: 'PRODUCT_ADDRESS', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    resolve: { sectionData: SectionsResolver }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: 'tipos',
        loadChildren: './type-section/type-section.module#TypeSectionModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ADDRESS_TYPES', action: 'VIEW_ALL' } }
      },
      {
        path: 'cadastro',
        loadChildren: './section-form/section-form.module#SectionFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ADDRESS', action: 'VIEW_ONE' } }
      },
    ]
  }
];
