import { Routes } from '@angular/router';
import { TypeSectionComponent } from './type-section.component';
import { TypesSectionsResolver } from './resolvers/types-sections.resolver';
import { AclGuard } from '../../../../guards/acl.guard';

const breadcrumb = 'Tipos de Seções';
const message = '';

export const TypeRoutes: Routes = [
  {
    path: '',
    component: TypeSectionComponent,
    canActivate: [AclGuard],
    resolve: { typeSectionData: TypesSectionsResolver },
    data: { permission: { page: 'PRODUCT_ADDRESS_TYPES', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: 'cadastro',
        loadChildren: './type-section-form/type-section-form.module#TypeSectionFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ADDRESS_TYPES', action: 'VIEW_ONE' } }
      },
    ]
  }
];
