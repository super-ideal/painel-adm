import { Routes } from '@angular/router';
import { TypeSectionFormComponent } from './type-section-form.component';
import { TypeSectionFormResolver } from './resolvers/type-section-form.resolver';
import { AclGuard } from '../../../../../guards/acl.guard';

const message = '';

export const TypeFormRoutes: Routes = [
  {
    path: '',
    component: TypeSectionFormComponent,
    canActivateChild: [AclGuard],
    data: { permission: { page: 'PRODUCT_ADDRESS_TYPES', action: 'NEW' }, breadcrumb: 'Cadastro', message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: TypeSectionFormComponent,
        resolve: { typeData: TypeSectionFormResolver },
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ADDRESS_TYPES', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' }
      },
      {
        path: ':id/editar',
        component: TypeSectionFormComponent,
        resolve: { typeData: TypeSectionFormResolver },
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ADDRESS_TYPES', action: 'EDIT' }, breadcrumb: 'Edição' }
      },
    ]
  }
];
