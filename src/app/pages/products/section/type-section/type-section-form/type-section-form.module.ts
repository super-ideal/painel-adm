import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { MultiSelectModule } from 'primeng/multiselect';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';

import { SharedModule } from '../../../../../shared/shared.module';

import { TypeSectionFormComponent } from './type-section-form.component';
import { TypeFormRoutes } from './type-section-form.routing';
import { TypeSectionFormResolver } from './resolvers/type-section-form.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(TypeFormRoutes),
    SharedModule,
    MultiSelectModule,
    InputSwitchModule,
    InputTextareaModule,
    DropdownModule,
    SweetAlert2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [TypeSectionFormComponent],
  providers: [TypeSectionFormResolver]
})
export class TypeSectionFormModule { }
