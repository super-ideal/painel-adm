import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';

import { SharedModule } from '../../../../shared/shared.module';
import { TypeSectionComponent } from './type-section.component';
import { TypeRoutes } from './type-section.routing';
import { TypesSectionsResolver } from './resolvers/types-sections.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(TypeRoutes),
    SharedModule,
    TableModule,
    PaginatorModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [TypeSectionComponent],
  providers: [TypesSectionsResolver]
})
export class TypeSectionModule { }
