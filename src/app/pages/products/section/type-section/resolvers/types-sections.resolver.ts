import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';

import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { SectionTypeService } from '../../../../../services/panel/products/section/types/type.service';

@Injectable()
export class TypesSectionsResolver implements Resolve<any> {

  constructor(
    private location: Location,
    private toastyService: ToastyService,
    private _sectionTypeService: SectionTypeService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this._sectionTypeService.getAllTypes({ pagination: true })
      .then(res => res['data'], (err) => {
        if (err.status !== 404) {
          this.addToast('error', 'Ops...', 'Ocorreu um erro interno no servidor. Tente novamente mais tarde!');
          this.location.back();
        }
      });
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
