import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';

import { SharedModule } from '../../../shared/shared.module';
import { SectionComponent } from './section.component';
import { SectionRoutes } from './section.routing';
import { SectionsResolver } from './sections.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SectionRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TableModule,
    PaginatorModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [SectionComponent],
  providers: [SectionsResolver]
})
export class SectionModule { }
