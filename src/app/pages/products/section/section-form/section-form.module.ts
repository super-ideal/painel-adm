import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';

import { SharedModule } from '../../../../shared/shared.module';

import { SectionFormComponent } from './section-form.component';
import { SectionFormRoutes } from './section-form.routing';
import { SectionFormResolver } from './resolvers/section-form.resolver';
import { SectionTypesResolver } from './resolvers/section-types.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SectionFormRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    InputSwitchModule,
    InputTextareaModule,
    DropdownModule,
    SweetAlert2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [SectionFormComponent],
  providers: [SectionFormResolver, SectionTypesResolver]
})
export class SectionFormModule { }
