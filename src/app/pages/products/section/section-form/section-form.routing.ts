import { Routes } from '@angular/router';
import { SectionFormComponent } from './section-form.component';
import { SectionFormResolver } from './resolvers/section-form.resolver';
import { AclGuard } from '../../../../guards/acl.guard';
import { SectionTypesResolver } from './resolvers/section-types.resolver';

const message = '';

export const SectionFormRoutes: Routes = [
  {
    path: '',
    component: SectionFormComponent,
    canActivateChild: [AclGuard],
    resolve: { typesData: SectionTypesResolver },
    data: { permission: { page: 'PRODUCT_ADDRESS', action: 'NEW' }, breadcrumb: 'Cadastro', message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: SectionFormComponent,
        resolve: { sectionData: SectionFormResolver, typesData: SectionTypesResolver },
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ADDRESS', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' }
      },
      {
        path: ':id/editar',
        component: SectionFormComponent,
        resolve: { sectionData: SectionFormResolver, typesData: SectionTypesResolver },
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ADDRESS', action: 'EDIT' }, breadcrumb: 'Edição' }
      },
    ]
  }
];
