import { Component, OnInit } from '@angular/core';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/api';

import { ProductService } from '../../services/panel/products/product.service';

import { Product } from '../../interfaces/product/product.interface';
import { Pagination } from '../../interfaces/pagination.interface';
import { VerifyPermissions } from '../../shared/verify-permissions/verify-permissions';
import { UploadService } from 'src/app/services/panel//upload/upload.service';

interface HeaderTable {
  field: string;
  header: string;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  permissionsUserLogged: any;
  itemOverlayImage: any;
  urlApi: string;

  whereBy;
  whereTextSearch = '';
  selectSearch: SelectItem[];

  table: {
    cols: HeaderTable[],
    dataProducts: Product[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number
  };
  selectedRowTable: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _productService: ProductService,
    private _uploadService: UploadService
  ) {
    this.urlApi = this._uploadService.urlApi + 'image/';

    this.table = {
      cols: [
        { field: 'id', header: 'Id' },
        { field: 'active', header: 'Ativo Venda' },
        { field: 'barcode', header: 'Cód. Barras' },
        { field: 'description', header: 'Descrição Curta' },
        { field: 'updated_at', header: 'Atualizado em' }
      ],
      dataProducts: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10
    };

    this.selectSearch = [
      { label: 'Descrição', value: 'description' },
      { label: 'Cód. Barras', value: 'barcode' },
      { label: 'Ativo', value: 'active' },
    ];
    this.whereBy = 'description';
  }

  ngOnInit() {
    this.route.data.subscribe(async data => {
      if (data['productsData']) {
        this.table.elementsPagination = await data['productsData'];
        this.table.dataProducts = await data['productsData']['data'];
      }
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões
    });
  }

  // Mostra preview da imagem na tabela de upload
  selectOverlayImageTableUpload(item) {
    let image = item['images'][0];

    if (image) {
      image = this.urlApi + image['fieldname'] + '/' + image['filename'] + '/?format=jpeg&quality=80&width=100';
      this.itemOverlayImage = image;
    }
  }

   /**
   * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
   */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllProducts();
  }

  /**
   * Realiza uma busca manual
   */
  searchManual() {
    let textSearch = this.whereTextSearch.trim();
    if (this.whereBy === 'active') {
      // tslint:disable-next-line:max-line-length
      if (textSearch === 'sim' || textSearch === 's') { textSearch = '1'; } else if (textSearch === 'nao' || textSearch === 'não' || textSearch === 'n') { textSearch = '0'; }
    }

    this.getAllProducts(textSearch, this.whereBy);
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  getAllProducts(wText: string = '', wBy: string = 'description') {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando alguns dados para você!', 15000, 100);

    this._productService.getAll({
      pageNumber: this.table.pageNumber,
      perPage: this.table.rowsOnPage,
      whereBy: wBy,
      whereText: wText
    }).then(data => {
      this.table.elementsPagination = data['data'];
      this.table.dataProducts = data['data']['data'];

      this.toastyService.clear(100);
    }, (err) => {
      if (err.status === 404) {
        return this.handleError(err, 'warning', 'Atenção');
      }
      this.handleError(err);
    });
  }

  goToViewDetails(id) {
    this.router.navigate(['cadastro/' + id], { relativeTo: this.route });
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any, type: string = 'error', title: string = 'Erro') {
    this.toastyService.clearAll();
    console.log(err);

    switch (err.name && err.status) {
      case 404: err.message = 'Não encontramos nenhum produto com os dados informados'; break;
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    this.addToast(type, title, err.message);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
