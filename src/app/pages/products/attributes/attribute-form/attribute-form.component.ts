import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';

import { AttributeService } from '../../../../services/panel/products/atributes/attribute.service';

import { VerifyPermissions } from '../../../../shared/verify-permissions/verify-permissions';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

@Component({
  selector: 'app-attribute-form',
  templateUrl: './attribute-form.component.html',
  styleUrls: ['./attribute-form.component.scss']
})
export class AttributeFormComponent implements OnInit {
  permissionsUserLogged: any;

  form: FormGroup;
  data: any;
  types: any[];
  typeSelected: any;

  buttonDelete: Button = {visible: false, disabled: false};
  buttonEdit: Button = {visible: false, disabled: false};
  buttonSave: Button = {visible: false, disabled: false};
  buttonCancel: Button = {visible: false, disabled: false};

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private renderer: Renderer2,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _attributeService: AttributeService
  ) {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      description: ['', [Validators.maxLength(255)]],
      active: [true],
      type_id: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.disableForm();

    this.route.data.subscribe(async data => {
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões
      this.data = data['attributeData'];
      this.types = data['typesData'];

      switch (data.permission.action) {
        case 'VIEW_ONE': {
          this.setDataForm();
          break;
        }
        case 'EDIT': {
          this.setDataForm();
          this.enableForm();
          this.focusElement('name');
          break;
        }
        case 'NEW': {
          this.enableForm();
          this.focusElement('name');
          break;
        }
        default: {
          this.goBack();
          break;
        }
      }
    });
  }

  private setDataForm() {
    this.form.patchValue({
      active: this.data['active'],
      name: this.data['name'],
      description: this.data['description'],
      type_id: this.data['type']
    });

    this.type_selected(this.data['type']);
  }

  type_selected(obj: any): void {
    this.typeSelected = obj;
  }

  async save() {
    if (this.form.invalid) {
      this.addToast('error', 'Erro', 'Formulário inválido. Verifique os dados e tente novamente!');
      return;
    }

    this.addToast('wait', 'Aguarde...', 'Estamos processando sua solicitação!', 15000, 100);
    this.form.value['type_id'] = this.form.value.type_id['id'];

    if (this.data) {
      await this.updateAtt(this.form.value);
    } else {
      await this.addAtt(this.form.value);
    }

    this.toastyService.clear(100);
  }

   /**
   * Crud
   */
  async addAtt(data: object) {
    await this._attributeService.post(data).then((result) => {
      this.goBack(false, 'success', 'Sucesso', 'O Atributo foi salvo com sucesso!');
    }, (err) => this.handleError(err));
  }

  async updateAtt(data: object) {
    await this._attributeService.update(data, this.data.id).then((result) => {
      this.goBack(false, 'success', 'Sucesso', 'O Atributo foi atualizado com sucesso!');
    }, (err) => this.handleError(err));
  }

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableForm(): void {
    this.buttonEdit.visible = false;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = true;
    this.buttonCancel.visible = true;
    this.form.enable();
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableForm(): void {
    this.buttonEdit.visible = true;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = false;
    this.buttonCancel.visible = false;
    this.form.disable();
  }

  /**
   * Volta para uma determinada página
   * @param {*} goList Volta para a listagem
   */
  goBack(goList: boolean = false, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    if (goList === true) {
      this.router.navigate(['/produtos/atributos']);
    } else {
      this.location.back();
    }
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.error.code) {
      case 'ER_DUP_ENTRY': err.error.message = 'O nome informado já esta cadastrado.'; break;
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.error.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
    * Toast de alertas
    * @param {string} type Tipo do toast (success, error, warning, wait)
    * @param {string} title Título do toast
    * @param {string} message Mensagem do toast
    * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
    * @param {string} [idToast] Id único para o toast
    */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
