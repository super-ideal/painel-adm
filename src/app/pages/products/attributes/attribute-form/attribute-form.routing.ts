import { Routes } from '@angular/router';
import { AttributeFormComponent } from './attribute-form.component';
import { AclGuard } from '../../../../guards/acl.guard';
import { AttributeFormResolver } from './resolvers/attribute-form.resolver';
import { AttributeTypesResolver } from './resolvers/attribute-types.resolver';

export const AttributeFormRoutes: Routes = [
  {
    path: '',
    component: AttributeFormComponent,
    canActivateChild: [AclGuard],
    resolve: { typesData: AttributeTypesResolver },
    data: { permission: { page: 'PRODUCT_ATTRIBUTES', action: 'NEW' }, breadcrumb: 'Cadastro', icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: AttributeFormComponent,
        canActivateChild: [AclGuard],
        resolve: { attributeData: AttributeFormResolver, typesData: AttributeTypesResolver },
        data: { permission: { page: 'PRODUCT_ATTRIBUTES', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' }
      },
      {
        path: ':id/editar',
        component: AttributeFormComponent,
        canActivateChild: [AclGuard],
        resolve: { attributeData: AttributeFormResolver, typesData: AttributeTypesResolver },
        data: { permission: { page: 'PRODUCT_ATTRIBUTES', action: 'EDIT' }, breadcrumb: 'Edição' }
      },
    ]
  }
];
