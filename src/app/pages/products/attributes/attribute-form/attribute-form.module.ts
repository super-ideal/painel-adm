import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';

import { SharedModule } from '../../../../shared/shared.module';

import { AttributeFormComponent } from './attribute-form.component';
import { AttributeFormRoutes } from './attribute-form.routing';
import { AttributeFormResolver } from './resolvers/attribute-form.resolver';
import { AttributeTypesResolver } from './resolvers/attribute-types.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AttributeFormRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    InputSwitchModule,
    InputTextareaModule,
    DropdownModule,
    SweetAlert2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [AttributeFormComponent],
  providers: [AttributeFormResolver, AttributeTypesResolver]
})
export class AttributeFormModule { }
