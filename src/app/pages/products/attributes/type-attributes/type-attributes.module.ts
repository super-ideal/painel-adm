import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';

import { SharedModule } from '../../../../shared/shared.module';

import { TypeAttributesComponent } from './type-attributes.component';
import { TypesAttributesRoutes } from './type-attributes.routing';
import { TypeAttributesResolver } from './resolvers/type-attributes.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(TypesAttributesRoutes),
    SharedModule,
    TableModule,
    PaginatorModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [TypeAttributesComponent],
  providers: [TypeAttributesResolver]
})
export class TypeAttributesModule { }
