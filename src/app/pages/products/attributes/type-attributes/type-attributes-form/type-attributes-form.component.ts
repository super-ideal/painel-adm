import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import * as _ from 'lodash';

import { AttributeTypeService } from '../../../../../services/panel/products/atributes/types/type.service';

import { TypeAttribute } from '../../../../../interfaces/product/attributes/type/type-attribute.interface';
import { VerifyPermissions } from '../../../../../shared/verify-permissions/verify-permissions';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

@Component({
  selector: 'app-type-form',
  templateUrl: './type-attributes-form.component.html',
  styleUrls: ['./type-attributes-form.component.scss']
})
export class TypeAttributesFormComponent implements OnInit {
  permissionsUserLogged: any;

  form: FormGroup;
  typeData: TypeAttribute;
  styles: any[];

  buttonDelete: Button = {visible: false, disabled: false};
  buttonEdit: Button = {visible: false, disabled: false};
  buttonSave: Button = {visible: false, disabled: false};
  buttonCancel: Button = {visible: false, disabled: false};
  editable: Boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private renderer: Renderer2,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _typeService: AttributeTypeService,
  ) {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      description: ['', [Validators.maxLength(255)]],
      active: [true],
      required: [false],
      public: [true],
      style_id: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.disableForm();

    this.route.data.subscribe(async data => {
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões
      this.typeData = data['typeData'];
      this.styles = data['stylesData'];

      switch (data.permission.action) {
        case 'VIEW_ONE': {
          this.setDataForm();
          break;
        }
        case 'EDIT': {
          this.setDataForm();
          this.enableForm();
          this.form.get('style_id').disable();
          break;
        }
        case 'NEW': {
          this.enableForm();
          break;
        }
        default: {
          this.goBack();
          break;
        }
      }
    });
  }

  private setDataForm(): void {
    this.form.patchValue({
      style_id: _.filter(this.styles, { id: this.typeData.style['id'] })[0],
      active: this.typeData.active,
      required: this.typeData.required,
      public: this.typeData.public,
      name: this.typeData.name,
      description: this.typeData.description,
    });
    this.editable = !!this.typeData['editable'];
  }

  async save() {
    if (this.form.invalid) {
      this.addToast('error', 'Erro', 'Formulário inválido. Verifique os dados e tente novamente!');
      return;
    }

    this.addToast('wait', 'Aguarde...', 'Estamos processando sua solicitação!', 15000, 100);

    if (this.typeData) {
      await this.updateType(this.form.value);
    } else {
      const form = _.cloneDeep(this.form.value);
      form['style_id'] = form['style_id']['id'];
      await this.addType(form);
    }

    this.toastyService.clear(100);
  }

   /**
   * Crud
   */
  async addType(data: object) {
    await this._typeService.addType(data).then((result) => {
      this.goBack(false, 'success', 'Sucesso', 'O Tipo de Atributo foi salvo com sucesso!');
    }, (err) => this.handleError(err));
  }

  async updateType(data: object) {
    await this._typeService.updateType(data, this.typeData.id).then((result) => {
      this.goBack(false, 'success', 'Sucesso', 'O Tipo de Atributo foi atualizado com sucesso!');
    }, (err) => this.handleError(err));
  }

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableForm(): void {
    this.buttonEdit.visible = false;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = true;
    this.buttonCancel.visible = true;
    this.form.enable();
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableForm(): void {
    this.buttonEdit.visible = true;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = false;
    this.buttonCancel.visible = false;
    this.form.disable();
  }

  /**
   * Volta para uma determinada página
   * @param {*} goList Volta para a listagem
   */
  goBack(goList: boolean = false, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    if (goList === true) {
      this.router.navigate(['/produtos/atributos/tipos']);
    } else {
      this.location.back();
    }
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.error.code) {
      case 'ER_DUP_ENTRY': err.error.message = 'O nome informado já esta cadastrado.'; break;
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.error.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
