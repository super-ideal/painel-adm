import { Routes } from '@angular/router';
import { TypeAttributesFormComponent } from './type-attributes-form.component';
import { AclGuard } from '../../../../../guards/acl.guard';
import { AttributesTypeFormResolver } from './resolvers/type-attributes-form.resolver';
import { StyleTypeAttributesResolver } from './resolvers/style-type-attributes.resolver';

export const TypeAttributesFormRoutes: Routes = [
  {
    path: '',
    component: TypeAttributesFormComponent,
    canActivateChild: [AclGuard],
    resolve: { stylesData: StyleTypeAttributesResolver },
    data: { permission: { page: 'PRODUCT_ATTRIBUTES_TYPES', action: 'NEW' }, breadcrumb: 'Cadastro', icon: 'fas fa-list-alt bg-c-blue', status: true }
  },
  {
    path: '',
    data: { icon: 'fas fa-list-alt bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: TypeAttributesFormComponent,
        canActivateChild: [AclGuard],
        resolve: { stylesData: StyleTypeAttributesResolver, typeData: AttributesTypeFormResolver },
        data: { permission: { page: 'PRODUCT_ATTRIBUTES_TYPES', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' }
      },
      {
        path: ':id/editar',
        component: TypeAttributesFormComponent,
        canActivateChild: [AclGuard],
        resolve: { stylesData: StyleTypeAttributesResolver, typeData: AttributesTypeFormResolver },
        data: { permission: { page: 'PRODUCT_ATTRIBUTES_TYPES', action: 'EDIT' }, breadcrumb: 'Edição' }
      },
    ]
  }
];
