import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/primeng';

import { SharedModule } from '../../../../../shared/shared.module';
import { TypeAttributesFormComponent } from './type-attributes-form.component';
import { TypeAttributesFormRoutes } from './type-attributes-form.routing';
import { AttributesTypeFormResolver } from './resolvers/type-attributes-form.resolver';
import { StyleTypeAttributesResolver } from './resolvers/style-type-attributes.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(TypeAttributesFormRoutes),
    SharedModule,
    InputSwitchModule,
    InputTextareaModule,
    DropdownModule,
    SweetAlert2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [TypeAttributesFormComponent],
  providers: [AttributesTypeFormResolver, StyleTypeAttributesResolver]
})
export class TypeAttributesFormModule { }
