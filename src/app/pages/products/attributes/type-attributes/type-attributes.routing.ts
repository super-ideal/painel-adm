import { Routes } from '@angular/router';
import { AclGuard } from '../../../../guards/acl.guard';

import { TypeAttributesResolver } from './resolvers/type-attributes.resolver';

import { TypeAttributesComponent } from './type-attributes.component';

const breadcrumb = 'Tipos de Atributos';
const message = '';

export const TypesAttributesRoutes: Routes = [
  {
    path: '',
    component: TypeAttributesComponent,
    canActivate: [AclGuard],
    resolve: { typesAttributesData: TypeAttributesResolver },
    data: { permission: { page: 'PRODUCT_ATTRIBUTES_TYPES', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: 'cadastro',
        loadChildren: './type-attributes-form/type-attributes-form.module#TypeAttributesFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ATTRIBUTES_TYPES', action: 'VIEW_ONE' } }
      },
    ]
  }
];
