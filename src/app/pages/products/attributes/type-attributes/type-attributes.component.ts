import { Component, OnInit } from '@angular/core';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';

import { AttributeTypeService } from '../../../../services/panel/products/atributes/types/type.service';

import { Pagination } from '../../../../interfaces/pagination.interface';
import { TypeAttribute } from '../../../../interfaces/product/attributes/type/type-attribute.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { VerifyPermissions } from '../../../../shared/verify-permissions/verify-permissions';

interface HeaderTable {
  field: string;
  header: string;
}

@Component({
  selector: 'app-types',
  templateUrl: './type-attributes.component.html',
  styleUrls: ['./type-attributes.component.scss']
})
export class TypeAttributesComponent implements OnInit {
  permissionsUserLogged: any;

  table: {
    cols: HeaderTable[],
    dataTypes: TypeAttribute[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number
  };
  selectedRowTable: any;

  whereTextSearch = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _typeService: AttributeTypeService
  ) {
    this.table = {
      cols: [
        { field: 'id', header: 'Id' },
        { field: 'active', header: 'Ativo' },
        { field: 'public', header: 'Público' },
        { field: 'required', header: 'Obrigatório' },
        { field: 'name', header: 'Nome' },
        { field: 'description', header: 'Descrição' },
        { field: 'style_id', header: 'Estilo' },
        { field: 'updated_at', header: 'Atualizado em' }
      ],
      dataTypes: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10
    };
  }

  ngOnInit() {
    this.route.data.subscribe(async data => {
      if (data['typesAttributesData']) {
        this.table.elementsPagination = await data['typesAttributesData'];
        this.table.dataTypes = await data['typesAttributesData']['data'];
      }
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões
    });
  }

   /**
   * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
   */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllTypes();
  }

  /**
   * Realiza uma busca manual
   */
  searchManual() {
    this.getAllTypes(this.whereTextSearch.trim());
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  getAllTypes(wText: string = '') {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando alguns dados para você!', 15000, 100);

    this._typeService.getAllTypes({
      pageNumber: this.table.pageNumber,
      perPage: this.table.rowsOnPage,
      whereBy: 'name',
      whereText: wText,
      pagination: true
    }).then(dataTypes => {
      this.table.elementsPagination = dataTypes['data'];
      this.table.dataTypes = dataTypes['data']['data'];

      this.toastyService.clear(100);
    }, (err) => {
      this.handleError(err);
    });
  }

  goToViewDetails(id) {
    this.router.navigate(['cadastro/' + id], { relativeTo: this.route });
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.error.code && err.name) {
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    this.addToast('error', 'Erro', err.error.message);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
