import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';

import { SharedModule } from '../../../shared/shared.module';
import { AttributesComponent } from './attributes.component';
import { AttributesRoutes } from './attributes.routing';
import { AttributesResolver } from './resolvers/attributes.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AttributesRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TableModule,
    PaginatorModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [AttributesComponent],
  providers: [AttributesResolver]
})
export class AttributesModule { }
