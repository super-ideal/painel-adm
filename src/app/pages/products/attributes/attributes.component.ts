import { Component, OnInit } from '@angular/core';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';

import { AttributeService } from '../../../services/panel/products/atributes/attribute.service';

import { SelectItem } from 'primeng/api';
import { Pagination } from '../../../interfaces/pagination.interface';
import { Attribute } from '../../../interfaces/product/attributes/attributes.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { VerifyPermissions } from '../../../shared/verify-permissions/verify-permissions';

interface HeaderTable {
  field: string;
  header: string;
}

@Component({
  selector: 'app-attributes',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.scss']
})
export class AttributesComponent implements OnInit {
  permissionsUserLogged: any;

  whereBy;
  whereTextSearch = '';
  selectSearch: SelectItem[];

  table: {
    cols: HeaderTable[],
    dataAttributes: Attribute[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number
  };
  selectedRowTable: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _attributeService: AttributeService
  ) {
    this.table = {
      cols: [
        { field: 'id', header: 'Id' },
        { field: 'active', header: 'Ativo' },
        { field: 'name', header: 'Nome' },
        { field: 'description', header: 'Descrição' },
        { field: 'type_name', header: 'Tipo' },
        { field: 'updated_at', header: 'Atualizado em' }
      ],
      dataAttributes: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10
    };

    this.selectSearch = [
      { label: 'Nome', value: 'name' },
      { label: 'Descrição', value: 'description' } ,
      { label: 'Tipo', value: 'type_name' },
      { label: 'Ativo', value: 'active' },
    ];
    this.whereBy = 'name';
  }

  ngOnInit() {
    this.route.data.subscribe(async data => {
      if (data['attributesData']) {
        this.table.elementsPagination = await data['attributesData'];
        this.table.dataAttributes = await data['attributesData']['data'];
      }
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões
    });
  }

   /**
   * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
   */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllAttributes();
  }

  /**
   * Realiza uma busca manual
   */
  searchManual() {
    let textSearch = this.whereTextSearch.trim();
    if (this.whereBy === 'active') {
      // tslint:disable-next-line:max-line-length
      if (textSearch === 'sim' || textSearch === 's') { textSearch = '1'; } else if (textSearch === 'nao' || textSearch === 'não' || textSearch === 'n') { textSearch = '0'; }
    }

    this.getAllAttributes(textSearch, this.whereBy);
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  getAllAttributes(wText: string = '', wBy: string = 'name') {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando alguns dados para você!', 15000, 100);

    this._attributeService.getAll({
      pageNumber: this.table.pageNumber,
      perPage: this.table.rowsOnPage,
      whereBy: wBy,
      whereText: wText
    }).then(data => {
      this.table.elementsPagination = data['data'];
      this.table.dataAttributes = data['data']['data'];

      this.toastyService.clear(100);
    }, (err) => {
      if (err.status === 404) {
        return this.handleError(err, 'warning', 'Atenção');
      }
      this.handleError(err);
    });
  }

  goToViewDetails(id) {
    this.router.navigate(['cadastro/' + id], { relativeTo: this.route });
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any, type: string = 'error', title: string = 'Erro') {
    this.toastyService.clearAll();

    switch (err.error.code && err.name && err.status) {
      case 404: err.error.message = 'Não encontramos nenhum atributo cadastrado'; break;
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    this.addToast(type, title, err.error.message);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
