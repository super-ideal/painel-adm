import { Routes } from '@angular/router';
import { AttributesComponent } from './attributes.component';
import { AttributesResolver } from './resolvers/attributes.resolver';
import { AclGuard } from '../../../guards/acl.guard';

const breadcrumb = 'Atributos';
const message = '';

export const AttributesRoutes: Routes = [
  {
    path: '',
    component: AttributesComponent,
    canActivate: [AclGuard],
    resolve: { attributesData: AttributesResolver },
    data: { permission: { page: 'PRODUCT_ATTRIBUTES', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: 'tipos',
        loadChildren: './type-attributes/type-attributes.module#TypeAttributesModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ATTRIBUTES_TYPES', action: 'VIEW_ALL' } }
      },
      {
        path: 'cadastro',
        loadChildren: './attribute-form/attribute-form.module#AttributeFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PRODUCT_ATTRIBUTES', action: 'VIEW_ONE' } }
      },
    ]
  }
];
