
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { OverlayPanelModule } from 'primeng/overlaypanel';

import { SharedModule } from '../../shared/shared.module';

import { ProductsComponent } from './products.component';
import { ProductsRoutes } from './products.routing';
import { ProductsResolver } from './resolvers/products.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductsRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TableModule,
    PaginatorModule,
    OverlayPanelModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ProductsComponent],
  providers: [ProductsResolver]
})
export class ProductsModule { }
