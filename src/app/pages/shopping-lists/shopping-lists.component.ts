import { Component, OnInit } from '@angular/core';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';

import { Pagination } from '../../interfaces/pagination.interface';
import { ShoppingListService } from 'src/app/services/panel//shopping-list/shopping-list.service';

import { VerifyPermissions } from '../../shared/verify-permissions/verify-permissions';

interface HeaderTable {
  field: string;
  header: string;
}

@Component({
  selector: 'app-shopping-lists',
  templateUrl: './shopping-lists.component.html',
  styleUrls: ['./shopping-lists.component.scss']
})
export class ShoppingListsComponent implements OnInit {
  search: {
    whereBy: string,
    whereOperator: string,
    whereTextSearch: string,
    whereByTypeSelected: string,
    searchOptions: Array<{ label: string, value: string, type: string }>,
    searchOptionsBoolean: Array<{ label: string, value: string }>,
    searchOptionsOperator: Array<{ label: string, value: string }>,
  };

  table: {
    cols: HeaderTable[],
    dataList: any[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number
  };
  selectedRowTable: any;
  calendarLanguage: any;

  permissionsUserLogged: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastyService: ToastyService,
    private _shoppingListService: ShoppingListService,
    private verifyPermissions: VerifyPermissions
  ) {
    this.table = {
      cols: [
        { field: 'id', header: 'Id' },
        { field: 'name', header: 'Nome' },
        { field: 'date_for_purchase', header: 'Data para Compra' },
        { field: 'public', header: 'Pública' },
        { field: 'shared', header: 'Compartilhada' },
        { field: 'created_by', header: 'Cadastrada por' },
        { field: 'created_at', header: 'Cadastrada em' },
      ],
      dataList: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10
    };

    this.search = {
      whereBy: 'name',
      whereTextSearch: '',
      whereOperator: 'anywhere',
      whereByTypeSelected: 'text',
      searchOptions: [
        { label: 'Cód. Lista', value: 'id', type: 'number' },
        { label: 'Nome Lista', value: 'name', type: 'text' },
        // { label: 'Data para Compra', value: 'date_for_purchase', type: 'date' },
        { label: 'Pública', value: 'public', type: 'boolean' },
        { label: 'Compartilhada', value: 'shared', type: 'boolean' },
        { label: 'Cadastrada por', value: 'created_by', type: 'text' },
        { label: 'Cód. Criador da Lista', value: 'created_by', type: 'number' },
        // { label: 'Cadastrada em', value: 'created_at', type: 'date' },
      ],
      searchOptionsBoolean: [
        { label: 'Sim', value: '1' },
        { label: 'Não', value: '0' },
        { label: 'Todos', value: '2' },
      ],
      searchOptionsOperator: []
    };

    this.calendarLanguage = {
      'pt_BR': {
        firstDayOfWeek: 1,
        dayNames: ['domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado'],
        dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
        // tslint:disable-next-line:max-line-length
        monthNames: ['janeiro ', 'fevereiro ', 'março ', 'abril ', 'maio ', 'junho ', 'julho ', 'agosto ', 'setembro ', 'outubro ', 'novembro ', 'dezembro '],
        monthNamesShort: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
        today: 'Hoje',
        clear: 'Limpar'
      }
    };
  }

  ngOnInit() {
    this.selectTheOperators();
    this.route.data.subscribe(async data => {
      if (data['listData']) {
        this.table.elementsPagination = await data['listData'];
        this.table.dataList = await data['listData']['data'];
      }
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões
    });
  }

  /**
   * Informa os operadores permitos para fazer a busca
   */
  selectTheOperators() {
    this.search.whereByTypeSelected = _.filter(this.search.searchOptions, { value: this.search.whereBy })[0]['type'];

    switch (this.search.whereByTypeSelected) {
      case 'boolean': {
        if (this.search.whereTextSearch === '2') {
          this.search.whereOperator = 'different';
        } else {
          this.search.whereOperator = 'equal';
        }

        this.search.searchOptionsOperator = [];
        break;
      }
      case 'number': {
        this.search.searchOptionsOperator = [
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Menor que', value: 'smaller' },
          { label: 'Maior que', value: 'bigger' },
          { label: 'Contido em', value: 'contained' }
        ];
        break;
      }
      case 'date': {
        this.search.searchOptionsOperator = [
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Menor que', value: 'smaller' },
          { label: 'Maior que', value: 'bigger' },
        ];
        break;
      }
      default: {
        this.search.searchOptionsOperator = [
          { label: 'Em qualquer lugar', value: 'anywhere' },
          { label: 'Começado com', value: 'started' },
          { label: 'Terminado com', value: 'finished' },
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Contido em', value: 'contained' }
        ];
        break;
      }
    }
  }

   /**
   * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
   */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllLists();
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  getAllLists() {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando alguns dados para você!', 15000, 100);

    // Seleciona o operador correto antes de pesquisar (Serve apenas para caso for booleano)
    this.selectTheOperators();

    this._shoppingListService.getAll({
      pageNumber: this.table.pageNumber,
      perPage: this.table.rowsOnPage,
      column: this.search.whereBy,
      operator: this.search.whereOperator,
      text: this.search.whereTextSearch.trim()
    })
    .then(data => {
      this.table.elementsPagination = data['data'];
      this.table.dataList = data['data']['data'];

      this.toastyService.clear(100);
    }, (err) => {
      this.handleError(err);
    });
  }

  goToViewDetails(id) {
    this.router.navigate(['detalhes/' + id], { relativeTo: this.route });
  }

  /**
  * Função de tratamento de erros
  * @param {*} err Objeto com dados do erro
  */
  handleError(err: any, type: string = 'error', title: string = 'Erro') {
    this.toastyService.clearAll();

    if (err.status === 404) {
      type = 'warning';
      title = 'Atenção';
      this.table.dataList = [];
    }

    switch (err.name && err.status) {
      case 404: err.message = 'Não encontramos nenhuma lista de compra com os dados informados'; break;
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast(type, title, err.message);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
