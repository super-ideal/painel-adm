import { Routes } from '@angular/router';
import { ShoppingListsComponent } from './shopping-lists.component';

import { AclGuard } from '../../guards/acl.guard';
import { ShoppingListsResolver } from './resolvers/shopping-lists.resolver';

const breadcrumb = 'Listas de Compras';
const message = '';

export const ShoppingListsRoutes: Routes = [
  {
    path: '',
    component: ShoppingListsComponent,
    canActivate: [AclGuard],
    resolve: { listData: ShoppingListsResolver },
    data: { permission: { page: 'SHOPPING_LISTS', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-clipboard-list bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-clipboard-list bg-c-blue', status: true },
    children: [
      {
        path: 'detalhes',
        loadChildren: './details-shopping-list/details-shopping-list.module#DetailsShoppingListModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'SHOPPING_LISTS', action: 'VIEW_ONE' } }
      },
    ]
  }
];
