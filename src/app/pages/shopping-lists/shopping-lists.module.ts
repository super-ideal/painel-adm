import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { CalendarModule } from 'primeng/calendar';

import { SharedModule } from '../../shared/shared.module';

import { ShoppingListsRoutes } from './shopping-lists.routing';
import { ShoppingListsComponent } from './shopping-lists.component';
import { ShoppingListsResolver } from './resolvers/shopping-lists.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(ShoppingListsRoutes),
    SharedModule,
    TableModule,
    PaginatorModule,
    CalendarModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ShoppingListsComponent],
  providers: [ShoppingListsResolver]
})
export class ShoppingListsModule { }
