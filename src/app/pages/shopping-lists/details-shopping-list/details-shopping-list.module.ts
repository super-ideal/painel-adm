
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { LightboxModule } from 'ngx-lightbox';

import { SharedModule } from '../../../shared/shared.module';

import { CardModule } from 'primeng/card';
import { InputSwitchModule } from 'primeng/inputswitch';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';

import { DetailsShoppingListComponent } from './details-shopping-list.component';
import { DetailsShoppingListRoutes } from './details-shopping-list.routing';
import { DetailsShoppingListResolver } from './resolvers/details-shopping-list.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DetailsShoppingListRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    SweetAlert2Module,
    CardModule,
    InputSwitchModule,
    LightboxModule,
    MessageModule,
    MessagesModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [DetailsShoppingListComponent],
  providers: [DetailsShoppingListResolver]
})
export class DetailsShoppingListModule { }
