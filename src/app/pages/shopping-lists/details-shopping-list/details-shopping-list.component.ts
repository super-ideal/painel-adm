import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import * as _ from 'lodash';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { Lightbox } from 'ngx-lightbox';
import { UploadService } from 'src/app/services/panel//upload/upload.service';

@Component({
  selector: 'app-details-shopping-list',
  templateUrl: './details-shopping-list.component.html',
  styleUrls: ['./details-shopping-list.component.scss']
})
export class DetailsShoppingListComponent implements OnInit {
  permissionsUserLogged: any;
  data: any;
  dataImages: any[] = [];
  messageNotFound = [{ severity: 'info', summary: 'Informação', detail: 'Nenhum produto foi encontrado' }];

  urlApi: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private toastyService: ToastyService,
    private lightbox: Lightbox,
    private _uploadService: UploadService
  ) {
    this.urlApi = this._uploadService.urlApi + 'image/';
  }

  /** Executa apenas na primeira vez que a página foi carregada */
  ngOnInit() {
    this.route.data.subscribe(async data => {
      this.data = data['detailsData'];

      if (this.data) {
        const listProducts = [];
        for (const product of this.data['listProducts']) {

          const dataImages = [];
          for (const image of product['product']['images']) {
            dataImages.push({
              thumb: this.urlApi + image['fieldname'] + '/' + image['filename'] + '/?format=jpeg&quality=100&width=150',
              src: this.urlApi + image['fieldname'] + '/' + image['filename'] + '/?format=jpeg&quality=100',
              original: this.urlApi + image['fieldname'] + '/' + image['filename'],
              caption: image['filename'],
              id: image['id'],
              fieldname: image['fieldname'],
              filename: image['filename'],
              master: image['master'],
            });
          }
          product['product']['images'] = dataImages;
          listProducts.push(product);
        }
        this.data['listProducts'] = listProducts;
      }
    });
  }

  /**
   * Mostra a imagem em tela grande, quando clica sobre ela na galeria
   * @param index Index dos dados da imagem no array de imagens
   */
  openLightBoxGallery(images: any): void {
    this.lightbox.open(images, 0, { centerVertically: true });
  }

  /**
   * Volta para uma determinada página
   */
  goBack(goList: boolean = false, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    if (goList === true) {
      this.router.navigate(['/listas-compras']);
    } else {
      this.location.back();
    }
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.error.code) {
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.error.message);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
