import { Routes } from '@angular/router';
import { AclGuard } from '../../../guards/acl.guard';

import { DetailsShoppingListComponent } from './details-shopping-list.component';
import { DetailsShoppingListResolver } from './resolvers/details-shopping-list.resolver';

const message = '';

export const DetailsShoppingListRoutes: Routes = [
  {
    path: ':id',
    component: DetailsShoppingListComponent,
    canActivate: [AclGuard],
    resolve: { detailsData: DetailsShoppingListResolver },
    data: { permission: { page: 'SHOPPING_LISTS', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes', message: message, icon: 'fas fa-clipboard-list bg-c-blue', status: true }
  }
];
