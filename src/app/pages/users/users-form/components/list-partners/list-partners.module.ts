
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';

import { SharedModule } from '../../../../../shared/shared.module';

import { ListPartnersComponent } from './list-partners.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    TableModule,
    PaginatorModule,
    DialogModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ListPartnersComponent],
  exports: [ListPartnersComponent]
})
export class ListPartnersModule { }
