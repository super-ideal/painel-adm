import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import * as _ from 'lodash';

import { MatchOtherValidator } from '../../../shared/match-other-validator/match-other-validator';
import { UsersService } from '../../../services/panel/users/users.service';
import { PartnersService } from '../../../services/panel/partners/partners.service';
import { VerifyPermissions } from '../../../shared/verify-permissions/verify-permissions';
import { UserPermissionsService } from '../../../services/panel/acl/user-permissions.service';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss']
})
export class UsersFormComponent implements OnInit {
  form: FormGroup;
  userData: any;
  permissions: any;

  statusArray: any = [
    {field: 'Administrador', value: 'admin'},
    {field: 'Usuário', value: 'user'}
  ];
  filteredStatus: any = [];

  displayListPartners = false;
  namePartner = '';
  permissionsUserLogged: any;

  buttonDelete: Button = {visible: false, disabled: false};
  buttonEdit: Button = {visible: false, disabled: false};
  buttonSave: Button = {visible: false, disabled: false};
  buttonCancel: Button = {visible: false, disabled: false};

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private renderer: Renderer2,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _userPermissionsService: UserPermissionsService,
    private _usersService: UsersService,
    private _partnersService: PartnersService,
  ) {
    this.form = this.formBuilder.group({
      active: [true],
      status: ['', [Validators.required]],
      username: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      email: [null, [Validators.required, Validators.pattern(/^[a-z0-9._+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0–9]{2,4}$/)]],
      partner_id: ['', [Validators.required]],
      password: [null, [Validators.minLength(6), Validators.maxLength(20)]],
      password_confirmation: [null, [MatchOtherValidator('password')]]
    });
  }

  ngOnInit() {
    this.form.disable();

    this.route.data.subscribe(async data => {
      this.userData = data['userData'];
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões

      switch (data.permission.action) {
        case 'VIEW_ONE': {
          this.setDataForm();
          this.buttonEdit.visible = true;
          this.buttonDelete.visible = true;
          break;
        }
        case 'EDIT': {
          this.setDataForm();
          this.enableForm();
          // this.focusElement('username');
          break;
        }
        case 'NEW': {
          this.form.get('password').setValidators([Validators.required, Validators.minLength(6), Validators.maxLength(20)]);
          this.enableForm();
          // this.focusElement('username');
          break;
        }
        default: {
          this.goBack();
          break;
        }
      }
    });
  }

  private setDataForm() {
    this.form.patchValue({
      active: !!this.userData.active,
      status: _.filter(this.statusArray, { value: this.userData.status })[0],
      username: this.userData.username,
      email: this.userData.email,
      partner_id: this.userData.partner_id,
    });

    if (this.userData && this.userData.partner_id) {
      this.namePartner = this.userData['partner']['name_reason'];
    }
  }

  /**
   * Tratamento dos dados para poder salvá-los
   */
  async save() {
    if (this.form.invalid) {
      this.addToast('error', 'Erro', 'Formulário inválido. Verifique os dados e tente novamente!');
      return;
    }

    this.addToast('wait', 'Aguarde...', 'Estamos processando sua solicitação!', 15000, 100);

      const form = this.form.value;
      form['status'] = form['status']['value'];

      if (this.userData) {
        await this.updateUser(form);
      } else {
        await this.addUser(form);
      }

      // Atualiza/Grava as permissões
      if (this.permissions && (this.permissions.newPermissions || this.permissions.deletePermissions)) {
        await this._userPermissionsService.update(this.permissions, this.userData.id)
          .then(() => {
            this.addToast('success', 'Sucesso', 'As permissões foram atualizadas com sucesso!');
          }, (err) => {
            this.handleError(err);
          });
      }

    this.toastyService.clear(100);
  }

  async addUser(data: any) {
    await this._usersService.post(data)
    .then((res) => {
      this.userData = res['data'];
      this.goBack(false, 'success', 'Sucesso', 'O usuário foi salvo com sucesso!');
    }, (err) => this.handleError(err));
  }

  async updateUser(data: any) {
    await this._usersService.update(data, this.userData.id)
    .then(() => {
      this.goBack(false, 'success', 'Sucesso', 'O usuário foi atualizado com sucesso!');
    }, (err) => this.handleError(err));
  }

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableForm(): void {
    this.buttonEdit.visible = false;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = true;
    this.buttonCancel.visible = true;
    this.form.enable();
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableForm(): void {
    this.buttonEdit.visible = true;
    this.buttonDelete.visible = true;
    this.buttonSave.visible = false;
    this.buttonCancel.visible = false;
    this.form.disable();
  }

  /**
   * Filtra os status para apresentar no autocomplete
   * @param {*} event Evento de objeto
   */
  filterStatus(event: any): void {
    this.filteredStatus = [];
    for (let i = 0; i < this.statusArray.length; i++) {
      const status = this.statusArray[i];
      if (status.field.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
        this.filteredStatus.push(status);
      }
    }
  }

  /**
   * Busca os parceiros para atribuir um ao usuário
   * @param openModal Verdadeiro se desejar abrir o modal para listar os parceiros
   * @param idPartner Id do parceiro para buscar os dados do mesmo
   */
  searchPartner(openModal: boolean, idPartner?: number) {
    if (openModal) {
      return this.displayListPartners = true;
    }

    if ((this.userData && idPartner === this.userData.id) || !idPartner) {
      return;
    }

    this._partnersService.get(idPartner)
    .then((res) => {
      this.setDataPartner(res['data']['id'], res['data']['name_reason']);
    }, err => {
      this.setDataPartner();

      if (err.status === 404) {
        this.addToast('warning', 'Não encontrado', 'O parceiro informado não foi encontrado');
      } else {
        this.addToast('err', 'Erro', 'Não conseguimos buscar o parceiro. Tente novamente!');
      }
    });
  }

  /**
   * Seta os dados do parceiro no formulário de usuário
   * @param id Id do parceiro
   * @param name Nome ou razão social do parceiro
   */
  setDataPartner(id: number = null, name: string = '') {
    this.form.get('partner_id').setValue(id);
    this.namePartner = name;
    this.displayListPartners = false;
  }

  /**
 * Recebe as atualização da permissão do usuário
 * @param data Permições novas e a serem excluidas
 */
  permissionsUser(data) {
    this.permissions = data;
  }

  /**
   * Volta para uma determinada página e envia um toast para o usuário
   * @param {boolean} [goList=false] Se verdadeiro volta para a listagem de parceiros, caso contrário volta para a última página
   * @param {string} type Tipo do toast de mensagem (success, error, warning, wait, default)
   * @param {string} title Título para o toast
   * @param {string} message Mensagem do toast
   */
  goBack(goList: boolean = false, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    if (goList === true) {
      this.router.navigate(['/usuarios']);
    } else {
      this.location.back();
    }
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.error.code) {
      case 'ER_DUP_ENTRY': err.error.message = 'O nome informado já esta cadastrado.'; break;
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.error.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
