import { Routes } from '@angular/router';
import { UsersFormComponent } from './users-form.component';
import { AclGuard } from '../../../guards/acl.guard';
import { UsersFormResolver } from './resolvers/users-form.resolver';

export const UsersFormRoutes: Routes = [
  {
    path: '',
    component: UsersFormComponent,
    canActivate: [AclGuard],
    data: { permission: { page: 'USERS', action: 'NEW' }, breadcrumb: 'Cadastro', icon: 'fas fa-users bg-c-blue', status: true }
  },
  {
    path: '',
    data: { icon: 'fas fa-users bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: UsersFormComponent,
        canActivateChild: [AclGuard],
        resolve: { userData: UsersFormResolver },
        data: { permission: { page: 'USERS', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' },
      },
      {
        path: ':id/editar',
        component: UsersFormComponent,
        canActivateChild: [AclGuard],
        resolve: { userData: UsersFormResolver },
        data: { permission: { page: 'USERS', action: 'EDIT' }, breadcrumb: 'Edição'}
      },
    ]
  }
];
