import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { DialogModule } from 'primeng/dialog';

import { SharedModule } from '../../../shared/shared.module';

import { UsersFormRoutes } from './users-form.routing';
import { UsersFormComponent } from './users-form.component';
import { TabViewModule } from 'primeng/tabview';

import { ListPartnersModule } from './components/list-partners/list-partners.module';
import { UsersFormResolver } from './resolvers/users-form.resolver';
import { PermissionsModule } from '../../permissions/components/permissions/permissions.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(UsersFormRoutes),
    SharedModule,
    SweetAlert2Module,
    InputSwitchModule,
    InputTextareaModule,
    AutoCompleteModule,
    DialogModule,
    TabViewModule,
    ListPartnersModule,
    PermissionsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [UsersFormComponent],
  providers: [UsersFormResolver]
})
export class UsersFormModule { }
