import { Component, OnInit } from '@angular/core';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';

import { Pagination } from '../../interfaces/pagination.interface';
import { UsersService } from '../../services/panel/users/users.service';

import { VerifyPermissions } from '../../shared/verify-permissions/verify-permissions';

interface HeaderTable {
  field: string;
  header: string;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  permissionsUserLogged: any;

  search: {
    whereBy: string,
    whereOperator: string,
    whereTextSearch: string,
    searchOptions: Array<{ label: string, value: string, type: string }>,
    searchOptionsBoolean: Array<{ label: string, value: string }>,
    searchOptionsOperator: Array<{ label: string, value: string }>,
  };

  table: {
    cols: HeaderTable[],
    data: any[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number,
    selectedRowTable: any
  };
  calendarLanguage: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _usersService: UsersService
  ) {
    this.table = {
      cols: [
        { field: 'id', header: 'Id' },
        { field: 'active', header: 'Ativo' },
        { field: 'name_reason', header: 'Nome/Razão Social' },
        { field: 'username', header: 'Username' },
        { field: 'email', header: 'E-mail' },
        { field: 'status', header: 'Status' },
        { field: 'created_at', header: 'Cadastrado em' },
        { field: 'updated_at', header: 'Atualizado em' }
      ],
      data: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10,
      selectedRowTable: []
    };

    this.search = {
      whereBy: 'name_reason',
      whereTextSearch: '',
      whereOperator: 'anywhere',
      searchOptions: [
        { label: 'Id', value: 'id', type: 'number' },
        { label: 'Nome/Razão Social', value: 'name_reason', type: 'text' },
        { label: 'Usuário', value: 'username', type: 'text' },
        { label: 'E-mail', value: 'email', type: 'text' },
        { label: 'Ativo', value: 'active', type: 'boolean' },
        // { label: 'Data Cadastro', value: 'created_at', type: 'date' },
      ],
      searchOptionsBoolean: [
        { label: 'Sim', value: '1' },
        { label: 'Não', value: '0' },
        { label: 'Todos', value: '2' },
      ],
      searchOptionsOperator: []
    };

    this.calendarLanguage = {
      'pt_BR': {
        firstDayOfWeek: 1,
        dayNames: ['domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado'],
        dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
        // tslint:disable-next-line:max-line-length
        monthNames: ['janeiro ', 'fevereiro ', 'março ', 'abril ', 'maio ', 'junho ', 'julho ', 'agosto ', 'setembro ', 'outubro ', 'novembro ', 'dezembro '],
        monthNamesShort: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
        today: 'Hoje',
        clear: 'Limpar'
      }
    };
  }

  ngOnInit() {
    this.route.data.subscribe(async data => {
      if (data['usersData']) {
        this.table.elementsPagination = await data['usersData'];
        this.table.data = await data['usersData']['data'];
      }
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões
    });
    this.selectTheOperators();
  }

  /**
   * Informa os operadores permitos para fazer a busca
   */
  selectTheOperators() {
    const whereByTypeSelected = _.filter(this.search.searchOptions, { value: this.search.whereBy })[0]['type'];

    switch (whereByTypeSelected) {
      case 'boolean': {
        if (this.search.whereTextSearch === '2') {
          this.search.whereOperator = 'different';
        } else {
          this.search.whereOperator = 'equal';
        }

        this.search.searchOptionsOperator = [];
        break;
      }
      case 'number': {
        this.search.searchOptionsOperator = [
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Menor que', value: 'smaller' },
          { label: 'Maior que', value: 'bigger' },
          { label: 'Contido em', value: 'contained' }
        ];
        break;
      }
      case 'date': {
        this.search.searchOptionsOperator = [
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Menor que', value: 'smaller' },
          { label: 'Maior que', value: 'bigger' },
        ];
        break;
      }
      default: {
        this.search.searchOptionsOperator = [
          { label: 'Em qualquer lugar', value: 'anywhere' },
          { label: 'Começado com', value: 'started' },
          { label: 'Terminado com', value: 'finished' },
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Contido em', value: 'contained' }
        ];
        break;
      }
    }
  }

   /**
   * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
   */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllUsers();
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  getAllUsers() {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando alguns dados para você!', 15000, 100);

    // Seleciona o operador correto antes de pesquisar (Serve apenas para caso for booleano)
    this.selectTheOperators();

    this._usersService.getAll({
      pageNumber: this.table.pageNumber,
      perPage: this.table.rowsOnPage,
      whereBy: this.search.whereBy,
      operator: this.search.whereOperator,
      whereText: this.search.whereTextSearch.trim()
    })
    .then(data => {
      this.table.elementsPagination = data['data'];
      this.table.data = data['data']['data'];

      this.toastyService.clear(100);
    }, (err) => {
      this.handleError(err);
    });
  }

  /**
   * Redireciona o usuário para a página de formulário
   * @param id ID único do usuário
   */
  goToViewDetails(id) {
    this.router.navigate(['cadastro/' + id], { relativeTo: this.route });
  }

  /**
  * Função de tratamento de erros
  * @param {*} err Objeto com dados do erro
  */
  handleError(err: any, type: string = 'error', title: string = 'Erro') {
    this.toastyService.clearAll();

    if (err.status === 404) {
      type = 'warning';
      title = 'Atenção';
      this.table.data = [];
    }

    switch (err.name && err.status) {
      case 404: err.message = 'Não encontramos nenhum parceiro com os dados informados'; break;
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast(type, title, err.message);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
