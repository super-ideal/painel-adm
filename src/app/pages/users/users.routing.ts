import { Routes } from '@angular/router';
import { UsersComponent } from './users.component';

import { AclGuard } from '../../guards/acl.guard';
import { UsersResolver } from './resolvers/users.resolver';

const breadcrumb = 'Usuários';
const message = '';

export const UsersRoutes: Routes = [
  {
    path: '',
    component: UsersComponent,
    canActivate: [AclGuard],
    resolve: { usersData: UsersResolver },
    data: { permission: { page: 'USERS', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-users bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-users bg-c-blue', status: true },
    children: [
      {
        path: 'cadastro',
        loadChildren: './users-form/users-form.module#UsersFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'USERS', action: 'VIEW_ONE' } }
      },
    ]
  }
];
