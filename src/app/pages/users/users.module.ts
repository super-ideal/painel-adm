import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { CalendarModule } from 'primeng/calendar';

import { SharedModule } from '../../shared/shared.module';

import { UsersRoutes } from './users.routing';
import { UsersComponent } from './users.component';
import { UsersResolver } from './resolvers/users.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(UsersRoutes),
    SharedModule,
    TableModule,
    PaginatorModule,
    CalendarModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [UsersComponent],
  providers: [UsersResolver]
})
export class UsersModule { }
