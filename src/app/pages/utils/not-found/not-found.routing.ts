import { Routes } from '@angular/router';
import { NotFoundComponent } from './not-found.component';

export const NotFoundRoutes: Routes = [
  {
    path: '',
    component: NotFoundComponent,
    data: { breadcrumb: 'Erro 404', icon: 'fas fa-users bg-c-blue', status: true }
  }
];
