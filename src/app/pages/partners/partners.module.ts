
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';

import { SharedModule } from '../../shared/shared.module';

import { PartnersRoutes } from './partners.routing';
import { PartnersComponent } from './partners.component';
import { PartnersResolver } from './partners.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PartnersRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TableModule,
    TabViewModule,
    DropdownModule,
    MultiSelectModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [PartnersComponent],
  providers: [PartnersResolver]
})
export class PartnersModule { }
