import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';

import { SharedModule } from '../../../../../shared/shared.module';

import { PhonesComponent } from './phones.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    InputSwitchModule,
    InputTextareaModule,
    TableModule,
    AutoCompleteModule,
    PanelModule,
    MessageModule,
    MessagesModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [PhonesComponent],
  exports: [PhonesComponent]
})
export class PhonesModule { }
