import { Component, Renderer2, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { VerifyPermissions } from '../../../../../shared/verify-permissions/verify-permissions';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

interface HeaderTable {
  field: string;
  header: string;
  width?: string;
}

@Component({
  selector: 'app-phone',
  templateUrl: './phones.component.html',
  styleUrls: ['./phones.component.scss']
})
export class PhonesComponent implements OnInit {
  permissionsUserLogged: any;

  form_phones: FormGroup;
  form_partner_enabled: Boolean = false;

  table: {
    phones: {
      cols: HeaderTable[],
      data: any[],
      selectedRowTable: any,
      types: string[],
      filteredType: any,
      messageNotFound: [{severity: string, summary: string, detail: string}],
      buttons: {
        buttonNew: Button,
        buttonDelete: Button,
        buttonEdit: Button,
        buttonSave: Button,
        buttonCancel: Button
      },
      disableRow: Boolean,
      new: Boolean
    }
  };

  maskDdi = ['+', /[0-9]/, /\d/, /\d/, /\d/];
  maskDdd = [/[0-9]/, /\d/, /\d/, /\d/];
  maskNumberTell = [/[0-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];

  constructor(
    private formBuilder: FormBuilder,
    private renderer: Renderer2,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
  ) {
    this.form_phones = this.formBuilder.group({
      type: [null, [Validators.required, Validators.maxLength(50)]],
      ddi: [null, Validators.maxLength(5)],
      ddd: ['', [Validators.required, Validators.maxLength(4)]],
      number: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(11)]],
      telephone_extension: ['', Validators.maxLength(4)],
      description: [null, Validators.maxLength(255)],
    });

    this.table = {
      phones: {
        cols: [
          { field: 'id', header: 'Id', width: '4em' },
          { field: 'type', header: 'Tipo', width: '10em' },
          { field: 'ddi', header: 'DDI', width: '5em' },
          { field: 'ddd', header: 'DDD', width: '5em' },
          { field: 'number', header: 'Número', width: '10em' },
          { field: 'telephone_extension', header: 'Ramal', width: '6em' },
          { field: 'description', header: 'Observação', width: '20em' },
          { field: 'created_at', header: 'Cadastrado em', width: '9em' },
        ],
        data: [],
        selectedRowTable: {},
        types: ['Casa', 'Escritório', 'Pessoal', 'Trabalho'],
        filteredType: [],
        messageNotFound: [{ severity: 'info', summary: 'Informação', detail: 'Nenhum telefone foi cadastrado' }],
        buttons: {
          buttonDelete: { visible: true, disabled: true },
          buttonNew: { visible: true, disabled: true },
          buttonEdit: { visible: true, disabled: true },
          buttonSave: { visible: false, disabled: false },
          buttonCancel: { visible: false, disabled: false }
        },
        disableRow: false,
        new: false
      },
    };
  }

  @Input()
  set data(phones) {
    this.table.phones.data = phones;
  }

  @Input()
  set enableForm(enabled: boolean) {
    this.form_partner_enabled = enabled;
  }

  ngOnInit() {
    this.disableFormPhones();
    this.permissionsUserLogged = this.verifyPermissions.getActionsUser('PARTNER_PHONES'); // permissão dos botões
  }

  /**
   * Mostra os dados do telefone selecionado na tabela
   * @param {*} data Dados do telefone
   */
  set selectedRowTablePhones(data: any) {
    if (this.form_partner_enabled) {
      this.table.phones.buttons.buttonEdit.disabled = false;
      this.table.phones.buttons.buttonDelete.disabled = false;
      this.table.phones.selectedRowTable = data;
    }

    if (data) {
      this.form_phones.patchValue({
        type: data.type,
        ddi: data.ddi,
        ddd: data.ddd,
        number: data.number,
        telephone_extension: data.telephone_extension,
        description: data.description
      });
    }
  }

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableFormPhones(newPhone: Boolean = false): void {
    this.table.phones.new = newPhone;

    this.table.phones.buttons.buttonNew.visible = false;
    this.table.phones.buttons.buttonEdit.visible = false;
    this.table.phones.buttons.buttonDelete.visible = false;
    this.table.phones.buttons.buttonSave.visible = true;
    this.table.phones.buttons.buttonCancel.visible = true;
    this.table.phones.disableRow = true;
    this.form_phones.enable();
    this.focusElement('type');
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableFormPhones(): void {
    this.table.phones.buttons.buttonNew.visible = true;
    this.table.phones.buttons.buttonNew.disabled = false;

    this.table.phones.buttons.buttonEdit.visible = true;
    this.table.phones.buttons.buttonEdit.disabled = true;

    this.table.phones.buttons.buttonDelete.visible = true;
    this.table.phones.buttons.buttonDelete.disabled = true;

    this.table.phones.buttons.buttonSave.visible = false;
    this.table.phones.buttons.buttonCancel.visible = false;
    this.table.phones.disableRow = false;
    this.form_phones.disable();
  }

  /**
   * Adiciona ou atualiza um telefone da lista
   */
  addOrUpdatePhone() {
    if (!this.table.phones.new) {
      const index = _.findIndex(this.table.phones.data, { Telefone: this.table.phones.selectedRowTable['number']});

      this.table.phones.selectedRowTable['type'] = this.form_phones.value['type'];
      this.table.phones.selectedRowTable['ddd'] = this.form_phones.value['ddd'];
      this.table.phones.selectedRowTable['ddi'] = this.form_phones.value['ddi'];
      this.table.phones.selectedRowTable['number'] = this.form_phones.value['number'];
      this.table.phones.selectedRowTable['telephone_extension'] = this.form_phones.value['telephone_extension'];
      this.table.phones.selectedRowTable['description'] = this.form_phones.value['description'];
      this.table.phones.selectedRowTable['updated'] = true; // serve para verificar ao salvar parceiro quais email foram atualizados
      this.table.phones.data[index] = this.table.phones.selectedRowTable;
    } else {
      const form = this.form_phones.value;
      const exists = _.filter(this.table.phones.data, function (o) {
        return o['number'].toString().toUpperCase().trim().replace(/\D+/g, '') === form['number'].toString().toUpperCase().trim().replace(/\D+/g, '')
          && o['ddd'].toString().toUpperCase().trim() === form['ddd'].toString().toUpperCase().trim()
          && o['telephone_extension'] === form['telephone_extension'];
      });

      if (exists.length !== 0) {
        this.addToast('warning', 'Duplicidade', 'O telefone informado já existe na lista');
        return;
      }

      this.table.phones.data.push(form);
    }

    this.disableFormPhones();
  }

  /**
   * Exclui um e-mail da lista
   */
  removePhone() {
    swal({
      title: 'Exclusão',
      text: 'Você deseja realmente excluir este telefone?',
      type: 'warning',
      timer: 5000,
      showCancelButton: true,
      cancelButtonText: 'Não',
      confirmButtonText: 'Sim, excluir!'
    }).then((result) => {

      if (result.value) {
        const index = _.findIndex(this.table.phones.data, { id: this.table.phones.selectedRowTable['id'] });
        if (index > -1 || !this.table.phones.selectedRowTable['id']) {
          this.table.phones.data.splice(index, 1);
          this.selectedRowTablePhones = {};
        }
      }

    }).catch(swal.noop);
  }

  /**
   * Filtra os tipos de phone para apresentar no autocomplete
   * @param {*} event Evento de objeto
   */
  filterTypePhones(event: any): void {
    this.table.phones.filteredType = [];
    for (let i = 0; i < this.table.phones.types.length; i++) {
      const phone = this.table.phones.types[i];
      if (phone.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
        this.table.phones.filteredType.push(phone);
      }
    }
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.status) {
      // case 422: err.error.message = 'O usuário informado já está cadastrado.'; break;
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }

}
