import { Component, Renderer2, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import * as _ from 'lodash';
import swal from 'sweetalert2';

import { PostmonService } from '../../../../../services/panel/others/postmon.service';
import { VerifyPermissions } from '../../../../../shared/verify-permissions/verify-permissions';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

interface HeaderTable {
  field: string;
  header: string;
  width?: string;
}

@Component({
  selector: 'app-address',
  templateUrl: './adresses.component.html',
  styleUrls: ['./adresses.component.scss']
})
export class AdressesComponent implements OnInit {
  permissionsUserLogged: any;

  form_adresses: FormGroup;
  form_partner_enabled: Boolean = false;

  maskCep = [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];

  table: {
    adresses: {
      cols: HeaderTable[],
      data: any[],
      selectedRowTable: any,
      messageNotFound: [{severity: string, summary: string, detail: string}],
      buttons: {
        buttonNew: Button,
        buttonDelete: Button,
        buttonEdit: Button,
        buttonSave: Button,
        buttonCancel: Button
      },
      disableRow: Boolean,
      new: Boolean
    }
  };

  constructor(
    private formBuilder: FormBuilder,
    private renderer: Renderer2,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _postmon: PostmonService
  ) {
    this.form_adresses = this.formBuilder.group({
      cep: [null, [Validators.required, Validators.maxLength(10)]],
      street: [null, [Validators.required]],
      number: [null],
      complement: [null],
      neighborhood: [null, [Validators.required]],
      city: [null, [Validators.required]],
      state: [null, [Validators.required]],
    });

    this.table = {
      adresses: {
        cols: [
          { field: 'id', header: 'Id', width: '4em' },
          { field: 'cep', header: 'CEP', width: '10em' },
          { field: 'street', header: 'Rua', width: '20em' },
          { field: 'number', header: 'Número', width: '6em' },
          { field: 'complement', header: 'Complemento', width: '15em' },
          { field: 'neighborhood', header: 'Bairro', width: '10em' },
          { field: 'city', header: 'Cidade', width: '15em' },
          { field: 'state', header: 'Estado', width: '10em' },
          { field: 'created_at', header: 'Cadastrado em', width: '9em' },
        ],
        data: [],
        selectedRowTable: {},
        messageNotFound: [{ severity: 'info', summary: 'Informação', detail: 'Nenhum endereço foi cadastrado' }],
        buttons: {
          buttonDelete: { visible: true, disabled: true },
          buttonNew: { visible: true, disabled: true },
          buttonEdit: { visible: true, disabled: true },
          buttonSave: { visible: false, disabled: false },
          buttonCancel: { visible: false, disabled: false }
        },
        disableRow: false,
        new: false
      }
    };
  }

  @Input()
  set data(adresses) {
    this.table.adresses.data = adresses;
  }

  @Input()
  set enableForm(enabled: boolean) {
    this.form_partner_enabled = enabled;
  }

  ngOnInit() {
    this.disableFormAdresses();
    this.permissionsUserLogged = this.verifyPermissions.getActionsUser('PARTNER_PHONES'); // permissão dos botões
  }

  /**
   * Mostra os dados do endereço selecionado na tabela
   * @param {*} data Dados do endereço
   */
  set selectedRowTableAdresses(data: any) {
    if (this.form_partner_enabled) {
      this.table.adresses.buttons.buttonEdit.disabled = false;
      this.table.adresses.buttons.buttonDelete.disabled = false;
      this.table.adresses.selectedRowTable = data;
    }

    if (data) {
      this.form_adresses.patchValue({
        cep: data.cep,
        street: data.street,
        number: data.number,
        complement: data.complement,
        neighborhood: data.neighborhood,
        city: data.city,
        state: data.state
      });
    }
  }

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableFormAdresses(newAddress: Boolean = false): void {
    this.table.adresses.new = newAddress;

    this.table.adresses.buttons.buttonNew.visible = false;
    this.table.adresses.buttons.buttonEdit.visible = false;
    this.table.adresses.buttons.buttonDelete.visible = false;
    this.table.adresses.buttons.buttonSave.visible = true;
    this.table.adresses.buttons.buttonCancel.visible = true;
    this.table.adresses.disableRow = true;
    this.form_adresses.enable();
    this.focusElement('cep');
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableFormAdresses(): void {
    this.table.adresses.buttons.buttonNew.visible = true;
    this.table.adresses.buttons.buttonNew.disabled = false;

    this.table.adresses.buttons.buttonEdit.visible = true;
    this.table.adresses.buttons.buttonEdit.disabled = true;

    this.table.adresses.buttons.buttonDelete.visible = true;
    this.table.adresses.buttons.buttonDelete.disabled = true;

    this.table.adresses.buttons.buttonSave.visible = false;
    this.table.adresses.buttons.buttonCancel.visible = false;
    this.table.adresses.disableRow = false;
    this.form_adresses.disable();
  }

  /**
   * Adiciona ou atualiza um endereço da lista
   */
  addOrUpdateAddress() {
    if (!this.table.adresses.new) {
      const index = _.findIndex(this.table.adresses.data, { street: this.table.adresses.selectedRowTable['street'], number: this.table.adresses.selectedRowTable['number'], neighborhood: this.table.adresses.selectedRowTable['neighborhood']});
      const form_adresses = _.cloneDeep(this.form_adresses.value);

      this.table.adresses.selectedRowTable['cep'] = form_adresses['cep'].toString().replace(/\D+/g, '').trim();
      this.table.adresses.selectedRowTable['street'] = form_adresses['street'];
      this.table.adresses.selectedRowTable['number'] = form_adresses['number'];
      this.table.adresses.selectedRowTable['complement'] = form_adresses['complement'];
      this.table.adresses.selectedRowTable['neighborhood'] = form_adresses['neighborhood'];
      this.table.adresses.selectedRowTable['city'] = form_adresses['city'];
      this.table.adresses.selectedRowTable['state'] = form_adresses['state'];
      this.table.adresses.selectedRowTable['updated'] = true; // serve para verificar ao salvar parceiro quais email foram atualizados

      this.table.adresses.data[index] = this.table.adresses.selectedRowTable;
      this.disableFormAdresses();

    } else {
      const form = _.cloneDeep(this.form_adresses.value);
      form['cep'] = form['cep'].toString().replace(/\D+/g, '').trim()

      const exists = _.filter(this.table.adresses.data, function (o) {
        return o.street.toString().toUpperCase().trim() === form['street'].toString().toUpperCase().trim()
          && o.number.toString().toUpperCase().trim() === form['number'].toString().toUpperCase().trim()
          && o.cep.toString().toUpperCase().trim() === form['cep'].toString().toUpperCase().trim();
      });

      if (exists.length !== 0) {
        this.addToast('warning', 'Duplicidade', 'O endereço informado já existe na lista');
        return;
      }

      this.table.adresses.data.push(form);
      this.disableFormAdresses();
    }
  }

  /**
   * Exclui um endereço da lista
   */
  removeAddress() {
    swal({
      title: 'Exclusão',
      text: 'Você deseja realmente excluir este endereço?',
      type: 'warning',
      timer: 5000,
      showCancelButton: true,
      cancelButtonText: 'Não',
      confirmButtonText: 'Sim, excluir!'
    }).then((result) => {

      if (result.value) {
        const index = _.findIndex(this.table.adresses.data, { id: this.table.adresses.selectedRowTable['id'] });
        if (index > -1 || !this.table.adresses.selectedRowTable['id']) {
          this.table.adresses.data.splice(index, 1);
          this.selectedRowTableAdresses = {};
        }
      }

    }).catch(swal.noop);
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.status) {
      // case 422: err.error.message = 'O usuário informado já está cadastrado.'; break;
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.message);
  }

  /**
   * Busca os dados do CEP nos Correios, ViaCEP e CepAberto
   * @param cepNumber Número do CEP
   */
  searchCep(number: string) {
    let cep = number.toString().trim();
    cep = cep.replace(/\D+/g, '');

    if (cep) {
      const form = this.form_adresses.value;

      this._postmon.getCep(cep)
      .then((res) => {
        this.form_adresses.patchValue({
          street: res['logradouro'] || form.street,
          complement: res['complemento'] || form.complement,
          neighborhood: res['bairro'] || form.neighborhood,
          city: res['cidade'] || form.city,
          state: res['estado_info']['nome'] || form.state,
        });
      })
      .catch((err) => {
        if (err.status === 0 || err.status === 404) {
          this.form_adresses.get('cep').setErrors({cepInvalid: true});
        }
      });
    }
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }

}
