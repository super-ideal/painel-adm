import { Component, Renderer2, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { VerifyPermissions } from '../../../../../shared/verify-permissions/verify-permissions';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

interface HeaderTable {
  field: string;
  header: string;
  width?: string;
}

@Component({
  selector: 'app-email',
  templateUrl: './emails.component.html',
  styleUrls: ['./emails.component.scss']
})
export class EmailsComponent implements OnInit {
  permissionsUserLogged: any;

  form_emails: FormGroup;
  form_partner_enabled: Boolean = false;

  table: {
    emails: {
      cols: HeaderTable[],
      data: any[],
      selectedRowTable: any,
      types: string[],
      filteredType: any,
      messageNotFound: [{severity: string, summary: string, detail: string}],
      buttons: {
        buttonNew: Button,
        buttonDelete: Button,
        buttonEdit: Button,
        buttonSave: Button,
        buttonCancel: Button
      },
      disableRow: Boolean,
      new: Boolean
    }
  };

  constructor(
    private formBuilder: FormBuilder,
    private renderer: Renderer2,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
  ) {
    this.form_emails = this.formBuilder.group({
      type: [null, [Validators.required]],
      description: [null],
      email: [null, [Validators.required, Validators.pattern(/^[a-z0-9._+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0–9]{2,4}$/)]],
    });

    this.table = {
      emails: {
        cols: [
          { field: 'id', header: 'Id', width: '4em' },
          { field: 'type', header: 'Tipo', width: '10em' },
          { field: 'email', header: 'E-mail', width: '15em' },
          { field: 'description', header: 'Observação', width: '20em' },
          { field: 'created_at', header: 'Cadastrado em', width: '9em' },
        ],
        data: [],
        selectedRowTable: {},
        types: ['Casa', 'Escritório', 'Pessoal', 'Trabalho'],
        filteredType: [],
        messageNotFound: [{severity: 'info', summary: 'Informação', detail: 'Nenhum e-mail foi cadastrado'}],
        buttons: {
          buttonDelete: { visible: true, disabled: true },
          buttonNew: { visible: true, disabled: true },
          buttonEdit: { visible: true, disabled: true },
          buttonSave: { visible: false, disabled: false },
          buttonCancel: { visible: false, disabled: false }
        },
        disableRow: false,
        new: false
      }
    };
  }

  @Input()
  set data(emails) {
    this.table.emails.data = emails;
  }

  @Input()
  set enableForm(enabled: boolean) {
    this.form_partner_enabled = enabled;
  }

  ngOnInit() {
    this.disableFormEmails();
    this.permissionsUserLogged = this.verifyPermissions.getActionsUser('PARTNER_PHONES'); // permissão dos botões
  }

  /**
   * Mostra os dados do e-mail selecionado na tabela
   * @param {*} data Dados do e-mail
   */
  set selectedRowTableEmails(data: any) {
    if (this.form_partner_enabled) {
      this.table.emails.buttons.buttonEdit.disabled = false;
      this.table.emails.buttons.buttonDelete.disabled = false;
      this.table.emails.selectedRowTable = data;
    }

    if (data) {
      this.form_emails.patchValue({
        type: data.type,
        description: data.description,
        email: data.email
      });
    }
  }

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableFormEmails(newEmail: boolean = false): void {
    this.table.emails.new = newEmail;

    this.table.emails.buttons.buttonNew.visible = false;
    this.table.emails.buttons.buttonEdit.visible = false;
    this.table.emails.buttons.buttonDelete.visible = false;
    this.table.emails.buttons.buttonSave.visible = true;
    this.table.emails.buttons.buttonCancel.visible = true;
    this.table.emails.disableRow = true;
    this.form_emails.enable();
    this.focusElement('type');
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableFormEmails(): void {
    this.table.emails.buttons.buttonNew.visible = true;
    this.table.emails.buttons.buttonNew.disabled = false;

    this.table.emails.buttons.buttonEdit.visible = true;
    this.table.emails.buttons.buttonEdit.disabled = true;

    this.table.emails.buttons.buttonDelete.visible = true;
    this.table.emails.buttons.buttonDelete.disabled = true;

    this.table.emails.buttons.buttonSave.visible = false;
    this.table.emails.buttons.buttonCancel.visible = false;
    this.table.emails.disableRow = false;
    this.form_emails.disable();
  }

  /**
   * Adiciona ou atualiza um e-mail da lista
   */
  addOrUpdateEmail() {
    if (!this.table.emails.new) {
      const index = _.findIndex(this.table.emails.data, {email: this.table.emails.selectedRowTable['email']});

      this.table.emails.selectedRowTable['type'] = this.form_emails.value['type'];
      this.table.emails.selectedRowTable['email'] = this.form_emails.value['email'];
      this.table.emails.selectedRowTable['description'] = this.form_emails.value['description'];
      this.table.emails.selectedRowTable['updated'] = true; // serve para verificar ao salvar parceiro quais email foram atualizados
      this.table.emails.data[index] = this.table.emails.selectedRowTable;

    } else {
      const form = _.cloneDeep(this.form_emails.value);
      const exists = _.filter(this.table.emails.data, function (o) {
        return o['email'].toString().toUpperCase().trim() === form['email'].toString().toUpperCase().trim();
      });

      if (exists.length !== 0) {
        this.addToast('warning', 'Duplicidade', 'O e-mail informado já existe na lista');
        return;
      }

      this.table.emails.data.push(form);
    }

    this.disableFormEmails();
  }

  /**
   * Exclui um e-mail da lista
   */
  removeEmail() {
    swal({
      title: 'Exclusão',
      text: 'Você deseja realmente excluir este e-mail?',
      type: 'warning',
      timer: 5000,
      showCancelButton: true,
      cancelButtonText: 'Não',
      confirmButtonText: 'Sim, excluir!'
    }).then((result) => {

      if (result.value) {
        const index = _.findIndex(this.table.emails.data, { id: this.table.emails.selectedRowTable['id'] });
        if (index > -1 || !this.table.emails.selectedRowTable['id']) {
          this.table.emails.data.splice(index, 1);
          this.selectedRowTableEmails = {};
        }
      }

    }).catch(swal.noop);
  }

  /**
   * Filtra os tipos de e-mail para apresentar no autocomplete
   * @param {*} event Evento de objeto
   */
  filterTypeEmails(event: any): void {
    this.table.emails.filteredType = [];
    for (let i = 0; i < this.table.emails.types.length; i++) {
      const email = this.table.emails.types[i];
      if (email.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
        this.table.emails.filteredType.push(email);
      }
    }
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.status) {
      // case 422: err.error.message = 'O usuário informado já está cadastrado.'; break;
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }

}
