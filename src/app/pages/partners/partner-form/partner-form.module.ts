import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';

import { SharedModule } from '../../../shared/shared.module';

import { PartnerFormRoutes } from './partner-form.routing';
import { PartnerFormComponent } from './partner-form.component';
import { EmailsModule } from './components/emails/emails.module';
import { PhonesModule } from './components/phones/phones.module';
import { AdressesModule } from './components/adresses/adresses.module';
import { PartnerFormResolver } from './resolvers/partner-form.resolver';
import { PartnerTypesResolver } from './resolvers/partner-types.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PartnerFormRoutes),
    FormsModule,
    ReactiveFormsModule,
    EmailsModule,
    PhonesModule,
    AdressesModule,
    SharedModule,
    TableModule,
    InputSwitchModule,
    InputTextareaModule,
    AutoCompleteModule,
    DropdownModule,
    MultiSelectModule,
    PanelModule,
    MessageModule,
    MessagesModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [PartnerFormComponent],
  providers: [PartnerFormResolver, PartnerTypesResolver]
})
export class PartnerFormModule { }
