import { Routes } from '@angular/router';
import { PartnerFormComponent } from './partner-form.component';
import { AclGuard } from '../../../guards/acl.guard';
import { PartnerFormResolver } from './resolvers/partner-form.resolver';
import { PartnerTypesResolver } from './resolvers/partner-types.resolver';

export const PartnerFormRoutes: Routes = [
  {
    path: '',
    component: PartnerFormComponent,
    resolve: { typesPartner: PartnerTypesResolver },
    data: { permission: { page: 'PARTNERS', action: 'NEW' }, breadcrumb: 'Cadastro', message: '', icon: 'fa-handshake bg-c-blue', status: true },
    canActivate: [AclGuard],
  },
  {
    path: '',
    data: { icon: 'fa-handshake bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: PartnerFormComponent,
        canActivateChild: [AclGuard],
        resolve: { partnerData: PartnerFormResolver, typesPartner: PartnerTypesResolver },
        data: { permission: { page: 'PARTNERS', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' }
      },
      {
        path: ':id/editar',
        component: PartnerFormComponent,
        canActivateChild: [AclGuard],
        resolve: { partnerData: PartnerFormResolver, typesPartner: PartnerTypesResolver },
        data: { permission: { page: 'PARTNERS', action: 'EDIT' }, breadcrumb: 'Edição' }
      },
    ]
  }
];
