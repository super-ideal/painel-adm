import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import swal from 'sweetalert2';
import * as _ from 'lodash';

import { AuthService } from '../../../services/panel/auth.service';
import { PartnersService } from '../../../services/panel/partners/partners.service';
import { PhonesService } from '../../../services/panel/partners/phones/phones.service';
import { AdressesService } from '../../../services/panel/partners/adresses/adresses.service';
import { EmailsService } from '../../../services/panel/partners/emails/emails.service';

import { ValidadorCpfCNPJ } from 'src/app/shared/validator-cpf-cnpj/validador';
import { VerifyPermissions } from '../../../shared/verify-permissions/verify-permissions';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

@Component({
  selector: 'app-partner-form',
  templateUrl: './partner-form.component.html',
  styleUrls: ['./partner-form.component.scss']
})
export class PartnerFormComponent implements OnInit {
  permissionsUserLogged: any;

  form: FormGroup;
  form_partner: FormGroup;

  userLogged: any;

  dataPartner: any;
  typesPartner: any[];
  typesPartnerFiltered: any[];
  typesPerson: any[];
  typePersonFisic: Boolean = true;
  rgIssuingBody: any[];
  rgUf: any[];
  sex: any[];

  dataNewEmails: any;
  dataOldEmail: any;

  dependences: {
    emails: { data_new: any[], data_old: any },
    phones: { data_new: any[], data_old: any },
    adresses: { data_new: any[], data_old: any },
  };

  buttonDelete: Button = { visible: false, disabled: false };
  buttonEdit: Button = { visible: false, disabled: false };
  buttonSave: Button = { visible: false, disabled: false };
  buttonCancel: Button = { visible: false, disabled: false };

  maskCpf = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  maskCnpj = [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private renderer: Renderer2,
    private toastyService: ToastyService,
    private validadorCpfCNPJ: ValidadorCpfCNPJ,
    private verifyPermissions: VerifyPermissions,
    private _authService: AuthService,
    private _partners: PartnersService,
    private _phones: PhonesService,
    private _addresses: AdressesService,
    private _emails: EmailsService,
  ) {
    this.form = this.formBuilder.group({
      partner: this.form_partner = this.formBuilder.group({
        name_reason: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
        name_fantasy: [null],
        cpf_cnpj: [''],
        rg_ie: [null],
        rg_uf: [null],
        rg_issuing_body: [null],
        type_person: [null],
        sex: [null],
        active: [true, [Validators.required]],
        types_partner: [null, [Validators.required]],
      })
    });

    this.dependences = {
      emails: {
        data_old: [],
        data_new: [],
      },
      phones: {
        data_old: [],
        data_new: [],
      },
      adresses: {
        data_old: [],
        data_new: [],
      }
    };

    this.typesPerson = [
      {label: 'Física', key: 'fisica'},
      {label: 'Jurídica', key: 'juridica'},
    ];

    this.sex = [
      {label: 'Masculino', key: 'M'},
      {label: 'Feminino', key: 'F'},
      {label: 'Outro', key: 'O'},
    ];

    this.rgUf = [
      {label: 'AC', key: 'AC'},
      {label: 'AL', key: 'AL'},
      {label: 'AP', key: 'AP'},
      {label: 'AM', key: 'AM'},
      {label: 'BA', key: 'BA'},
      {label: 'CE', key: 'CE'},
      {label: 'DF', key: 'DF'},
      {label: 'ES', key: 'ES'},
      {label: 'GO', key: 'GO'},
      {label: 'MA', key: 'MA'},
      {label: 'MT', key: 'MT'},
      {label: 'MS', key: 'MS'},
      {label: 'MG', key: 'MG'},
      {label: 'PA', key: 'PA'},
      {label: 'PB', key: 'PB'},
      {label: 'PR', key: 'PR'},
      {label: 'PI', key: 'PI'},
      {label: 'RJ', key: 'RJ'},
      {label: 'RN', key: 'RN'},
      {label: 'RS', key: 'RS'},
      {label: 'RO', key: 'RO'},
      {label: 'RR', key: 'RR'},
      {label: 'SC', key: 'SC'},
      {label: 'SP', key: 'SP'},
      {label: 'SE', key: 'SE'},
      {label: 'TO', key: 'TO'},
    ];

    this.rgIssuingBody = [
      {label: 'CRE - Conselho Regional de Estatística', key: 'CRE'},
      {label: 'COREN - Conselho Regional de Enfermagem', key: 'COREN'},
      {label: 'CRAS - Conselho Regional de Assistentes Sociais', key: 'CRAS'},
      {label: 'SSP - Secretaria de Segurança Pública', key: 'SSP'},
      {label: 'CRB - Conselho Regional de Biblioteconomia', key: 'CRB'},
      {label: 'CREA - Conselho Regional de Engenharia Arquitetura e Agronomia', key: 'CREA'},
      {label: 'CRECI - Conselho Regional de Corretores de Imóveis', key: 'CRECI'},
      {label: 'CREFIT - Conselho Regional de Fisioterapia e Terapia Ocupacional', key: 'CREFIT'},
      {label: 'CRF - Conselho Regional de Farmácia', key: 'CRF'},
      {label: 'CRN - Conselho Regional de Nutrição', key: 'CRN'},
      {label: 'CRO - Conselho Regional de Odontologia', key: 'CRO'},
      {label: 'CRM - Conselho Regional de Medicina', key: 'CRM'},
      {label: 'CRP - Conselho Regional de Psicologia', key: 'CRP'},
      {label: 'IFP - Instituto de Identificação Félix Pacheco', key: 'IFP'},
      {label: 'CRQ - Conselho Regional de Química', key: 'CRQ'},
      {label: 'CRPRE - Conselho Regional de Profissionais de Relações Públicas', key: 'CRPRE'},
      {label: 'CRRC - Conselho Regional de Representantes Comerciais', key: 'CRRC'},
      {label: 'CRMV - Conselho Regional de Medicina Veterinária', key: 'CRMV'},
      {label: 'DPF - Polícia Federal', key: 'DPF'},
      {label: 'EST - Documentos Estrangeiros', key: 'EST'},
      {label: 'ICLA - Carteira de Identidade Classista', key: 'ICLA'},
      {label: 'MAE - Ministério da Aeronáutica', key: 'MAE'},
      {label: 'MEX - Ministério do Exército', key: 'MEX'},
      {label: 'OAB - Ordem dos Advogados do Brasil', key: 'OAB'},
      {label: 'OMB - Ordens dos Músicos do Brasil', key: 'OMB'},
      {label: 'MMA - Ministério da Marinha', key: 'MMA'},
    ];

    this.userLogged = this._authService.getUserLocal();
  }

  ngOnInit() {
    this.disableForm();

    this.route.data.subscribe(async data => {
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões

      this.typesPartner = await _.cloneDeep(data['typesPartner']);
      this.typesPartnerFiltered = await _.cloneDeep(this.typesPartner);
      this.dataPartner = await _.cloneDeep(data['partnerData']);

      switch (data.permission.action) {
        case 'VIEW_ONE': {
          await this.organizesPartnerData();
          break;
        }
        case 'EDIT': {
          await this.organizesPartnerData();
          this.enableForm();
          this.form_partner.get('type_person').value ? this.form_partner.get('type_person').disable() : this.form_partner.get('type_person').enable();
          this.form_partner.get('cpf_cnpj').value ? this.form_partner.get('cpf_cnpj').disable() : this.form_partner.get('cpf_cnpj').enable();
          this.focusElement('name_reason');
          break;
        }
        case 'NEW': {
          this.enableForm();
          this.form_partner.get('type_person').setValue(this.typesPerson[0]);
          break;
        }
        default: {
          this.goBack();
          break;
        }
      }

      this.typePersonSelect();
    });
  }

  /**
   * Busca os dados de um determinado parceiro
   */
  async organizesPartnerData() {
    const typesPartner = [];
    if (this.dataPartner && this.dataPartner.partnerType.length !== 0) {
      this.dataPartner.partnerType.forEach(element => {
        typesPartner.push(_.filter(this.typesPartner, {id: element['id']})[0]);
      });
    }

    this.dependences.emails.data_old = this.dataPartner['emails'];
    this.dependences.emails.data_new = _.cloneDeep(this.dependences.emails.data_old);

    this.dependences.phones.data_old = this.dataPartner['phones'];
    this.dependences.phones.data_new = _.cloneDeep(this.dependences.phones.data_old);

    this.dependences.adresses.data_old = this.dataPartner['adresses'];
    this.dependences.adresses.data_new = _.cloneDeep(this.dependences.adresses.data_old);

    await this.form_partner.patchValue({
      name_reason: this.dataPartner.name_reason,
      name_fantasy: this.dataPartner.name_fantasy,
      rg_ie: this.dataPartner.rg_ie,
      rg_uf: _.filter(this.rgUf, {key: this.dataPartner.rg_uf})[0],
      rg_issuing_body: _.filter(this.rgIssuingBody, {key: this.dataPartner.rg_issuing_body})[0],
      cpf_cnpj: this.dataPartner.cpf_cnpj,
      type_person: _.filter(this.typesPerson, { label: this.dataPartner.type_person })[0],
      sex: _.filter(this.sex, {key: this.dataPartner.sex})[0],
      active: !!this.dataPartner.active,
      types_partner: typesPartner
    });
    await this.typePersonSelect();
}

  /**
   * Tratamento dos dados para poder salvá-los
   */
  async save() {
    if (this.form.invalid) {
      return this.addToast('error', 'Erro', 'Formulário inválido. Verifique os dados e tente novamente!');
    }

    this.addToast('wait', 'Aguarde...', 'Estamos processando sua solicitação!', 30000, 104);

    const form = {};
    const form_partner = _.cloneDeep(this.form_partner.value);

    // Organiza os tipos do parceiro
    const idsTypesPartner = [];
    if (form_partner.types_partner.length !== 0) {
      form_partner.types_partner.forEach(element => {
        idsTypesPartner.push(element['id']);
      });
    }
    delete form_partner['types_partner'];

    // Seta o valor nas variaveis
    form['partner'] = form_partner;
    form['typesPartner'] = idsTypesPartner;

    if (this.typePersonFisic) {
      form['partner']['rg_uf'] = form['partner']['rg_uf']['key'];
      form['partner']['rg_issuing_body'] = form['partner']['rg_issuing_body']['key'];
      form['partner']['sex'] = form['partner']['sex']['key'];
    }

    /** Atualiza ou cadastra **/
    if (this.dataPartner) {
      await this.updatePartner(form);
    } else {
      form['partner']['type_person'] = form['partner']['type_person']['label'];
      form['partner']['cpf_cnpj'] = form['partner']['cpf_cnpj'].toString().replace(/\D+/g, '').trim();

      await this.postPartner(form);
    }

    if (this.dataPartner) {
      // Adiciona na variavel os endereços, telefone e email novos
      // Verifica se o elemento do NEW existe no OLD, se não existir o elemento foi adicionado
      const addEmail = [];
      this.dependences.emails.data_new.forEach(async (element, index) => {
        if (_.filter(this.dependences.emails.data_old, { id: element.id }).length === 0) {
          const email = _.cloneDeep(element);
          delete this.dependences.emails.data_new[index]['updated']; // Remove a propriedade de update do objeto que é novo e foi atualizado
          email['added_by'] = this.userLogged['id'];

          addEmail.push(email);
        }
      });
      if (addEmail.length !== 0) { await this.postEmail(addEmail, this.dataPartner['id']); }

      const addPhone = [];
      this.dependences.phones.data_new.forEach((element, index) => {
        if (_.filter(this.dependences.phones.data_old, { id: element.id }).length === 0) {
          const phone = _.cloneDeep(element);
          delete this.dependences.phones.data_new[index]['updated']; // Remove a propriedade de update do objeto que é novo e foi atualizado
          phone['added_by'] = this.userLogged['id'];

          addPhone.push(phone);
        }
      });
      if (addPhone.length !== 0) { await this.postPhone(addPhone, this.dataPartner['id']); }

      const addAddress = [];
      this.dependences.adresses.data_new.forEach((element, index) => {
        if (_.filter(this.dependences.adresses.data_old, { id: element.id }).length === 0) {
          const address = _.cloneDeep(element);
          delete this.dependences.adresses.data_new[index]['updated']; // Remove a propriedade de update do objeto que é novo e foi atualizado
          address['added_by'] = this.userLogged['id'];
          address['cep'] = address['cep'].toString().replace(/\D+/g, '').trim();

          addAddress.push(address);
        }
      });
      if (addAddress.length !== 0) { await this.postAddress(addAddress, this.dataPartner['id']); }

      // Atualizados
      // Verifica se o elemento de NEW existe a propriedade 'updated' como verdadeira, se existir o item foi atualizado
      this.dependences.emails.data_new.forEach(async element => {
        if (element['updated']) {
          const email = _.cloneDeep(element);

          await this.updateEmail(element['id'], {
            email: email.email,
            description: email.description,
            type: email.type,
            partner_id: email.partner_id,
            updated_by: this.userLogged['id']
          });
        }
      });

      this.dependences.phones.data_new.forEach(async element => {
        if (element['updated']) {
          const phone = _.cloneDeep(element);

          await this.updateEmail(element['id'], {
            ddi: phone.ddi.toString().trim(),
            ddd: phone.ddd.toString().trim(),
            number: phone.number.toString().replace(/\D+/g, '').trim(),
            telephone_extension: phone.telephone_extension,
            description: phone.description,
            type: phone.type,
            partner_id: phone.partner_id,
            updated_by: this.userLogged['id']
          });
        }
      });

      this.dependences.adresses.data_new.forEach(async element => {
        if (element['updated']) {
          const address = _.cloneDeep(element);

          await this.updateAddress(element['id'], {
            cep: address.cep.toString().replace(/\D+/g, '').trim(),
            street: address.street.toString().trim(),
            number: address.number,
            complement: address.complement,
            neighborhood: address.neighborhood.toString().trim(),
            city: address.city.toString().trim(),
            state: address.state.toString().trim(),
            partner_id: address.partner_id,
            updated_by: this.userLogged['id']
          });
        }
      });

      // Removidos
      // Verifica se o elemento do OLD existe no NEW, se não existir o item foi removido
      this.dependences.emails.data_old.forEach(async element => {
        if (_.filter(this.dependences.emails.data_new, { id: element.id }).length === 0) {
          await this.deleteEmail(element.id);
        }
      });

      this.dependences.phones.data_old.forEach(async element => {
        if (_.filter(this.dependences.phones.data_new, { id: element.id }).length === 0) {
          await this.deletePhone(element.id);
        }
      });

      this.dependences.adresses.data_old.forEach(async element => {
        if (_.filter(this.dependences.adresses.data_new, { id: element.id }).length === 0) {
          await this.deleteAddress(element.id);
        }
      });

      this.goBack(false, 'success', 'Sucesso', 'Sua solicitação foi processada!');
      this.toastyService.clear(104);
    }
  }

    /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableForm(): void {
    this.buttonEdit.visible = false;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = true;
    this.buttonCancel.visible = true;
    this.form.enable();
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableForm(): void {
    this.buttonEdit.visible = true;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = false;
    this.buttonCancel.visible = false;
    this.form.disable();
  }

  // Métodos de POST
  async postPartner(data) {
    await this._partners.post(data).then((result) => {
      this.dataPartner = result['data'];
    }, (err) => {
      this.toastyService.clearAll();

      if (err.status === 422) {
        err['error']['message'].forEach(element => {
          this.addToast('error', 'Duplicidade', element.message, 5000);
        });
      } else {
        this.handleError(err);
      }
    });
  }

  async postEmail(data, idPartner) {
    await this._emails.post(data, idPartner).then((result) => {}, (err) => this.handleError(err));
  }

  async postPhone(data, idPartner) {
    await this._phones.post(data, idPartner).then((result) => {}, (err) => this.handleError(err));
  }

  async postAddress(data, idPartner) {
    await this._addresses.post(data, idPartner).then((result) => {}, (err) => this.handleError(err));
  }

  // Métodos de UPDATE
  async updatePartner(data) {
    return await this._partners.update(data, this.dataPartner['id']).then(
      (result) => {},
      (err) => this.handleError(err)
    );
  }

  async updateEmail(id: number, data) {
    await this._emails.update(id, data).then(
      () => {},
      (err) => this.handleError(err)
    );
  }

  async updatePhone(id: number, data) {
    await this._phones.update(id, data).then(
      () => {},
      (err) => this.handleError(err)
    );
  }

  async updateAddress(id: number, data) {
    await this._addresses.update(id, data).then(
      () => {},
      (err) => this.handleError(err)
    );
  }

  // Métodos de REMOVE
  async deletePartner() {
    await swal({
      title: 'Exclusão',
      text: 'Você deseja realmente excluir este parceiro?',
      type: 'warning',
      timer: 5000,
      showCancelButton: true,
      cancelButtonText: 'Não',
      confirmButtonText: 'Sim, excluir!'
    }).then((result) => {

      if (result.value) {
        this._partners.delete(this.dataPartner['id']).then((data) => {
          this.goBack(true, 'success', 'Sucesso', 'Parceiro excluído com sucesso!');
        }, (err) => this.handleError(err));
      }

    }).catch(swal.noop);
  }

  async deleteEmail(id: number) {
    await this._emails.delete(id).then(
      () => {},
      (err) => this.handleError(err)
    );
  }

  async deletePhone(id: number) {
    await this._phones.delete(id).then(
      () => {},
      (err) => this.handleError(err)
    );
  }

  async deleteAddress(id: number) {
    await this._addresses.delete(id).then(
      () => {},
      (err) => this.handleError(err)
    );
  }

  /**
   * Seta a validação de cada input de acordo com o tipo de pessoa
   */
  setValidators() {
    if (this.typePersonFisic) {
      this.form_partner.controls['name_fantasy'].setValidators(null);
      this.form_partner.controls['cpf_cnpj'].setValidators([Validators.required, Validators.minLength(14), Validators.maxLength(14)]);
      this.form_partner.controls['type_person'].setValidators([Validators.required]);
      this.form_partner.controls['rg_ie'].setValidators([Validators.required, Validators.minLength(8), Validators.maxLength(8)]);
      this.form_partner.controls['rg_uf'].setValidators([Validators.required]);
      this.form_partner.controls['rg_issuing_body'].setValidators([Validators.required]);
      this.form_partner.controls['sex'].setValidators([Validators.required]);

      // Reseta
      this.form_partner.controls['name_fantasy'].reset();
    } else {
      this.form_partner.controls['name_fantasy'].setValidators([Validators.required]);
      this.form_partner.controls['cpf_cnpj'].setValidators([Validators.required, Validators.minLength(18), Validators.maxLength(18)]);
      this.form_partner.controls['type_person'].setValidators(null);
      this.form_partner.controls['rg_ie'].setValidators([Validators.minLength(14), Validators.maxLength(14)]);
      this.form_partner.controls['rg_uf'].setValidators(null);
      this.form_partner.controls['rg_issuing_body'].setValidators(null);
      this.form_partner.controls['sex'].setValidators(null);

      // Reseta
      this.form_partner.controls['rg_uf'].reset();
      this.form_partner.controls['rg_issuing_body'].reset();
      this.form_partner.controls['sex'].reset();
    }
  }

  /**
   * Verifica o tipo de pessoa selecionada
   */
  typePersonSelect(): void {
    if (this.form_partner.get('type_person').value && this.form_partner.get('type_person').value['key'] === 'juridica') {
      this.typePersonFisic = false;
      this.setValidators();
    } else {
      this.typePersonFisic = true;
      this.setValidators();
    }
  }

  /**
   * Válida o CPF/CNPJ
   * @param type Tipo de validação (cpf ou cnpj)
   */
  async validCpfCnpj(type: string) {
    const number = this.form_partner.get('cpf_cnpj').value || '';
    const result = await this.validadorCpfCNPJ.valida(number, type);

    if (result === false) {
      this.form_partner.get('cpf_cnpj').setErrors({ cpfCnpjValido: !result });
    }
  }

  /**
   * Filtra os tipos de phone para apresentar no autocomplete
   * @param {*} event Evento de objeto
   */
  filterTypesPartner(event: any): void {
    this.typesPartnerFiltered = [];
    for (let i = 0; i < this.typesPartner.length; i++) {
      const type = this.typesPartner[i];
      if (type['name'].toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
        this.typesPartnerFiltered.push(type);
      }
    }
  }

  /**
   * Volta para uma determinada página e envia um toast para o usuário
   * @param {boolean} [goList=false] Se verdadeiro volta para a listagem de parceiros, caso contrário volta para a última página
   * @param {string} type Tipo do toast de mensagem (success, error, warning, wait, default)
   * @param {string} title Título para o toast
   * @param {string} message Mensagem do toast
   */
  goBack(goList: boolean = false, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    if (goList === true) {
      this.router.navigate(['/parceiros']);
    } else {
      this.location.back();
    }
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.status) {
      case 422: err.message = 'A dados duplicados, verifique.'; break;
      case 429: err.message = 'Aguarde, você fez muitas requisições, no mesmo endereço, em menos de um minuto.'; break;
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
