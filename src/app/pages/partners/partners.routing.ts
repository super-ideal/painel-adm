import { Routes } from '@angular/router';
import { PartnersComponent } from './partners.component';
import { AclGuard } from '../../guards/acl.guard';
import { PartnersResolver } from './partners.resolver';

const breadcrumb = 'Parceiros';
const message = '';

export const PartnersRoutes: Routes = [
  {
    path: '',
    component: PartnersComponent,
    pathMatch: 'full',
    canActivate: [AclGuard],
    resolve: { partnersData: PartnersResolver },
    data: { permission: { page: 'PARTNERS', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fa-handshake bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fa-handshake bg-c-blue', status: true },
    children: [
      {
        path: 'cadastro',
        loadChildren: './partner-form/partner-form.module#PartnerFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PARTNERS', action: 'VIEW_ONE' } }
      },
    ]
  }
];
