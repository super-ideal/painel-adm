import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import * as _ from 'lodash';

import { UsersService } from '../../services/panel/users/users.service';
import { UserPermissionsService } from '../../services/panel/acl/user-permissions.service';
import { VerifyPermissions } from '../../shared/verify-permissions/verify-permissions';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

@Component({
  selector: 'app-permissions-form',
  templateUrl: './permissions-form.component.html',
  styleUrls: ['./permissions-form.component.scss']
})
export class PermissionsFormComponent implements OnInit {
  permissionsUserLogged: any;

  form: FormGroup;
  dataUser: any;
  dataUserCloned: any;
  displayListUsers = false;
  permissions: any;

  buttonEdit: Button = { visible: false, disabled: false };
  buttonSave: Button = { visible: false, disabled: false };
  buttonCancel: Button = { visible: false, disabled: false };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private renderer: Renderer2,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _userPermissionsService: UserPermissionsService,
    private _usersService: UsersService,
  ) {
    this.form = this.formBuilder.group({
      user_id: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.verifyRoute();
  }

  /**
   * Verifica o que o usuário deseja fazer para ativar as opções corretas
   * @param id Id único do usuário (opcional)
   */
  private verifyRoute(id?) {
    this.toastyService.clearAll();

    this.route.data.subscribe(async data => {
      let idUser;
      if (!id) {
        idUser = this.route.snapshot.params['id'];
      } else {
        idUser = id;
      }

      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões

      switch (data.permission.action) {
        case 'VIEW_ONE': {
          this.form.enable();
          await this.getDataUser(idUser);
          this.buttonEdit.visible = true;
          this.buttonSave.visible = false;
          this.buttonCancel.visible = false;
          break;
        }
        case 'EDIT': {
          this.form.disable();
          await this.getDataUser(idUser);
          this.buttonEdit.visible = false;
          this.buttonSave.visible = true;
          this.buttonCancel.visible = true;
          break;
        }
        default: {
          this.disableButtons();
          this.focusElement('user_id');
          break;
        }
      }

      this.form.get('user_id').setValue(idUser);
    });
  }

  /**
   * Ação para salvar as alterações de permissão do usuário
   */
  async save() {
    if (this.form.invalid) {
      this.addToast('error', 'Erro', 'Formulário inválido. Verifique os dados e tente novamente!');
      return;
    }

    this.addToast('wait', 'Aguarde...', 'Estamos processando sua solicitação!', 15000, 100);
      if (this.permissions && (this.permissions.newPermissions || this.permissions.deletePermissions)) {
        await this._userPermissionsService.update(this.permissions, this.dataUser.id)
        .then(() => {
          this.addToast('success', 'Sucesso', 'Todas as alterações foram salvas com sucesso!');
          this.goBack();
        }, (err) => {
          this.handleError(err);
        });
      } else {
        this.buttonEdit.visible = true;
        this.buttonSave.visible = false;
        this.buttonCancel.visible = false;
        this.form.enable();
        this.addToast('default', 'Informação', 'Não encontramos nenhuma alteração nas permissões do usuário');
      }
    this.toastyService.clear(100);
  }

  /**
   * Busca o usuário
   * @param openModal Verdadeiro se desejar abrir o modal para listar os usuários
   * @param idUser Id do usuário para buscar os dados do mesmo
   */
  searchUser(openModal: boolean, idUser?: number) {
    if (openModal) {
      return this.displayListUsers = true;
    }

    if (idUser) {
      this.displayListUsers = false;
      this.router.navigate(['/permissoes/' + idUser]);
      this.verifyRoute(idUser);
    }
  }

  /**
   * Desabilita os botões de ações na página
   */
  disableButtons() {
    this.form.enable();
    this.buttonEdit.visible = false;
    this.buttonSave.visible = false;
    this.buttonCancel.visible = false;
  }

  /**
   * Busca os dados do usuário
   * @param idUser Id do usuário
   */
  async getDataUser(idUser: number) {
    this._usersService.get(idUser).then(async (res) => {
      this.dataUser = res['data'];
      this.addToast('success', 'Localizado', 'Usuário foi localizado com sucesso!');
    }, err => {
      this.dataUser = null;
      this.router.navigate(['/permissoes/']);

      if (err.status === 404) {
        this.addToast('warning', 'Não encontrado', 'O usuário informado não foi encontrado');
      } else {
        this.addToast('err', 'Erro', 'Não conseguimos buscar o usuário. Tente novamente!');
      }
    });
  }

  /**
   * Recebe as atualização da permissão do usuário
   * @param data Permições novas e a serem excluidas
   */
  permissionsUser(data) {
    this.permissions = data;
  }

  /**
   * Volta para uma determinada página
   */
  goBack(type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    this.location.back();
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  private handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.status) {
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
    * Toast de alertas
    * @param {string} type Tipo do toast (success, error, warning, wait, default)
    * @param {string} title Título do toast
    * @param {string} message Mensagem do toast
    * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
    * @param {string} [idToast] Id único para o toast
    */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
