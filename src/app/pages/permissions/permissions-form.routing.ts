import { Routes } from '@angular/router';
import { PermissionsFormComponent } from './permissions-form.component';
import { AclGuard } from '../../guards/acl.guard';

const breadcrumb = 'Permissões';
const message = '';

export const PermissionsFormRoutes: Routes = [
  {
    path: '',
    component: PermissionsFormComponent,
    canActivate: [AclGuard],
    data: { permission: { page: 'PERMISSIONS', action: 'ACCESS' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-lock bg-c-blue', status: true },
  },
  {
    path: '',
    data: { icon: 'fas fa-lock bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: PermissionsFormComponent,
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PERMISSIONS', action: 'VIEW_ONE' }, breadcrumb: breadcrumb, message: message }
      },
      {
        path: ':id/editar',
        component: PermissionsFormComponent,
        canActivateChild: [AclGuard],
        data: { permission: { page: 'PERMISSIONS', action: 'EDIT' }, breadcrumb: breadcrumb, message: message }
      },
    ]
  }
];
