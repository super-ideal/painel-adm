import { Component, Output, EventEmitter, Input, AfterContentInit } from '@angular/core';
import { ToastOptions, ToastData, ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';

import { UsersService } from '../../../../services/panel/users/users.service';

import { Pagination } from '../../../../interfaces/pagination.interface';

interface HeaderTable {
  field: string;
  header: string;
}

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements AfterContentInit {
  search: {
    whereBy: string,
    whereOperator: string,
    whereTextSearch: string,
    searchOptions: Array<{ label: string, value: string, type: string }>,
    searchOptionsBoolean: Array<{ label: string, value: string }>,
    searchOptionsOperator: Array<{ label: string, value: string }>,
  };

  table: {
    cols: HeaderTable[],
    data: any[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number
  };
  selectedRowTable: any;

  productsOffer: any = [];

  /** Envia o usuário que foi selecionado */
  @Output()
  userSelectedEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _usersService: UsersService,
    private toastyService: ToastyService
  ) {
    this.table = {
      cols: [
        { field: 'id', header: 'Id' },
        { field: 'active', header: 'Ativo' },
        { field: 'username', header: 'Usuário' },
        { field: 'status', header: 'Status' },
        { field: 'name_reason', header: 'Parceiro' },
        { field: 'cpf_cnpj', header: 'CPF/CNPJ' },
        { field: 'type_person', header: 'Tipo de Pessoa' },
      ],
      data: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10
    };

    this.search = {
      whereBy: 'name_reason',
      whereTextSearch: '',
      whereOperator: 'anywhere',
      searchOptions: [
        { label: 'Usuário', value: 'username', type: 'text' },
        { label: 'Nome/Razão Social', value: 'name_reason', type: 'text' },
        { label: 'Status', value: 'status', type: 'text' },
        { label: 'CPF/CNPJ', value: 'cpf_cnpj', type: 'text' },
        { label: 'Ativo', value: 'active', type: 'boolean' },
        { label: 'Tipo de Pessoa', value: 'type_person', type: 'text' },
        { label: 'Id', value: 'id', type: 'number' },
      ],
      searchOptionsBoolean: [
        { label: 'Sim', value: '1' },
        { label: 'Não', value: '0' },
        { label: 'Todos', value: '2' },
      ],
      searchOptionsOperator: []
    };
  }

  async ngAfterContentInit() {
    this.selectTheOperators();
    await this.getAllUsers();
  }

  /**
   * Envia o id do usuário selecionado para a página PermissionsForm
   */
  addUserSelected(user) {
    this.userSelectedEvent.emit({ idUser: user.id });
  }

  /**
   * Informa os operadores permitos para fazer a busca
   */
  selectTheOperators() {
    const whereByTypeSelected = _.filter(this.search.searchOptions, { value: this.search.whereBy })[0]['type'];

    switch (whereByTypeSelected) {
      case 'boolean': {
        if (this.search.whereTextSearch === '2') {
          this.search.whereOperator = 'different';
        } else {
          this.search.whereOperator = 'equal';
        }

        this.search.searchOptionsOperator = [];
        break;
      }
      case 'number': {
        this.search.searchOptionsOperator = [
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Menor que', value: 'smaller' },
          { label: 'Maior que', value: 'bigger' },
          { label: 'Contido em', value: 'contained' }
        ];
        break;
      }
      default: {
        this.search.searchOptionsOperator = [
          { label: 'Em qualquer lugar', value: 'anywhere' },
          { label: 'Começado com', value: 'started' },
          { label: 'Terminado com', value: 'finished' },
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Contido em', value: 'contained' }
        ];
        break;
      }
    }
  }

  /**
  * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
  */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllUsers();
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  getAllUsers() {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando alguns dados para você!', 20000, 100);

    // Seleciona o operador correto antes de pesquisar (Serve apenas para caso for booleano)
    this.selectTheOperators();

    this._usersService.getAll({
      pageNumber: this.table.pageNumber,
      perPage: this.table.rowsOnPage,
      whereBy: this.search.whereBy,
      operator: this.search.whereOperator,
      whereText: this.search.whereTextSearch.trim()
    }).then(data => {
      this.table.elementsPagination = data['data'];
      this.table.data = data['data']['data'];

      this.toastyService.clear(100);
    }, (err) => {
      this.handleError(err);
    });
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any, type: string = 'error', title: string = 'Erro') {
    this.toastyService.clearAll();

    if (err.status === 404) {
      type = 'warning';
      title = 'Atenção';
      this.table.data = [];
    }

    switch (err.name && err.status) {
      case 404: err.message = 'Não encontramos nenhum usuário com os dados informados'; break;
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast(type, title, err.message);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
