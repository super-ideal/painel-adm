
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { TreeTableModule } from 'primeng/treetable';
import { DialogModule } from 'primeng/dialog';

import { SharedModule } from '../../../../shared/shared.module';

import { PermissionsComponent } from './permissions.component';
import { ListUsersModule } from '../list-users/list-users.module';

@NgModule({
  imports: [
    ListUsersModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    DialogModule,
    TreeTableModule,
    TableModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [PermissionsComponent],
  exports: [PermissionsComponent]
})
export class PermissionsModule { }
