import { Component, Output, EventEmitter, Input, AfterContentInit } from '@angular/core';
import { ToastOptions, ToastData, ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';

import { TreeNode } from 'primeng/components/common/treenode';
import { PageActionPermissionService } from '../../../../services/panel/acl/page-action.service';
import { PagePermissionService } from '../../../../services/panel/acl/pages.service';
import { UserPermissionsService } from '../../../../services/panel/acl/user-permissions.service';
import { UsersService } from '../../../../services/panel/users/users.service';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements AfterContentInit {
  userId: number;
  userData: any;
  displayListUsers: false;
  permissionsUserSelected: any[] = []; // serve apenas para mostrar ao usuário quais as ações estão selecioandas
  permissionsUser: any[] = []; // permissões que o usuário tem
  permissionsUserNew: any[] = []; // Novas permissões
  permissionsUserRemoved: any[] = []; // Permições removidas

  buttonActive: false;
  clone = false;

  pages: any;
  permissionsTreeOrganized: TreeNode[] = [];
  permissionsActions: any[] = [];

  verifyAndSelectPermissionsData = [];

  /** Envia as alteraç~eos das permissões */
  @Output()
  permissionsEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private toastyService: ToastyService,
    private _pageActionPermissionService: PageActionPermissionService,
    private _pagePermissionService: PagePermissionService,
    private _userPermissionsService: UserPermissionsService,
    private _usersService: UsersService,
  ) {
  }

  async ngAfterContentInit() {
    await this.getPages();
    await this.getPermissions();
  }

  @Input()
  set idUserEvent(id) {
    this.userData = null;
    this.userId = null;

    if (id) {
      this.userId = id;
      this.getPermissionsUser(id);
    }
  }

  @Input()
  set buttonSaveActive(event) {
    if (event) {
      this.buttonActive = event;
    } else {
      // Se clicar em salvar ou cancelar
      this.buttonActive = event;
      this.userData = null;
      this.permissionsUserSelected = this.permissionsUser;
      this.permissionsUserNew = [];
      this.permissionsUserRemoved = [];
    }
  }

  /**
   * Faz a busca do usuário e suas permissões
   * @param idUser ID único do usuário
   * @param clone Informa se está clonando as permissões
   */
  async searchDataUser(idUser: number, clone: boolean = false) {
    if (idUser.toString() === this.userId.toString()) {
      return this.addToast('warning', 'Alerta', 'Não é permitido clocar as permissões do mesmo usuário');
    }

    this.clone = clone;
    this.displayListUsers = false;
    await this.getDataUser(idUser);
  }

/**
 * Busca os dados do usuário
 * @param idUser Id do usuário
 */
  async getDataUser(idUser: number) {
    this._usersService.get(idUser).then(async (res) => {
      this.userData = res['data'];
      this.getPermissionsUser(this.userData.id);
    }, err => {
      if (err.status === 404) {
        this.addToast('warning', 'Não encontrado', 'O usuário informado não foi encontrado');
      } else {
        this.addToast('err', 'Erro', 'Não conseguimos buscar o usuário. Tente novamente!');
      }
    });
  }

  /**
   * Busca as permissões do usuário
   * @param idUser Id do usuário
   */
  private getPermissionsUser(idUser: number) {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando as permissões do usuário.', 15000, 102);

    this._userPermissionsService.getIdUser(idUser)
      .then(async (res) => {
        this.permissionsUserNew = [];
        this.permissionsUserRemoved = [];
        const permissions = [];

        _.forEach(res['data'], function (a) {
          permissions.push({
            id: a.action_id,
            name: a.action_name,
            description: a.action_description,
            page_action_id: a.page_action_id
          });
        });

        // Se estiver clonando as permissões
        if (this.clone) {
          permissions.forEach(element => {
            this.permissionsUserNew.push(element.page_action_id);
          });

          this.permissionsUser.forEach(element => {
            this.permissionsUserRemoved.push(element.page_action_id);
          });

          this.permissionsEvent.emit({
            newPermissions: this.permissionsUserNew,
            deletePermissions: this.permissionsUserRemoved
          });
        }

        this.permissionsUser = permissions;
        this.permissionsUserSelected = permissions;
        this.toastyService.clear(102);
      }, err => {
        this.handleError(err);
      });
  }

  /**
   * Busca todas as permissões cadastradas na base de dados
   */
  async getPermissions() {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando as permissões na base de dados.', 15000, 103);

    await this._pageActionPermissionService.getAll()
      .then((res) => {
        this.permissionsTreeOrganized = this.organizePermissions(res['data'], true);
        this.toastyService.clear(103);
      }, err => {
        this.handleError(err);
      });
  }

  /**
 * Busca todas as páginas cadastradas
 */
  async getPages() {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando as páginas na base de dados.', 15000, 104);

    await this._pagePermissionService.getAll()
      .then((result) => {
        this.pages = result['data'];
        this.toastyService.clear(104);
      }, (err) => {
        this.toastyService.clear(104);
        this.handleError(err);
      });
  }

  /**
   * Preenche o formulário de permições, com as ações especificas da página selecionada
   * @param selected Dados da página selecionada
   */
  set selectedPageTree(selected: any) {
    this.permissionsActions = [];
    const actions = [];
    if (selected && selected.actions !== undefined) {
      _.forEach(selected.actions, function (a) {
        actions.push({
          id: a.data.id,
          name: a.data.name,
          description: a.data.description,
          page_action_id: a.data.page_action_id
        });
      });
    }

    this.permissionsActions = actions;
  }

  /**
   * Tratamento da ação desmarcada
   * @param event Dados da ação desmarcada
   */
  onRowUnselectAction(event) {
    const indexPermissionUser = _.findIndex(this.permissionsUser, { page_action_id: event.data.page_action_id });
    const indexPermissionUserNew = _.findIndex(this.permissionsUserNew, { page_action_id: event.data.page_action_id });

    if (indexPermissionUser !== -1) {
      this.permissionsUserRemoved.push(event.data.page_action_id);
    }

    // Remove a ação desmarcada da variavel que guarda as novas ações do usuário
    if (indexPermissionUserNew > -1) {
      this.permissionsUserNew.splice(indexPermissionUserNew, 1);
    }

    this.permissionsEvent.emit({
      newPermissions: this.permissionsUserNew,
      deletePermissions: this.permissionsUserRemoved
    });
  }

  /**
   * Tratamento da ação selecionada
   * @param event Dados da ação selecionada
   */
  onRowSelectAction(event) {
    // Verifica se a permissão selecionada já existia nas permissão do usuário
    const indexPermissionUser = _.findIndex(this.permissionsUser, { page_action_id: event.data.page_action_id });
    const indexPermissionUserNew = _.findIndex(this.permissionsUserNew, { page_action_id: event.data.page_action_id });
    const indexPermissionUserRemoved = this.permissionsUserRemoved.indexOf(event.data.page_action_id);

    if (indexPermissionUser === -1 && indexPermissionUserNew === -1) {
      this.permissionsUserNew.push(event.data.page_action_id);
    }

    // Remove a ação selecionada da variavel que guarda as ações há remover
    if (indexPermissionUserRemoved > -1) {
      this.permissionsUserRemoved.splice(indexPermissionUserRemoved, 1);
    }

    this.permissionsEvent.emit({
      newPermissions: this.permissionsUserNew,
      deletePermissions: this.permissionsUserRemoved
    });
  }

  /**
   * Organiza as permissões para preencher o Tree
   * @param data Dados das permissões
   */
  private organizePermissions(data: any, viewAction: boolean = false): any {
    const dataProcessed = [];

    // Organiza as páginas e ações
    _.forEach(data, function (p) {
      const indexPage = _.findIndex(dataProcessed, { data: { id: p.page_id } });
      if (indexPage === -1) {
        const org = {
          data: {
            id: p.page_id,
            name: p.page_name,
            description: p.page_description, // label é o que aparece para o usuário selecionar
            level: p.page_level,
            tree: p.page_tree,
            type: 'page'
          },
          children: []
        };

        if (viewAction && p.page_action_id) {
          if (org['actions'] === undefined) {
            org['actions'] = [];
          }

          org['actions'].push({
            data: {
              page_action_id: p.page_action_id,
              id: p.action_id,
              name: p.action_name,
              description: p.action_description, // label é o que aparece para o usuário selecionar
              type: 'action'
            }
          });
        }

        dataProcessed.push(org);

      } else if (viewAction) {
        dataProcessed[indexPage]['actions'].push({
          data: {
            page_action_id: p.page_action_id,
            id: p.action_id,
            name: p.action_name,
            description: p.action_description, // label é o que aparece para o usuário selecionar
            type: 'action'
          }
        });
      }
    });

    // Organiza os níveis das páginas
    // Para funcionar o level tem que estar ordenado do maior para o menor
    const pages = this.pages;
    _.forEach(dataProcessed, function (dp) {
      if (dp.data.tree) {
        let indexTree = _.findIndex(dataProcessed, { data: { id: dp.data.tree } });

        if (indexTree && indexTree > -1) {
          dataProcessed[indexTree]['children'].push(dp);
        } else {
          indexTree = _.findIndex(pages, { id: dp.data.tree } );

          if (indexTree) {
            const org = {
              data: {
                id: pages[indexTree].id,
                name: pages[indexTree].name,
                description: pages[indexTree].description, // label é o que aparece para o usuário selecionar
                level: pages[indexTree].level,
                tree: pages[indexTree].tree,
                type: 'page'
              },
              children: []
            };
            org['children'].push(dp);

            dataProcessed.push(org);
          }
        }
      }
    });

    // Remove as páginas que são de level ou que não tem ações
    const dataProcessedFinal = [];
    _.forEach(dataProcessed, function (dp) {
      if (dp && !dp.data.level) {
        dataProcessedFinal.push(dp);
      }
    });

    return dataProcessedFinal;
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any, type: string = 'error', title: string = 'Erro') {
    this.toastyService.clearAll();

    switch (err.name && err.status) {
      case 404: {
        if (err.error && err.error.code === 'no-relationship') {
          err.message = 'Erro interno na permissão da página/ação solicitada'; break;
        } else {
          err.message = 'Não encontramos nenhum usuário com os dados informados'; break;
        }
      }
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast(type, title, err.message);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
