import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { DialogModule } from 'primeng/dialog';

import { PermissionsFormComponent } from './permissions-form.component';
import { PermissionsFormRoutes } from './permissions-form.routing';

import { SharedModule } from '../../shared/shared.module';
import { PermissionsModule } from './components/permissions/permissions.module';
import { ListUsersModule } from './components/list-users/list-users.module';
import { PermissionsFormResolver } from './resolvers/permissions-form.resolver';

@NgModule({
  imports: [
    ListUsersModule,
    PermissionsModule,
    CommonModule,
    RouterModule.forChild(PermissionsFormRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    SweetAlert2Module,
    DialogModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [PermissionsFormComponent],
  providers: [PermissionsFormResolver]
})
export class PermissionsFormModule { }
