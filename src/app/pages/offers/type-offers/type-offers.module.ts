import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';

import { SharedModule } from '../../../shared/shared.module';

import { TypeOffersComponent } from './type-offers.component';
import { TypesOffersRoutes } from './type-offers.routing';
import { TypeOffersResolver } from './type-offers.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(TypesOffersRoutes),
    SharedModule,
    TableModule,
    PaginatorModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [TypeOffersComponent],
  providers: [TypeOffersResolver]
})
export class TypeOffersModule { }
