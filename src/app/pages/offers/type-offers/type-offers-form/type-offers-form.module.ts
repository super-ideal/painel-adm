import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';

import { SharedModule } from '../../../../shared/shared.module';

import { TypeOffersFormComponent } from './type-offers-form.component';
import { TypeOffersFormRoutes } from './type-offers-form.routing';
import { TypeOfferResolver } from './resolvers/type-offers.resolver';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(TypeOffersFormRoutes),
    SharedModule,
    InputSwitchModule,
    InputTextareaModule,
    SweetAlert2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [TypeOffersFormComponent],
  providers: [TypeOfferResolver]
})
export class TypeOffersFormModule { }
