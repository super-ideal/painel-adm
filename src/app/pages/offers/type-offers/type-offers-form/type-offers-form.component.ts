import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';

import { OffersTypesService } from '../../../../services/panel/offers/offersType.service';
import { VerifyPermissions } from '../../../../shared/verify-permissions/verify-permissions';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

@Component({
  selector: 'app-type-offers-form',
  templateUrl: './type-offers-form.component.html',
  styleUrls: ['./type-offers-form.component.scss']
})
export class TypeOffersFormComponent implements OnInit {
  permissionsUserLogged: any;

  form: FormGroup;
  typeData: any;

  buttonEdit: Button = {visible: false, disabled: false};
  buttonSave: Button = {visible: false, disabled: false};
  buttonCancel: Button = {visible: false, disabled: false};
  editable: Boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private renderer: Renderer2,
    private verifyPermissions: VerifyPermissions,
    private toastyService: ToastyService,
    private _offersTypesService: OffersTypesService,
  ) {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      active: [true],
      description: ['', [Validators.maxLength(255)]],
      opt_top_offer: [false],
      opt_discount: [false],
      opt_discount_price_two: [false],
      opt_cumulative: [false],
      opt_cumulative_payable: [false],
      opt_wholesale: [false],
      opt_price_group: [false],
      opt_limit_per_coupon: [false],
      opt_view_percent_discount: [false]
    });
  }

  ngOnInit() {
    this.disableForm();

    this.route.data.subscribe(async data => {
      this.typeData = data['typeData'];
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões

      switch (data.permission.action) {
        case 'VIEW_ONE': {
          await this.setDataForm();
          break;
        }
        case 'EDIT': {
          await this.setDataForm();
          this.enableForm();
          this.focusElement('name');
          break;
        }
        case 'NEW': {
          this.enableForm();
          this.focusElement('name');
          break;
        }
        default: {
          this.goBack();
          break;
        }
      }
    });
  }

  /**
   * Seta as informações no formulário
   */
  async setDataForm() {
    this.form.patchValue({
      active: this.typeData.active,
      name: this.typeData.name,
      description: this.typeData.description,
      opt_top_offer: !!this.typeData.opt_top_offer,
      opt_discount: !!this.typeData.opt_discount,
      opt_discount_price_two: !!this.typeData.opt_discount_price_two,
      opt_cumulative: !!this.typeData.opt_cumulative,
      opt_cumulative_payable: !!this.typeData.opt_cumulative_payable,
      opt_wholesale: !!this.typeData.opt_wholesale,
      opt_price_group: !!this.typeData.opt_price_group,
      opt_limit_per_coupon: !!this.typeData.opt_limit_per_coupon,
      opt_view_percent_discount: !!this.typeData.opt_view_percent_discount
    });
    this.editable = !!this.typeData['editable'];
  }

  /**
   * Verificação e ajustes para salvar as informações
   */
  async save() {
    if (this.form.invalid) {
      this.addToast('error', 'Erro', 'Formulário inválido. Verifique os dados e tente novamente!');
      return;
    }

    this.addToast('wait', 'Aguarde...', 'Estamos processando sua solicitação!', 15000, 100);

    if (this.typeData) {
      await this.updateType(this.form.value);
    } else {
      await this.addType(this.form.value);
    }

    this.toastyService.clear(100);
  }

   /**
   * Crud
   */
  async addType(data: object) {
    await this._offersTypesService.post(data).then((result) => {
      this.goBack(false, 'success', 'Sucesso', 'O Tipo de Oferta foi salvo com sucesso!');
    }, (err) => this.handleError(err));
  }

  async updateType(data: object) {
    await this._offersTypesService.patch(data, this.typeData.id).then((result) => {
      this.goBack(false, 'success', 'Sucesso', 'O Tipo de Oferta foi atualizado com sucesso!');
    }, (err) => this.handleError(err));
  }

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableForm(): void {
    this.buttonEdit.visible = false;
    this.buttonSave.visible = true;
    this.buttonCancel.visible = true;
    this.form.enable();
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableForm(): void {
    this.buttonEdit.visible = true;
    this.buttonSave.visible = false;
    this.buttonCancel.visible = false;
    this.form.disable();
  }


  /**
   * Volta para uma determinada página
   */
  goBack(goList: boolean = false, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    if (goList === true) {
      this.router.navigate(['/ofertas/tipos']);
    } else {
      this.location.back();
    }
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.error.code) {
      case 'ER_DUP_ENTRY': err.error.message = 'O nome informado já esta cadastrado.'; break;
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.error.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
