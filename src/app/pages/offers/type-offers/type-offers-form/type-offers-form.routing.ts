import { Routes } from '@angular/router';
import { TypeOffersFormComponent } from './type-offers-form.component';
import { TypeOfferResolver } from './resolvers/type-offers.resolver';
import { AclGuard } from '../../../../guards/acl.guard';

export const TypeOffersFormRoutes: Routes = [
  {
    path: '',
    component: TypeOffersFormComponent,
    canActivate: [AclGuard],
    data: { permission: { page: 'OFFERS_TYPES', action: 'NEW' }, breadcrumb: 'Cadastro', icon: 'fas fa-list-alt bg-c-blue', status: true }
  },
  {
    path: '',
    data: { icon: 'fas fa-list-alt bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: TypeOffersFormComponent,
        canActivateChild: [AclGuard],
        resolve: { typeData: TypeOfferResolver },
        data: { permission: { page: 'OFFERS_TYPES', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' }
      },
      {
        path: ':id/editar',
        component: TypeOffersFormComponent,
        canActivateChild: [AclGuard],
        resolve: { typeData: TypeOfferResolver },
        data: { permission: { page: 'OFFERS_TYPES', action: 'EDIT' }, breadcrumb: 'Edição' }
      },
    ]
  }
];
