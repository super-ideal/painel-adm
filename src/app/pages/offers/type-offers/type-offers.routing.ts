import { Routes } from '@angular/router';
import { TypeOffersComponent } from './type-offers.component';
import { AclGuard } from '../../../guards/acl.guard';
import { TypeOffersResolver } from './type-offers.resolver';

const breadcrumb = 'Tipos de Agenda';
const message = '';

export const TypesOffersRoutes: Routes = [
  {
    path: '',
    component: TypeOffersComponent,
    canActivate: [AclGuard],
    resolve: { typeData: TypeOffersResolver },
    data: { permission: { page: 'OFFERS_TYPES', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: 'cadastro',
        loadChildren: './type-offers-form/type-offers-form.module#TypeOffersFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'OFFERS_TYPES', action: 'VIEW_ONE' } }
      },
    ]
  }
];
