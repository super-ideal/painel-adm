import { Routes } from '@angular/router';
import { OfferFormComponent } from './offer-form.component';
import { AclGuard } from '../../../guards/acl.guard';
import { OfferFormResolver } from './resolvers/offer-form.resolver';
import { OfferTypesResolver } from './resolvers/offer-types.resolver';

export const OfferFormRoutes: Routes = [
  {
    path: '',
    component: OfferFormComponent,
    pathMatch: 'full',
    canActivate: [AclGuard],
    resolve: { typesData: OfferTypesResolver },
    data: { permission: { page: 'OFFERS', action: 'NEW' }, breadcrumb: 'Cadastro', icon: 'fas fa-cart-arrow-down bg-c-blue', status: true }
  },
  {
    path: '',
    data: { icon: 'fas fa-cart-arrow-down bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: OfferFormComponent,
        canActivateChild: [AclGuard],
        resolve: { offerData: OfferFormResolver, typesData: OfferTypesResolver },
        data: { permission: { page: 'OFFERS', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' }
      },
      {
        path: ':id/editar',
        component: OfferFormComponent,
        canActivateChild: [AclGuard],
        resolve: { offerData: OfferFormResolver, typesData: OfferTypesResolver },
        data: { permission: { page: 'OFFERS', action: 'EDIT' }, breadcrumb: 'Edição' }
      },
    ]
  }
];
