import { Component, OnInit, Renderer2 } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import * as moment from 'moment';
import * as _ from 'lodash';

import { OfferService } from '../../../services/panel/offers/offers.service';

import { Offer } from '../../../class/offer/offer';
import { VerifyPermissions } from '../../../shared/verify-permissions/verify-permissions';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

@Component({
  selector: 'app-offer-form',
  templateUrl: './offer-form.component.html',
  styleUrls: ['./offer-form.component.scss']
})
export class OfferFormComponent implements OnInit {
  permissionsUserLogged: any;

  data: Offer = new Offer();
  status = '';
  types: any[];
  viewProducts: Boolean = false;

  calendar: {
    language: any,
    datetime_start: { minDate: Date, maxDate: Date },
    datetime_end: { minDate: Date, maxDate: Date }
  };

  form: FormGroup;

  buttonEdit: Button = {visible: false, disabled: false};
  buttonSave: Button = {visible: false, disabled: false};
  buttonCancel: Button = {visible: false, disabled: false};

  constructor(
    private route: ActivatedRoute,
    private renderer: Renderer2,
    private router: Router,
    private location: Location,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _offerService: OfferService,
  ) {
    this.calendar = {
      language: {
        'pt_BR': {
          firstDayOfWeek: 1,
          dayNames: [ 'domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado' ],
          dayNamesShort: [ 'dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb' ],
          dayNamesMin: [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
          // tslint:disable-next-line:max-line-length
          monthNames: [ 'janeiro ', 'fevereiro ', 'março ', 'abril ', 'maio ', 'junho ', 'julho ', 'agosto ', 'setembro ', 'outubro ', 'novembro ', 'dezembro ' ],
          monthNamesShort: [ 'jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez' ],
          today: 'Hoje',
          clear: 'Limpar'
        }
      },
      datetime_start: { minDate: new Date(), maxDate: null },
      datetime_end: { minDate: new Date(), maxDate: null }
    };

    this.form = new FormGroup({
      active: new FormControl(true),
      name: new FormControl(null, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
      description: new FormControl(null, Validators.maxLength(255)),
      datetime_start: new FormControl(null, Validators.required),
      datetime_end: new FormControl(null, Validators.required),
      offer_types_id: new FormControl(null, Validators.required),
      status: new FormControl(null),
    });
  }

  /** Executa apenas na primeira vez que a página foi carregada */
  ngOnInit() {
    this.disableForm();

    this.route.data.subscribe(async data => {
      this.types = data['typesData'];
      this.data = data['offerData'];
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões

      switch (data.permission.action) {
        case 'VIEW_ONE': {
          await this.setDataForm();
          this.viewProducts = true;
          break;
        }
        case 'EDIT': {
          await this.setDataForm();
          this.enableForm();

          if (this.status === 'ANDAMENTO') {
            this.form.get('datetime_start').disable();
            this.form.get('offer_types_id').disable();
            this.calendar.datetime_end.minDate = this.form.get('datetime_end').value;
          } else if (this.status === 'FINALIZADA') {
            this.goScheduleOffer(this.data['id'], 'info', 'Edição não permitida', 'Esta agenda de oferta já finalizou.');
          }

          this.focusElement('name');
          break;
        }
        case 'NEW': {
          this.enableForm();
          break;
        }
        default: {
          this.goBack();
          break;
        }
      }
    });
  }

  /**
   * Seta as informações no formulário
   */
  async setDataForm() {
    if (this.data) {
      this.form.patchValue({
        active: !!this.data['active'],
        name: this.data['name'],
        description: this.data['description'],
        datetime_start: new Date(this.data['datetime_start']),
        datetime_end: new Date(this.data['datetime_end']),
        offer_types_id: _.filter(this.types, { id: this.data['offer_types_id'] })[0],
        status: this.data['status'],
      });
      this.status = this.data['status'];
    }
  }

  /**
   * Verificação e ajustes para salvar as informações
   */
  async saveScheduleOffer() {
    if (this.form.invalid) {
      this.addToast('error', 'Erro', 'Formulário inválido. Verifique os dados e tente novamente!');
      return;
    } else if (this.status === 'FINALIZADA') {
      this.goScheduleOffer(this.data['id'], 'info', 'Edição não permitida', 'Esta agenda de oferta já finalizou.');
    }

    this.addToast('wait', 'Aguarde...', 'Estamos processando sua solicitação!', 15000, 100);

    this.form.removeControl('status');
    const form = this.form.value;

    if (form['offer_types_id']) {
      form['offer_types_id'] = form['offer_types_id']['id'];
    }

    if (form['datetime_start']) {
      form['datetime_start'] = moment(form['datetime_start']).format('YYYY-MM-DDTHH:mm');
    }

    form['datetime_end'] = moment(form['datetime_end']).format('YYYY-MM-DDTHH:mm');

    if (this.data) {
      await this.updateScheduleOffer(form);
    } else {
      await this.addScheduleOffer(form);
    }

    this.toastyService.clear(100);
  }

  /**
   * Crud de Schedule Offer
   */
  async addScheduleOffer(data: any) {
    await this._offerService.post(data).then((result) => {
      return this.goScheduleOffer(result['data']['id'], 'success', 'Sucesso', 'A agenda de oferta foi salva com sucesso!');
    }, (err) => this.handleError(err));
  }

  async updateScheduleOffer(data: any) {
    await this._offerService.update(data, this.data.id).then((result) => {
      return this.goScheduleOffer(this.data.id, 'success', 'Sucesso', 'A agenda de oferta foi atualizada com sucesso!');
    }, (err) => this.handleError(err));
  }

  setTimeFromEnd(event: any) {
    this.calendar.datetime_end.minDate = this.form.get('datetime_start').value;
    this.form.get('datetime_end').setValue(null);
  }

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableForm(): void {
    this.buttonEdit.visible = false;
    this.buttonSave.visible = true;
    this.buttonCancel.visible = true;
    this.form.enable();
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableForm(): void {
    this.buttonEdit.visible = true;
    this.buttonSave.visible = false;
    this.buttonCancel.visible = false;
    this.form.disable();
  }

  /**
   * Volta para uma determinada página
   */
  goBack(goList: boolean = false, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    if (goList === true) {
      this.router.navigate(['/ofertas']);
    } else {
      this.location.back();
    }
  }

  goScheduleOffer(idOffer: number, type?: string, title?: string, message?: string): void {
    this.addToast(type, title, message);
    this.router.navigate(['/ofertas/cadastro/' + idOffer]);
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();
    console.log(err);

    switch (err.error.code) {
      case 'ER_DUP_ENTRY': err.error.message = 'O nome informado já esta cadastrado.'; break;
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.error.message);
  }

  /**
   * Atribui o foco em um elemento
   * @param {string} id Id do input que receberá o foco
   * @param {number} [time=0] Tempo em milissegundos para focar no input
   */
  private focusElement(id: string, time: number = 0) {
    const element = this.renderer.selectRootElement('#' + id);
    setTimeout(() => element.focus(), time * 1000);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
