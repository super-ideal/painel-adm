import { Routes } from '@angular/router';
import { MotiveOffersComponent } from './motive-offers.component';
import { AclGuard } from '../../../../guards/acl.guard';
import { MotiveOffersResolver } from './motive-offers.resolver';

const breadcrumb = 'Motivos de Oferta';
const message = '';

export const MotiveOffersRoutes: Routes = [
  {
    path: '',
    component: MotiveOffersComponent,
    canActivate: [AclGuard],
    resolve: { motiveData: MotiveOffersResolver },
    data: { permission: { page: 'OFFERS_MOTIVES', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-shopping-cart bg-c-blue', status: true },
    children: [
      {
        path: 'cadastro',
        loadChildren: './motive-offers-form/motive-offers-form.module#MotiveOffersFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'OFFERS_MOTIVES', action: 'VIEW_ONE' } }
      },
    ]
  }
];
