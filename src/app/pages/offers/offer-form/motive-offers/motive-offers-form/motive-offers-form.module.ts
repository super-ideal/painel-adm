import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';

import { SharedModule } from '../../../../../shared/shared.module';

import { MotiveOffersFormRoutes } from './motive-offers-form.routing';
import { MotiveOffersFormComponent } from './motive-offers-form.component';
import { MotiveOfferResolver } from './resolvers/motive-offers.resolver';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(MotiveOffersFormRoutes),
    SharedModule,
    InputSwitchModule,
    InputTextareaModule,
    SweetAlert2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [MotiveOffersFormComponent],
  providers: [MotiveOfferResolver]
})
export class MotiveOffersFormModule { }
