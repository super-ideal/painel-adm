import { Routes } from '@angular/router';
import { MotiveOffersFormComponent } from './motive-offers-form.component';
import { AclGuard } from '../../../../../guards/acl.guard';
import { MotiveOfferResolver } from './resolvers/motive-offers.resolver';

export const MotiveOffersFormRoutes: Routes = [
  {
    path: '',
    component: MotiveOffersFormComponent,
    canActivate: [AclGuard],
    data: { permission: { page: 'OFFERS_MOTIVES', action: 'NEW' }, breadcrumb: 'Cadastro', icon: 'fas fa-list-alt bg-c-blue', status: true }
  },
  {
    path: '',
    data: { icon: 'fas fa-list-alt bg-c-blue', status: true },
    children: [
      {
        path: ':id',
        component: MotiveOffersFormComponent,
        canActivateChild: [AclGuard],
        resolve: { motiveData: MotiveOfferResolver },
        data: { permission: { page: 'OFFERS_MOTIVES', action: 'VIEW_ONE' }, breadcrumb: 'Detalhes' }
      },
      {
        path: ':id/editar',
        component: MotiveOffersFormComponent,
        canActivateChild: [AclGuard],
        resolve: { motiveData: MotiveOfferResolver },
        data: { permission: { page: 'OFFERS_MOTIVES', action: 'EDIT' }, breadcrumb: 'Edição' }
      },
    ]
  }
];
