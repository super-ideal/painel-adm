import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';

import { SharedModule } from '../../../../shared/shared.module';

import { MotiveOffersRoutes } from './motive-offers.routing';
import { MotiveOffersComponent } from './motive-offers.component';
import { MotiveOffersResolver } from './motive-offers.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(MotiveOffersRoutes),
    SharedModule,
    TableModule,
    PaginatorModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [MotiveOffersComponent],
  providers: [MotiveOffersResolver]
})
export class MotiveOffersModule { }
