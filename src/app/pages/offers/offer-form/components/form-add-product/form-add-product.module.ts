
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { CurrencyMaskModule } from 'ng2-currency-mask';

import { InputSwitchModule } from 'primeng/inputswitch';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CheckboxModule } from 'primeng/checkbox';
import { DialogModule } from 'primeng/dialog';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { PaginatorModule } from 'primeng/paginator';
import { MultiSelectModule } from 'primeng/multiselect';

import { SharedModule } from '../../../../../shared/shared.module';

import { FormAddProductComponent } from './form-add-product.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    InputSwitchModule,
    RadioButtonModule,
    CheckboxModule,
    DialogModule,
    PanelModule,
    DropdownModule,
    PaginatorModule,
    MultiSelectModule,
    SweetAlert2Module,
    CurrencyMaskModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [FormAddProductComponent],
  exports: [FormAddProductComponent]
})
export class FormAddProductModule { }
