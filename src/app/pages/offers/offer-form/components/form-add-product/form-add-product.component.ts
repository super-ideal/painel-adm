import { Component, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastOptions, ToastData, ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';

import { OfferProductService } from '../../../../../services/panel/offers/offersProducts.service';
import { OffersMotiveService } from '../../../../../services/panel/offers/offersMotive.service';
import { ProductService } from 'src/app/services/panel//products/product.service';

interface Button {
  visible: Boolean;
  disabled: Boolean;
}

@Component({
  selector: 'app-form-add-product',
  templateUrl: './form-add-product.component.html',
  styleUrls: ['./form-add-product.component.scss']
})
export class FormAddProductComponent {

  form: FormGroup;
  form_product: FormGroup;
  form_products_price_group: FormGroup;

  motives: any[];
  dataProductOffers: {page: number, data: Array<any>};
  dataProductsGroupPrice: any;
  dataProductsGroupPriceInOffer: any[] = [];

  typeForm: string;
  offer: any;
  idOfferProduct: number;
  produt_details: {id: number, barcode: string, description: string, price: number, group_price_name: string, group_price_id: number};
  offerProductSelected: any;

  discount_percent_per_product = 0;
  discount_money_per_product = 0;

  buttonDelete: Button = {visible: true, disabled: false};
  buttonEdit: Button = {visible: true, disabled: false};
  buttonSave: Button = {visible: false, disabled: false};
  buttonCancel: Button = {visible: false, disabled: false};

  @Output()
  showDisplayAddProductList: EventEmitter<any> = new EventEmitter<any>();
  closeDisplayAddProductForm: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private _offerProductService: OfferProductService,
    private _offersMotiveService: OffersMotiveService,
    private _productService: ProductService,
    private toastyService: ToastyService,
  ) {
    this.dataProductOffers = {page: 0, data: []};

    this.form = this.formBuilder.group({
      product: this.form_product = this.formBuilder.group({
        active: [true],
        top_offer: [false],
        offer_motive_id: [null],
        type_discount_money: [false],
        type_discount_percentage: [false],
        discount: [null],
        discount_price_two: [null],
        cumulative: [null, [Validators.min(2)]],
        cumulative_payable: [null, [Validators.min(1)]],
        view_percent_discount: [false],
        price_group: [false],
        wholesale: [false],
        limit_per_coupon: [null],
      }),
      products_price_group: this.form_products_price_group = this.formBuilder.group({
        addProductsGroupPrice: [null],
        rmProductsGroupPrice: [null]
      })
    });
  }

  /** Recebe o produto que foi selecionado */
  @Input()
  set data(data: {type: string, product: any, offer: number}) {
    this.typeForm = data['type'];
    this.offer = data['offer'];
    this.offerProductSelected = data['product'];

    // Pega os dados do produto
    if (this.typeForm === 'view') {
      this.idOfferProduct = this.offerProductSelected['id'];

      this.produt_details = {
        id: this.offerProductSelected['product_id'],
        barcode: this.offerProductSelected['product_barcode'],
        description: this.offerProductSelected['product_description'],
        price: this.offerProductSelected['product_price'],
        group_price_name: this.offerProductSelected['group_price_attribute_name'],
        group_price_id: this.offerProductSelected['group_price_attribute_id']
      };

    } else if (this.typeForm === 'new') {
      const group_price = _.filter(this.offerProductSelected['attributesProduct'], {type: {id_unique: 'price_group'}});

      this.produt_details = {
        id: this.offerProductSelected['id'],
        barcode: this.offerProductSelected['barcode'],
        description: this.offerProductSelected['description'],
        price: this.offerProductSelected['price'],
        group_price_name: group_price.length !== 0 ? group_price[0]['name'] : null,
        group_price_id: group_price.length !== 0 ? group_price[0]['id'] : null
      };
    }

    this.loadData();
  }

  /**
   * Busca dados na API
   */
  async loadData() {
    this.disableForm();

    this.addToast('wait', 'Aguarde...', 'Estamos buscando os dados da agenda!', 15000, 100);
      // Busca os motivos da oferta
      await this._offersMotiveService.getAll().then((result) => {
        this.motives = result['data'];
      }, (err) => {
        if (err.status !== 404) {
          this.addToast('error', 'Ops...', 'Ocorreu um erro interno no servidor. Tente novamente mais tarde!');
          this.goBack(false);
        }
      });

      // Busca todos os produtos do mesmo grupo de preço do produto
      if (this.produt_details.group_price_id) {
        await this._productService.getDetailsProductsGroupPrice(
          this.produt_details.group_price_id, 'p.id', this.produt_details.id.toString(), 'different'
        ).then((result) => {
          this.dataProductsGroupPrice = result['data'];
        }, (err) => {
          if (err.status !== 404) {
            this.addToast('error', 'Ops...', 'Ocorreu um erro interno no servidor. Tente novamente mais tarde!');
            this.goBack(false);
          }
        });
      }

      // Busca todos os produtos do mesmo grupo de preço do produto, que estão em oferta
      if (this.typeForm !== 'new') {
        await this._offerProductService.getProductsGroupPrice(this.offer['id'], this.idOfferProduct)
        .then((result) => {
          this.dataProductsGroupPriceInOffer = [];
          result['data'].forEach(element => {
            this.dataProductsGroupPriceInOffer.push(_.filter(this.dataProductsGroupPrice, {'id': element.product_id})[0]);
          });

          this.form_products_price_group.patchValue({
            addProductsGroupPrice: this.dataProductsGroupPriceInOffer,
            rmProductsGroupPrice: null
          });
        }, (err) => {
          if (err.status !== 404) {
            this.addToast('error', 'Ops...', 'Ocorreu um erro interno no servidor. Tente novamente mais tarde!');
            this.goBack(false);
          }
        });
      }

      // Verifica se o produto está em outras agendas de oferta já iniciada ou a iniciar
      await this._offerProductService.verifyProductsInOffers('product_id', this.produt_details.id).then((result) => {
        const product = _.filter(result['data'], {offer_id: this.offer['id']}); // filtra somente os produtos que pertence a essa agenda.
        this.dataProductOffers.data = _.difference(result['data'], product); // remove o produto atual da listagem, para não mostra a agenda atual
      }, (err) => {
        if (err.status !== 404) {
          this.addToast('error', 'Ops...', 'Ocorreu um erro ao buscar os dados do Preço 2.');
          this.goBack(false);
        }
      });

      this.loadForm();
    this.toastyService.clear(100);
  }

  /**
   * Carrega os dados no formulário
   */
  async loadForm() {
    this.form_product.reset();
    this.form_product.get('active').setValue(true);
    this.form_product.get('top_offer').setValue(false);
    this.form_product.get('view_percent_discount').setValue(false);
    this.form_product.get('wholesale').setValue(false);

    switch (this.typeForm) {
      case 'view': {
        await this.form_product.patchValue({
          active: !!this.offerProductSelected['active'],
          top_offer: !!this.offerProductSelected['top_offer'],
          offer_motive_id: _.filter(this.motives, {'id': this.offerProductSelected['offer_motive_id']})[0] || null,
          type_discount_money: !!this.offerProductSelected['type_discount_money'],
          type_discount_percentage: !!this.offerProductSelected['type_discount_percentage'],
          discount: this.offerProductSelected['discount'],
          discount_price_two: this.offerProductSelected['discount_price_two'],
          cumulative: this.offerProductSelected['cumulative'],
          cumulative_payable: this.offerProductSelected['cumulative_payable'],
          price_group: !!this.offerProductSelected['price_group'],
          wholesale: !!this.offerProductSelected['wholesale'] || 0,
          view_percent_discount: !!this.offerProductSelected['view_percent_discount'],
          limit_per_coupon: this.offerProductSelected['limit_per_coupon'],
        });

        await this.calculatePercentualDiscountPerProduct();
        this.disableForm();
        break;
      }
      case 'new': {
        this.enableForm();
        break;
      }
    }
  }

  /**
   * Tratamento dos dados para poder gravar
   */
  async save() {
    if (this.form.invalid) {
      this.addToast('error', 'Erro', 'Formulário inválido. Verifique os dados e tente novamente!');
      return;
    } else if (this.offer['status'] === 'FINALIZADA') {
      this.addToast('info', 'Edição não permitida', 'A agenda de oferta já finalizou.');
      return;
    }

    const form = this.form.value;
    form['product']['offer_motive_id'] = this.form_product.get('offer_motive_id').value != null ? this.form_product.value.offer_motive_id['id'] : null;
    form['product']['product_id'] = this.produt_details.id;

    if (this.typeForm === 'view') {
      if (form['products_price_group'] && form['products_price_group']['addProductsGroupPrice']) {
        // Verifica quais produtos do grupo de preço foram removidos ou adicionados
        const form_products_price_group = form['products_price_group']['addProductsGroupPrice'];

        const rm = [];
        this.dataProductsGroupPriceInOffer.forEach(element => {
          if (_.filter(form_products_price_group, {id: element.id}).length === 0) {
            rm.push(element);
          }
        });

        const add = [];
        form_products_price_group.forEach(element => {
          if (_.filter(this.dataProductsGroupPriceInOffer, {id: element.id}).length === 0) {
            add.push(element);
          }
        });

        form['products_price_group']['addProductsGroupPrice'] = add;
        form['products_price_group']['rmProductsGroupPrice'] = rm;
      }

      return await this.updateProduct(form);
    } else {
      if (form['product']['cumulative'] && form['product']['cumulative'] === '') {
        form['product']['cumulative'] = null;
      }

      return await this.addProduct(form);
    }
  }


  /**
   * Adiciona o produto a agenda de oferta
   * @param {Object} data Dados do produto da oferta
   */
  async addProduct(data: object) {
    await this._offerProductService.post(data, this.offer['id']).then((result) => {
      this.addToast('success', 'Sucesso', 'O Produto foi adicionado com sucesso!');
      this.goBack(false);
    }, (err) => this.handleError(err));
  }

  /**
   * Atualiza o produto a agenda de oferta
   * @param {Object} data Dados do produto da oferta
   */
  async updateProduct(data: object) {
    await this._offerProductService.update(data, this.idOfferProduct).then((result) => {
      this.addToast('success', 'Sucesso', 'O Produto foi atualizado com sucesso!');
      this.goBack(true);
    }, (err) => this.handleError(err));
  }

  /**
   * Seta o valor correto ao tipo de desconto.
   * Utilizado para que o valor dos tipo de desconto nunca fiquem iguais.
   * @param {string} typeDiscount Tipo do desconto
   */
  setValueTypeDiscount(typeDiscount: string) {
    if (typeDiscount === 'type_discount_money') {
      this.form_product.patchValue({
        discount: null,
        discount_price_two: null,
        type_discount_percentage: false,
        type_discount_money: true
      });
    } else {
      this.form_product.patchValue({
        discount: null,
        discount_price_two: null,
        type_discount_percentage: true,
        type_discount_money: false
      });
    }

    console.log(this.form_product.value);

    this.calculatePercentualDiscountPerProduct();
  }


  /**
   * Cálcula o desconto por produto e a porcentagem de desconto
   */
  calculatePercentualDiscountPerProduct() {
    this.form_product.get('limit_per_coupon').setValidators(Validators.min(this.form_product.get('cumulative').value));
    this.form_product.get('cumulative_payable').setValidators(Validators.max(this.form_product.get('cumulative').value - 1));

    let discount_percent_per_product = 0;
    let discount_money_per_product = 0;

    const price = this.produt_details.price;
    const discount = this.form_product.get('discount').value;
    const discount_price_two = this.form_product.get('discount_price_two').value;
    const cumulative = this.form_product.get('cumulative').value;
    const cumulative_payable = this.form_product.get('cumulative_payable').value;
    const type_discount_money = this.form_product.get('type_discount_money').value;
    const type_discount_percentage = this.form_product.get('type_discount_percentage').value;

    if (type_discount_money && discount) {
      if (discount_price_two && cumulative) { // desconto + desconto preço 2 + acumulativo
        discount_percent_per_product = (discount + discount_price_two) / (price * cumulative);
        discount_money_per_product = (discount + discount_price_two) / cumulative;
      } else if (discount_price_two) { // desconto + desconto preço 2
        discount_percent_per_product = (discount + discount_price_two) / price;
        discount_money_per_product = discount + discount_price_two;
      } else if (cumulative) { // desconto + acumulativo
        discount_percent_per_product = discount / (price * cumulative);
        discount_money_per_product = discount / cumulative;
      } else { // desconto
        discount_percent_per_product = discount / price;
        discount_money_per_product = discount;
      }
    } else if (type_discount_percentage && discount) {
      if (discount_price_two && cumulative) { // desconto + desconto preço 2 + acumulativo
        discount_percent_per_product = (discount + discount_price_two) / cumulative;
        discount_money_per_product = (discount + discount_price_two) / cumulative;
      } else if (discount_price_two) { // desconto + desconto preço 2
        discount_percent_per_product = discount + discount_price_two;
        discount_money_per_product = discount + discount_price_two;
      } else if (cumulative) { // desconto + acumulativo
        discount_percent_per_product = discount / cumulative;
        discount_money_per_product = (discount / cumulative) * price;
      } else { // desconto
        discount_percent_per_product = discount;
        discount_money_per_product = discount * price;
      }
    } else {
      if (cumulative && cumulative_payable) { // acumulativo + acumulativo a pagar
        discount_percent_per_product = (((cumulative - cumulative_payable) * price) / (price * cumulative)) / cumulative;
        discount_money_per_product = ((cumulative - cumulative_payable) * price) / cumulative_payable;
      } else if (cumulative) { // acumulativo apenas
        discount_percent_per_product = 0;
        discount_money_per_product = 0;
      }
    }

    this.discount_percent_per_product = discount_percent_per_product;
    this.discount_money_per_product = discount_money_per_product;

    // this.disableCumulativePayable();
  }

  /**
   * Desativa o campo acumulativo a pagar caso o campo acumulativo esteja vazio
   */
  disableCumulativePayable(): void {
    if (this.form_product.value['cumulative']) {
      this.form_product.get('cumulative_payable').enable();
    } else {
      this.form_product.get('cumulative_payable').reset();
      this.form_product.get('cumulative_payable').disable();
      this.enableDiscountAndTypeDiscount();
    }
  }

  /**
   * Desativa o campo de desconto de preço 2
   */
  disablePriceTwo(): void {
    if (this.form_product.value['discount']) {
      this.form_product.get('discount_price_two').enable();
    } else {
      this.form_product.get('discount_price_two').reset();
      this.form_product.get('discount_price_two').disable();
    }
  }

  verifyEnableDisableDiscountAndTypeDiscount(event) {
    if (event.srcElement.value) {
      this.disableDiscountAndTypeDiscount();
    } else {
      this.enableDiscountAndTypeDiscount();
    }
  }


  /**
   * Desabilita ou habilita o campo de desconto e tipo de desconto
   */
  disableDiscountAndTypeDiscount(): void {
      this.form_product.get('discount').reset();
      this.form_product.get('discount_price_two').reset();

      this.form_product.get('type_discount_money').disable();
      this.form_product.get('type_discount_percentage').disable();
      this.form_product.get('discount').disable();
      this.form_product.get('discount_price_two').disable();
      this.form_product.patchValue({discount: null, type_discount_percentage: false, type_discount_money: false});
  }

  /**
   * Habilita o campo de desconto e tipos de desconto
   */
  enableDiscountAndTypeDiscount(): void {
    this.form_product.get('type_discount_money').enable();
    this.form_product.get('type_discount_percentage').enable();
    this.form_product.get('discount').enable();
    this.form_product.get('discount_price_two').enable();
    this.form_product.patchValue({cumulative_payable: null, type_discount_money: true, type_discount_percentage: false});
  }

  /**
   * Ativa a edição do formulário e ajusta os botões de ações
   */
  enableForm(): void {
    this.buttonEdit.visible = false;
    this.buttonDelete.visible = false;
    this.buttonSave.visible = true;
    this.buttonCancel.visible = true;
    this.form.enable();

    this.calculatePercentualDiscountPerProduct();

    if (this.offer['opt_discount'] || this.offer['opt_discount_price_two']) {
      this.form_product.get('type_discount_money').enable();
      this.form_product.get('type_discount_percentage').enable();

      if (this.form_product.get('cumulative').value && this.form_product.get('cumulative_payable').value) {
        this.disableDiscountAndTypeDiscount();
      }

      if (this.offer['opt_discount']) {
        this.form_product.get('discount').enable();
      }

      if (this.offer['opt_discount_price_two']) {
        this.form_product.get('discount_price_two').enable();
      }
    } else {
      this.form_product.get('type_discount_money').disable();
      this.form_product.get('type_discount_percentage').disable();
      this.form_product.get('discount_price_two').disable();
      this.form_product.get('discount').disable();
    }

    this.offer['opt_wholesale'] ? this.form_product.get('wholesale').enable() : this.form_product.get('wholesale').disable();
    this.offer['opt_cumulative'] ? this.form_product.get('cumulative').enable() : this.form_product.get('cumulative').disable();
    this.offer['opt_cumulative_payable'] ? this.form_product.get('cumulative_payable').enable() : this.form_product.get('cumulative_payable').disable();
    this.offer['opt_limit_per_coupon'] ? this.form_product.get('limit_per_coupon').enable() : this.form_product.get('limit_per_coupon').disable();
    this.offer['opt_top_offer'] ? this.form_product.get('top_offer').enable() : this.form_product.get('top_offer').disable();
    this.offer['opt_view_percent_discount'] ? this.form_product.get('view_percent_discount').enable() : this.form_product.get('view_percent_discount').disable();

    if (this.offer['opt_price_group'] && this.produt_details['group_price_id']) {
      this.form_product.get('price_group').enable();
      this.form_products_price_group.enable();
    } else {
      this.form_product.get('price_group').disable();
      this.form_products_price_group.disable();
    }
  }

  /**
   * Função do botão cancelar
   */
  async cancel() {
    if (this.typeForm !== 'new') {
      await this.loadData();
      await this.disableForm();
    } else {
      this.goBack(true);
    }
  }

  /**
   * Desativa a edição do formulário e ajusta os botões de ações
   */
  disableForm() {
    this.buttonEdit.visible = true;
    this.buttonDelete.visible = true;
    this.buttonSave.visible = false;
    this.buttonCancel.visible = false;
    this.form.disable();
  }

  /**
   * Função de voltar para uma determinada página
   * @param {boolean} [close=false] ID do produto
   */
  goBack(close: boolean = false): void {
    if (this.typeForm === 'view' || close) {
      this.showDisplayAddProductList.emit({productList: false, productForm: false});
    } else {
      this.showDisplayAddProductList.emit({productList: true, productForm: false});
    }
  }

  /**
   * Função de tratamento de erros
   * @param {Object} err Objeto com os dados do erro
   */
  handleError(err: any): void {
    switch (err.status) {
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast('error', 'Erro', err.message);
  }

   /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number): void {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
    }
  }
}
