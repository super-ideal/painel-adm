
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { PanelModule } from 'primeng/panel';
import { MessagesModule } from 'primeng/messages';

import { SharedModule } from '../../../../../shared/shared.module';

import { ListProductsInOfferComponent } from './list-products-in-offer.component';
import { FormAddProductModule } from '../form-add-product/form-add-product.module';
import { ModalListProductsModule } from '../modal-list-products-add/modal-list-products-add.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TableModule,
    PaginatorModule,
    PanelModule,
    DialogModule,
    MessagesModule,
    FormAddProductModule,
    ModalListProductsModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ListProductsInOfferComponent],
  exports: [ListProductsInOfferComponent]
})
export class ListProductsInOfferModule { }
