import { Component, Input, ViewChildren } from '@angular/core';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { Message } from 'primeng/components/common/api';
import * as _ from 'lodash';

import { OfferProductService } from '../../../../../services/panel/offers/offersProducts.service';

import { Pagination } from '../../../../../interfaces/pagination.interface';
import { OfferProducts } from '../../../../../class/offer/offer-product';

interface HeaderTable {
  field: string;
  header: string;
  tooltip?: string;
}

@Component({
  selector: 'app-list-products-in-offer',
  templateUrl: './list-products-in-offer.component.html',
  styleUrls: ['./list-products-in-offer.component.scss']
})
export class ListProductsInOfferComponent {
  search: {
    whereBy: string,
    whereTextSearch: string,
    whereOperator: string,
    searchOptions: Array<{label: string, value: string, type: string}>,
    searchOptionsOperator: Array<{label: string, value: string}>,
  };

  table: {
    cols: HeaderTable[],
    dataOffersProducts: OfferProducts[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number
  };
  selectedRowTable: any;

  offerData: any;
  displayAddProductList: Boolean = false;
  displayAddProductForm: Boolean = false;
  productSelected: any;
  typeViewFormProduct: string;

  msgNotFoundProducts: Message[] = [];

  constructor(
    private toastyService: ToastyService,
    private offerProductService: OfferProductService,
  ) {
    this.table = {
      cols: [
        { field: 'id', header: 'ID' },
        { field: 'active', header: 'Ativo', tooltip: 'Ativo na agenda' },
        { field: 'product_barcode', header: 'EAN', tooltip: 'Código de barras do produto' },
        { field: 'product_description', header: 'Descrição', tooltip: 'Descrição curta do produto' },
        { field: 'product_price', header: 'Preço', tooltip: 'Preço atual do produto' },
        { field: 'type_discount_percentage', header: 'Porcentagem', tooltip: 'Tipo de desconto: Porcentagem' },
        { field: 'type_discount_money', header: 'Dinheiro', tooltip: 'Tipo de desconto: Dinheiro' },
        { field: 'discount', header: 'Desconto', tooltip: 'Valor a descontar emcima do preço de venda de acordo com o tipo de desconto.' },
        { field: 'discount_price_two', header: 'Desconto Preço 2', tooltip: 'Valor a descontar emcima do desconto atual' },
        { field: 'cumulative', header: 'Qtd. acumulativo', tooltip: 'Quantidade acumulativo' },
        { field: 'cumulative_payable', header: 'Qtd. acumulativo a pagar', tooltip: 'Quantidade acumulativo a pagar' },
        { field: 'top_offer', header: 'Top', tooltip: 'Informa que o produto é um item Top da agenda de oferta' },
        { field: 'wholesale', header: 'Atacado', tooltip: 'Informa se o produto está em oferta de atacado' },
        { field: 'limit_per_coupon', header: 'Limite por cupom', tooltip: 'Limite de produtos por cupom para obter o desconto' },
        { field: 'price_group', header: 'Grupo de preço', tooltip: 'Informa se o grupo de preço está ativo' },
        { field: 'view_percent_discount', header: 'Mostrar porcentagem desconto', tooltip: 'Informa se irá mostrar a quantidade em porcentagem de desconto' }
      ],
      dataOffersProducts: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10
    };

    this.search = {
      whereBy: 'product_description',
      whereTextSearch: '',
      whereOperator: 'anywhere',
      searchOptions: [
        { label: 'Descrição', value: 'product_description', type: 'text' },
        { label: 'Cód. barras (EAN)', value: 'product_barcode', type: 'text' },
        { label: 'Preço venda', value: 'product_price', type: 'number' },
        { label: 'Desconto', value: 'discount', type: 'number' },
        { label: 'Desconto preço 2', value: 'discount_price_two', type: 'number' },
        { label: 'Qtd. acumulativo', value: 'cumulative', type: 'number' },
        { label: 'Qtd. acumulativo a pagar', value: 'cumulative_payable', type: 'number' },
        { label: 'Limite por cupom', value: 'limit_per_coupon', type: 'number' },
      ],
      searchOptionsOperator: []
    };
    this.selectTheOperators();
  }

  /** Recebe os dados da agenda para buscar os produtos relacionada a ela */
  @Input()
  set offer(offer: any) {
    this.offerData = offer || null;

    if (this.offerData) {
      this.getAllOffersProducts();
    }
  }

   /**
   * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
   */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllOffersProducts();
  }

  /**
   * Realiza uma busca manual
   */
  searchManual() {
    this.getAllOffersProducts(true);
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  getAllOffersProducts(searchManual = false) {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando os produtos da agenda!', 15000, 100);

    this.offerProductService.getAll(
      this.offerData['id'],
      this.table.pageNumber,
      this.table.rowsOnPage,
      this.search.whereBy,
      this.search.whereOperator,
      this.search.whereTextSearch.trim()
    ).then(data => {
      this.table.elementsPagination = data['data'];
      this.table.dataOffersProducts = data['data']['data'];

      this.msgNotFoundProducts = [];
      this.toastyService.clear(100);
    }, (err) => {
      this.toastyService.clear(100);

      if (err.status === 404 && searchManual === false) {
        this.msgNotFoundProducts = [];
        return this.msgNotFoundProducts = [{
          severity: 'warn',
          summary: 'Atenção',
          detail: 'Esta agenda ainda não tem nenhum produto.'
        }];
      }

      this.handleError(err);
    });
  }

  /**
   * Informa os operadores permitos para fazer a busca
   */
  selectTheOperators() {
    const optionSelected = _.filter(this.search.searchOptions, {'value': this.search.whereBy})[0];

    if (optionSelected.type === 'text') {
      this.search.searchOptionsOperator = [
        { label: 'Em qualquer lugar', value: 'anywhere' },
        { label: 'Começado com', value: 'started' },
        { label: 'Terminado com', value: 'finished' },
        { label: 'Igual a', value: 'equal' },
        { label: 'Diferente de', value: 'different' },
        { label: 'Contido em', value: 'contained' }
      ];
    } else if (optionSelected.type === 'number') {
      this.search.searchOptionsOperator = [
        { label: 'Igual a', value: 'equal' },
        { label: 'Diferente de', value: 'different' },
        { label: 'Menor que', value: 'smaller' },
        { label: 'Maior que', value: 'bigger' },
        { label: 'Contido em', value: 'contained' }
      ];
    }
  }

  /**
   * Modal - Listagem de produtos para adicionar a agenda
   */
  showDisplayListProducts({productList = false, productForm = false}) {
    // Atualiza a tabela de produtos da agenda quando sai do formulario e vai para a listagem de produtos
    if (productForm === false && productList === true) {
      this.table.dataOffersProducts = [];
      this.getAllOffersProducts();
    }

    this.displayAddProductList = productList;
    this.displayAddProductForm = productForm;
  }

  /**
   * Modal - Formulário de produto da agenda de oferta
   */
  async showDisplayAddProductForm(obj: {product: any, type: string}) {
    // Ajustes para abrir o modal de formulário do produto em oferta
    this.productSelected = obj.product;
    this.typeViewFormProduct = obj.type;
    this.displayAddProductList = false;
    this.displayAddProductForm = true;
  }

  verifyDuplicateProduct(product: any) {
    const filter = _.filter(this.table.dataOffersProducts, {'barcode': product.barcode});
    return filter.length === 0 ? false : true;
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();

    switch (err.error.code && err.name && err.status) {
      case 404: err.error.message = 'Nenhum produto foi encontrado com os dados informados'; break;
      default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    this.addToast('error', 'Erro', err.error.message);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
