
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';

import { SharedModule } from '../../../../../shared/shared.module';

import { ModalListProductsAddComponent } from './modal-list-products-add.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    TableModule,
    PaginatorModule,
    DialogModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ModalListProductsAddComponent],
  exports: [ModalListProductsAddComponent]
})
export class ModalListProductsModule { }
