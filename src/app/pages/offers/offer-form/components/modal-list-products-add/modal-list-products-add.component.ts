import { Component, Output, EventEmitter, Input, AfterContentInit } from '@angular/core';
import { ToastOptions, ToastData, ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';

import { Product } from '../../../../../interfaces/product/product.interface';
import { ProductService } from '../../../../../services/panel/products/product.service';
import { OfferProductService } from '../../../../../services/panel/offers/offersProducts.service';

import { Pagination } from '../../../../../interfaces/pagination.interface';

interface HeaderTable {
  field: string;
  header: string;
}

@Component({
  selector: 'app-modal-list-products-add',
  templateUrl: './modal-list-products-add.component.html',
  styleUrls: ['./modal-list-products-add.component.scss']
})
export class ModalListProductsAddComponent implements AfterContentInit {
  search: {
    whereBy: string,
    whereTextSearch: string,
    searchOptions: Array<{label: string, value: string}>
  };

  table: {
    cols: HeaderTable[],
    dataProducts: Product[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number
  };
  selectedRowTable: any;

  productsOffer: any = [];

  /** Envia o produto que foi selecionado */
  @Output()
  productSelectedEvent: EventEmitter<any> = new EventEmitter<any>();

  /** Recebe todos os produtos da agenda de oferta */
  @Input()
  set productsInOffer(productsInOffer: any) {
    this.productsOffer = productsInOffer;
  }

  constructor(
    private _productService: ProductService,
    private _offerProductService: OfferProductService,
    private toastyService: ToastyService
  ) {
    this.table = {
      cols: [
        { field: 'id', header: 'Id' },
        { field: 'active', header: 'Ativo' },
        { field: 'barcode', header: 'Cód. Barras' },
        { field: 'description', header: 'Descrição' },
        { field: 'price', header: 'Preço Venda' },
      ],
      dataProducts: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10
    };

    this.search = {
      whereBy: 'description',
      whereTextSearch: '',
      searchOptions: [
        { label: 'Descrição', value: 'description' },
        { label: 'Cód. Barras', value: 'barcode' },
        // { label: 'Ativo', value: 'active' },
      ]
    };
  }

  async ngAfterContentInit() {
    await this.getAllProducts();
  }

  /**
   * Envia o produto selecionado para a página OfferProductList
   */
  addProductSelected(product) {
    if (product.inOffer) {
      return this.addToast('warning', 'Duplicidade', 'O produto selecionado já existe na agenda de promoção.', 5000);
    } else if (product.inPriceGroup) {
      // tslint:disable-next-line:max-line-length
      return this.addToast('warning', 'Duplicidade', 'O produto selecionado pertence ao grupo de preço de um produto que está na agenda de promoção.', 5000);
    }

    this.productSelectedEvent.emit({product: product, type: 'new'});
  }

   /**
   * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
   */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllProducts();
  }

  /**
   * Realiza uma busca manual
   */
  searchManual() {
    let textSearch = this.search.whereTextSearch.trim();
    if (this.search.whereBy === 'active') {
      if (textSearch === 'sim' || textSearch === 's') { textSearch = '1'; } else if (textSearch === 'nao' || textSearch === 'não' || textSearch === 'n') { textSearch = '0'; }
    }

    this.getAllProducts(textSearch, this.search.whereBy['value']);
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  async getAllProducts(wText: string = '1', wBy: string = 'active') {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando os produtos!', 20000, 100);

    await this._productService.getAll({
      pageNumber: this.table.pageNumber,
      perPage: this.table.rowsOnPage,
      whereBy: wBy,
      whereText: wText
    }).then(async data => {
      this.table.elementsPagination = data['data'];
      this.table.dataProducts = data['data']['data'];

      if (this.productsOffer) {
        console.log(this.productsOffer); // corrigir erro
        await this.verifyProductsInOffer();
        await this.verifyProductsInOfferGroupPrice();
      }

      this.toastyService.clear(100);
    }, (err) => {
      if (err.status === 404) {
        this.toastyService.clearAll();
        this.addToast('warning', 'Atenção', 'Não encontramos nenhum produto com os dados informados. Verifique!');
        this.table.dataProducts = [];
      } else {
        this.handleError(err);
      }
    });
  }

  /**
   * Verifica se o produto está na Agenda de Oferta
   * @param products Produtos vindos da busca que foi feita
   */
  async verifyProductsInOffer() {
    const dataProductsSearch = [];
    const products = this.table.dataProducts;
    const productsOffer = this.productsOffer;
    _.forEach(products, function (productSearch) {
      productSearch['inOffer'] = false;

      if (_.filter(productsOffer, { product_id: productSearch['id'] }).length !== 0) {
        productSearch['inOffer'] = true;
      }

      dataProductsSearch.push(productSearch);
    });
    this.table.dataProducts = dataProductsSearch;
  }

  /**
   * Verifica se o produto buscado pertence a algum grupo de preço na agenda
   * @param products Produtos vindos da busca que foi feita
   */
  async verifyProductsInOfferGroupPrice() {
    if (this.productsOffer[0]) {
      await this._offerProductService.getAllProductsGroupPrice(this.productsOffer[0]['offer_id'])
        .then(productsGroupPrice => {

          const dataProductsSearch = [];
          const products = this.table.dataProducts;
          _.forEach(products, function (productSearch) {
            productSearch['inPriceGroup'] = false;
            productSearch['inProductPriceGroup'] = null;

            // Verifica se o produto está em algum grupo de preço de produto que já esta na agenda
            const exists = _.filter(productsGroupPrice['data'], { product_id: productSearch['id'] });
            if (exists.length !== 0) {
              productSearch['inPriceGroup'] = true;
              productSearch['inProductPriceGroup'] = exists[0].parent_product;
            }

            dataProductsSearch.push(productSearch);
          });

          this.table.dataProducts = dataProductsSearch;
        }, (err) => {
          if (err.status !== 404) {
            this.handleError(err);
          }
        });
    }
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any, type: string = 'error', title: string = 'Erro') {
    this.toastyService.clearAll();

    switch (err.name && err.status) {
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    return this.addToast(type, title, err.message);
  }

  /**
  * Toast de alertas
  * @param {string} type Tipo do toast (success, error, warning, wait)
  * @param {string} title Título do toast
  * @param {string} message Mensagem do toast
  * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
  * @param {string} [idToast] Id único para o toast
  */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
