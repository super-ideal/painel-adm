
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { InputSwitchModule } from 'primeng/inputswitch';

import { SharedModule } from '../../../shared/shared.module';

import { OfferFormComponent } from './offer-form.component';
import { OfferFormRoutes } from './offer-form.routing';
import { ListProductsInOfferModule } from './components/list-products-in-offer/list-products-in-offer.module';
import { OfferFormResolver } from './resolvers/offer-form.resolver';
import { OfferTypesResolver } from './resolvers/offer-types.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(OfferFormRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    InputTextareaModule,
    InputSwitchModule,
    DropdownModule,
    CalendarModule,
    ListProductsInOfferModule,
    SweetAlert2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [OfferFormComponent],
  providers: [OfferFormResolver, OfferTypesResolver]
})
export class OfferFormModule { }
