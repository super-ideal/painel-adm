import { Routes } from '@angular/router';
import { OffersComponent } from './offers.component';
import { OffersListResolver } from './offers.resolver';
import { AclGuard } from '../../guards/acl.guard';

const breadcrumb = 'Agendas de Ofertas';
const message = '';

export const OffersRoutes: Routes = [
  {
    path: '',
    component: OffersComponent,
    pathMatch: 'full',
    canActivate: [AclGuard],
    resolve: { offersData: OffersListResolver },
    data: { permission: { page: 'OFFERS', action: 'VIEW_ALL' }, breadcrumb: breadcrumb, message: message, icon: 'fas fa-cart-arrow-down bg-c-blue', status: true }
  },
  {
    path: '',
    data: { breadcrumb: breadcrumb, message: message, icon: 'fas fa-cart-arrow-down bg-c-blue', status: true },
    children: [
      {
        path: 'tipos',
        loadChildren: './type-offers/type-offers.module#TypeOffersModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'OFFERS_TYPES', action: 'VIEW_ALL' } }
      },
      {
        path: 'motivos',
        loadChildren: './offer-form/motive-offers/motive-offers.module#MotiveOffersModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'OFFERS_MOTIVES', action: 'VIEW_ALL' } }
      },
      {
        path: 'cadastro',
        loadChildren: './offer-form/offer-form.module#OfferFormModule',
        canActivateChild: [AclGuard],
        data: { permission: { page: 'OFFERS', action: 'VIEW_ONE' } }
      },
    ]
  }
];
