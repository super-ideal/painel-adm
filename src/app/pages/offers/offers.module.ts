
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { GrowlModule } from 'primeng/growl';
import { TabViewModule } from 'primeng/tabview';
import { CalendarModule } from 'primeng/calendar';

import { SharedModule } from '../../shared/shared.module';

import { OffersComponent } from './offers.component';
import { OffersRoutes } from './offers.routing';
import { OffersListResolver } from './offers.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(OffersRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    InputSwitchModule,
    InputTextareaModule,
    DropdownModule,
    MultiSelectModule,
    GrowlModule,
    TableModule,
    PaginatorModule,
    TabViewModule,
    CalendarModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [OffersComponent],
  providers: [OffersListResolver]
})
export class OffersModule { }
