import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import * as moment from 'moment';
import * as _ from 'lodash';

import { OfferService } from '../../services/panel/offers/offers.service';

import { Pagination } from '../../interfaces/pagination.interface';
import { Offer } from '../../class/offer/offer';
import { VerifyPermissions } from '../../shared/verify-permissions/verify-permissions';

interface HeaderTable {
  field: string;
  header: string;
  width: string;
}

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {
  permissionsUserLogged: any;

  search: {
    whereBy: string,
    whereOperator: string,
    whereTextSearch: string,
    whereTextSearch2: string,
    whereByTypeSelected: string,
    searchOptions: Array<{ label: string, value: string, type: string}>,
    searchOptionsStatus: Array<{ label: string, value: string }>,
    searchOptionsBoolean: Array<{ label: string, value: string }>,
    searchOptionsOperator: Array<{ label: string, value: string }>,
    calendarLanguage: any
  };

  table: {
    cols: HeaderTable[],
    dataOffers: Offer[],
    elementsPagination: Pagination,
    pageNumber: number,
    selectRowsOnPage: Array<number>,
    rowsOnPage: number,
    selectedRowTable: any
  };
  selectedViewColumns: any[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastyService: ToastyService,
    private verifyPermissions: VerifyPermissions,
    private _offerService: OfferService,
  ) {
    this.table = {
      cols: [
        { field: 'id', header: 'Id', width: '3em' },
        { field: 'active', header: 'Ativa', width: '5em' },
        { field: 'name', header: 'Nome', width: '18em' },
        { field: 'offer_type_name', header: 'Tipo Oferta', width: '7em' },
        { field: 'status', header: 'Status', width: '7.5em' },
        { field: 'datetime_start', header: 'Data Início', width: '9em' },
        { field: 'datetime_end', header: 'Data Fim', width: '9em' },
        { field: 'created_name_reason', header: 'Criada Por', width: 'unset' },
      ],
      dataOffers: [],
      elementsPagination: { perPage: 0, total: 0, lastPage: 0, page: 0 },
      pageNumber: 1,
      selectRowsOnPage: [10, 25, 50, 100],
      rowsOnPage: 10,
      selectedRowTable: [],
    };
    this.selectedViewColumns = this.table.cols;

    this.search = {
      whereBy: 'name',
      whereOperator: 'anywhere',
      whereByTypeSelected: 'text',
      whereTextSearch: '',
      whereTextSearch2: '',
      searchOptions: [
        { label: 'Nome', value: 'name', type: 'text' },
        { label: 'Tipo Oferta', value: 'offer_type_name', type: 'text' },
        { label: 'Ativo', value: 'active', type: 'boolean' },
        { label: 'Status', value: 'status', type: 'textStatus' },
        // { label: 'Data Início', value: 'datetime_start', type: 'date' },
        // { label: 'Data Fim', value: 'datetime_end', type: 'date' },
        { label: 'Criada Por', value: 'created_name_reason', type: 'text' },
      ],
      searchOptionsStatus: [
        { label: 'Aguardando', value: 'AGUARDANDO' },
        { label: 'Andamento', value: 'ANDAMENTO' },
        { label: 'Desativada', value: 'DESATIVADA' },
        { label: 'Finalizada', value: 'FINALIZADA' },
        { label: 'Todos', value: '2' },
      ],
      searchOptionsBoolean: [
        { label: 'Sim', value: '1' },
        { label: 'Não', value: '0' },
        { label: 'Todos', value: '2' },
      ],
      searchOptionsOperator: [],
      calendarLanguage: {
        'pt_BR': {
          firstDayOfWeek: 1,
          dayNames: [ 'domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado' ],
          dayNamesShort: [ 'dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb' ],
          dayNamesMin: [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
          // tslint:disable-next-line:max-line-length
          monthNames: [ 'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro' ],
          monthNamesShort: [ 'jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez' ],
          today: 'Hoje',
          clear: 'Limpar'
        }
      }
    };
  }

  ngOnInit() {
    this.route.data.subscribe(async data => {
      if (data['offersData']) {
        this.table.elementsPagination = await data['offersData'];
        this.table.dataOffers = await data['offersData']['data'];
      }
      this.permissionsUserLogged = this.verifyPermissions.getActionsUser(data['permission']['page']); // permissão dos botões
    });
    this.selectTheOperators();
  }

  /**
  * Informa os operadores permitos para fazer a busca
  */
  selectTheOperators() {
    this.search.whereByTypeSelected = _.filter(this.search.searchOptions, { value: this.search.whereBy })[0]['type'];

    switch (this.search.whereByTypeSelected) {
      case 'textStatus': {
        if (this.search.whereTextSearch === '2') {
          this.search.whereOperator = 'different';
        } else {
          this.search.whereOperator = 'equal';
        }

        this.search.searchOptionsOperator = [];
        break;
      }
      case 'boolean': {
        if (this.search.whereTextSearch === '2') {
          this.search.whereOperator = 'different';
        } else {
          this.search.whereOperator = 'equal';
        }

        this.search.searchOptionsOperator = [];
        break;
      }
      case 'number': {
        this.search.searchOptionsOperator = [
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Menor que', value: 'smaller' },
          { label: 'Maior que', value: 'bigger' },
          { label: 'Contido em', value: 'contained' }
        ];
        break;
      }
      case 'date': {
        this.search.searchOptionsOperator = [
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Menor que', value: 'smaller' },
          { label: 'Maior que', value: 'bigger' },
        ];
        break;
      }
      default: {
        this.search.searchOptionsOperator = [
          { label: 'Em qualquer lugar', value: 'anywhere' },
          { label: 'Começado com', value: 'started' },
          { label: 'Terminado com', value: 'finished' },
          { label: 'Igual a', value: 'equal' },
          { label: 'Diferente de', value: 'different' },
          { label: 'Contido em', value: 'contained' }
        ];
        break;
      }
    }
  }

   /**
   * Informa a página a selecionar e o total de linha a ser apresentado para buscar os resultados
   */
  setPage(pageInfo) {
    this.table.rowsOnPage = pageInfo.rows;
    this.table.pageNumber = pageInfo.page + 1;
    this.getAllOffers();
  }

  /**
   * Realiza a busca e preenche a tabela com os resultados encontrados
   */
  getAllOffers() {
    this.addToast('wait', 'Aguarde...', 'Estamos buscando alguns dados para você!', 15000, 100);

    // Seleciona o operador correto antes de pesquisar (Serve apenas para caso for booleano)
    this.selectTheOperators();

    this._offerService.getAll({
      pageNumber: this.table.pageNumber,
      perPage: this.table.rowsOnPage,
      whereBy: this.search.whereBy,
      operator: this.search.whereOperator,
      whereText: this.search.whereTextSearch.trim()
    }).then(data => {
      this.table.elementsPagination = data['data'];
      this.table.dataOffers = data['data']['data'];

      this.toastyService.clear(100);
    }, (err) => {
      if (err.status === 404) {
        return this.handleError(err, 'warning', 'Atenção');
      }
      this.handleError(err);
    });
  }

  goToViewDetails(id) {
    this.router.navigate(['cadastro/' + id], { relativeTo: this.route });
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any, type: string = 'error', title: string = 'Erro') {
    this.toastyService.clearAll();
    console.log(err);

    switch (err.name && err.status) {
      case 404: err.message = 'Não encontramos nenhuma agenda com os dados informados'; break;
      default: err.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
    }

    this.addToast(type, title, err.message);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
