
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { LightboxModule } from 'ngx-lightbox';

import { SharedModule } from '../../../shared/shared.module';

import { CardModule } from 'primeng/card';
import { InputSwitchModule } from 'primeng/inputswitch';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';

import { PublicDetailsShoppingListComponent } from './public-details-shopping-list.component';
import { PublicDetailsShoppingListRoutes } from './public-details-shopping-list.routing';
import { PublicDetailsShoppingListResolver } from './resolvers/public-details-shopping-list.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PublicDetailsShoppingListRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    SweetAlert2Module,
    CardModule,
    InputSwitchModule,
    LightboxModule,
    MessageModule,
    MessagesModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [PublicDetailsShoppingListComponent],
  providers: [PublicDetailsShoppingListResolver]
})
export class PublicDetailsShoppingListModule { }
