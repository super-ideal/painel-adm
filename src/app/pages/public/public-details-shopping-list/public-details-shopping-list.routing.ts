import { Routes } from '@angular/router';

import { PublicDetailsShoppingListComponent } from './public-details-shopping-list.component';
import { PublicDetailsShoppingListResolver } from './resolvers/public-details-shopping-list.resolver';

const message = '';

export const PublicDetailsShoppingListRoutes: Routes = [
  {
    path: ':id',
    component: PublicDetailsShoppingListComponent,
    resolve: { detailsData: PublicDetailsShoppingListResolver },
    data: { breadcrumb: 'Lista de Compra', message: message, icon: 'fas fa-clipboard-list bg-c-blue', status: true }
  }
];
