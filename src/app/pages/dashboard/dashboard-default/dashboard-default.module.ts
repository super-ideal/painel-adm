import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../../shared/shared.module';

import { DashboardDefaultRoutes } from './dashboard-default.routing';
import { DashboardDefaultComponent } from './dashboard-default.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardDefaultRoutes),
    SharedModule
  ],
  declarations: [DashboardDefaultComponent]
})
export class DashboardDefaultModule { }
