import { Routes } from '@angular/router';
import { DashboardDefaultComponent } from './dashboard-default.component';

export const DashboardDefaultRoutes: Routes = [
  {
    path: '',
    component: DashboardDefaultComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'fas fa-home bg-c-blue',
      status: false
    }
  }
];
