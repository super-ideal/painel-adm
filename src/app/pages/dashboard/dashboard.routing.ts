import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

const breadcrumb = 'Agenda de Oferta';
const message = '';

export const DashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        redirectTo: 'default',
        pathMatch: 'full'
      },
      {
        path: 'default',
        loadChildren: './dashboard-default/dashboard-default.module#DashboardDefaultModule'
      }
    ]
  }
];
