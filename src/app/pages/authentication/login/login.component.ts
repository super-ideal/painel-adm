import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import * as _ from 'lodash';

import { AnimationBuilder, AnimationService } from 'css-animator';
import { NgwWowService } from 'ngx-wow';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

import { AuthService } from '../../../services/panel/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  // Variáveis
  loginForm: FormGroup;
  msgs: Message[];
  animator: AnimationBuilder;
  returnUrl: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private _authService: AuthService,
    private animationService: AnimationService,
    private wowService: NgwWowService,
    private toastyService: ToastyService
  ) {
    // this.wowService.init();
    this.animator = this.animationService.builder();

    this.loginForm = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)])],
      password: [null, [Validators.required, Validators.minLength(6)]],
    });
  }

  ngOnInit() {
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  async onSubmit(element: HTMLElement) {
    this.addToast('wait', 'Aguarde...', 'Estamos processando sua solicitação!', 15000, 100);

    await this._authService.login(this.loginForm.value)
    .then((resp) => {
      this.toastyService.clear(100);
      const access_panel = _.filter(resp['data']['user']['permissions'], { page_name: 'CONTROL_PANEL' });

      if (access_panel.length !== 0 && resp['data']['user']['active'] !== 0) {
        this.router.navigateByUrl(this.returnUrl); // login successful so redirect to return url
      } else {
        this.addToast('warning', 'Sem permissão', 'Você não tem permissão para acessar o painel', 15000, 110);
        this._authService.logout().then(() => {
          localStorage.clear();
        }, (err) => {
          localStorage.clear();
        });
      }
    }, (err) => {
      this.toastyService.clear(100);
      this.animator.setType('shake').setDuration(1500).animate(element);
      this.handleError(err);
    });
  }

  /**
   * Função de tratamento de erros
   * @param {*} err Objeto com dados do erro
   */
  handleError(err: any) {
    this.toastyService.clearAll();
    this.msgs = [];

    if (err.error.code === 'E_USER_NOT_FOUND' || err.error.code === 'E_PASSWORD_MISMATCH') {
      this.msgs.push({severity: 'error', summary: 'Erro', detail: 'E-mail ou Senha inválidos'});
    } else {
      switch (err.name) {
        default: err.error.message = 'Ocorreu um erro inesperado. Tente novamente!'; break;
      }

      return this.addToast('error', 'Erro', err.error.message, 15000);
    }
  }

  /**
 * Toast de alertas
 * @param {string} type Tipo do toast (success, error, warning, wait)
 * @param {string} title Título do toast
 * @param {string} message Mensagem do toast
 * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
 * @param {string} [idToast] Id único para o toast
 */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
