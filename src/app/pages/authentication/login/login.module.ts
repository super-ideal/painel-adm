import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { CaptchaModule } from 'primeng/captcha';

import { SharedModule } from '../../../shared/shared.module';

import { LoginComponent } from './login.component';
import { LoginRoutes } from './login.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LoginRoutes),
    SharedModule,
    ReactiveFormsModule,
    MessagesModule,
    MessageModule,
    CaptchaModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
