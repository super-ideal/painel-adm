import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable, ErrorHandler, Injector } from '@angular/core';

import { ToastyService, ToastData, ToastOptions } from 'ng2-toasty';

@Injectable()
export class AplicationErrorHandle extends ErrorHandler {

  constructor(private injector: Injector, private toastyService: ToastyService) {
    super();
  }

  handleError(errorResponse: HttpErrorResponse | any) {
    if (errorResponse instanceof HttpErrorResponse) {
      const error = (typeof errorResponse.error !== 'object') ? JSON.parse(errorResponse.error) : errorResponse.error;

      console.log('entrou => ', error);
      if ((errorResponse.status === 400 || errorResponse.status === 401) &&
      (error.error === 'E_JWT_TOKEN_EXPIRED' || error.error === 'E_INVALID_JWT_TOKEN')) {
        this.addToast('warning', 'Sem permissão', 'O token expirou ou não é válido. Faça o login novamente!', 5000);
        this.goToLogin();
      } else {
        this.addToast('error', 'Erro', 'Desculpe mas nesse momento estamos com problemas. Tente novamente dentre alguns segundos', 10000);
      }

    }

    super.handleError(errorResponse);
  }

  goToLogin(): void {
    const router = this.injector.get(Router);
    router.navigate(['auth/login']);
  }

  /**
   * Toast de alertas
   * @param {string} type Tipo do toast (success, error, warning, wait)
   * @param {string} title Título do toast
   * @param {string} message Mensagem do toast
   * @param {number} [timeout=3000] Tempo em milissegundos para o toast sumir
   * @param {string} [idToast] Id único para o toast
   */
  addToast(type: string, title: string, message: string, timeout: number = 3000, idToast?: number) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: timeout,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        if (idToast) { toast.id = idToast; }
      }
    };

    switch (type) {
      case 'success': this.toastyService.success(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'default': this.toastyService.default(toastOptions); break;
    }
  }
}
