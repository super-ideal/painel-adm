// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  api_url: 'http://localhost:3333/v1/',
  public_api_url: 'http://localhost:3333/v1/web/',
  upload_url: 'http://54.233.242.37/',
  version: require('../../package.json').version,
};
