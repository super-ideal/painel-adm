export const environment = {
  production: true,
  api_url: 'https://apiideal.herokuapp.com/v1/',
  public_api_url: 'https://apiideal.herokuapp.com/v1/web/',
  upload_url: 'http://54.233.242.37/',
  version: require('../../package.json').version
};
